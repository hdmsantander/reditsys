package mx.uam.ayd.proyecto;

import mx.uam.ayd.proyecto.datos.*;
import mx.uam.ayd.proyecto.negocio.*;
import mx.uam.ayd.proyecto.negocio.dominio.*;
import mx.uam.ayd.proyecto.presentacion.*;

/**
 * Clase principal que arranca la aplicación. Lanza la ventana de login.
 */
public class Aplicacion {
	// Modulos de la aplicacion
	//private static DAOLibro daoLibro;
	//private static ServicioLibro servicioLibro;
	//private static ControlAgregarLibro controlAgregarLibro;
	//private static ControlListarCatalogo controlListarCatalogo;
		
	
	// Modulos de la aplicacion
	private static DAOCotizacionGlobal daoCotizacionGlobal;
	private static DAOSubCotizacion daoSubCotizacion;
	private static DAOProducto daoProducto;
	private static DAOCliente daoCliente;
	private static DAOUsuario daoUsuario;
	private static DAOTipoProducto daoTipoProducto;
	private static DAOProveedor daoProveedor;
	
	// Servicios de la aplicacion
	private static ServicioCliente servicioCliente;
	private static ServicioUsuario servicioUsuario;
	private static ServicioTipoProducto servicioTipoProducto;
	private static ServicioCotizacionGlobal servicioCotizacionGlobal;
		
	private static ControlVentanaPrincipal controlPrincipal;
	private static ControlCrearCotizacionGlobal controlCrearCotizacionGlobal;
	private static ControlBusquedaCotizacionGlobal controlBusca;
	private static ControlCotizacionGlobal controlGlobal;
	private static ControlSubCotizacion controlSub;
	private static ControlLoginUsuario controlUsuario;

	/**
	 * Método principal, invoca la conexión de módulos y lanza la ventana de login.
	 * @param args argumentos de la linea de comandos. (No usados).
	 */
	public static void main(String[] args) {
		creaYConectaModulos();
		//controlBusca.inicia();
		//controlGlobal.inicia();
		//controlSub.inicia();
		controlUsuario.inicia();
		
	}
	
	/**
	 * Se encarga de realizar la conexión de los módulos de la aplicación.
	 */
	private static void creaYConectaModulos() {
		// Conecta los modulos
		//daoLibro = new DAOLibroBD();
		//servicioLibro = new ServicioLibroImpl(daoLibro);
		//controlAgregarLibro = new ControlAgregarLibro(servicioLibro);
		servicioUsuario = new ServicioUsuarioImpl(new DAOUsuarioBD());
		
		//controlListarCatalogo = new ControlListarCatalogo(servicioLibro);
		
		daoCotizacionGlobal = new DAOCotizacionGlobalBD();
		daoSubCotizacion = new DAOSubCotizacionBD();
		daoProducto = new DAOProductoBD();
		daoCliente = new DAOClienteBD();
		daoUsuario = new DAOUsuarioBD();
		daoTipoProducto = new DAOTipoProductoBD();
		daoProveedor = new DAOProveedorBD();
		
		servicioCliente = new ServicioClienteImpl(daoCliente);				
		servicioUsuario = new ServicioUsuarioImpl(daoUsuario);				
		servicioTipoProducto = new ServicioTipoProductoImpl(daoTipoProducto);				
		servicioCotizacionGlobal = new ServicioCotizacionGlobalImpl(daoCotizacionGlobal, daoSubCotizacion, daoProducto, daoProveedor);				
		
		controlCrearCotizacionGlobal = new ControlCrearCotizacionGlobal(servicioCotizacionGlobal, servicioCliente, servicioUsuario, servicioTipoProducto);
		
		//controlPrincipal = new ControlVentanaPrincipal();
		controlBusca = new ControlBusquedaCotizacionGlobal(controlCrearCotizacionGlobal);
		controlGlobal = new ControlCotizacionGlobal();
		controlSub = new ControlSubCotizacion();
		controlUsuario = new ControlLoginUsuario(servicioUsuario);
	}
	
}
