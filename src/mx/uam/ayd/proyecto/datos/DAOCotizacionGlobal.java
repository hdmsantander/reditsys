package mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;

/**
 * DAO Para la entidad cotizacionGlobal
 */

public interface DAOCotizacionGlobal {
	
	/**
	 * boolean <code>crea</code>(CotizacionGlobal cotizacionGlobal)
	 * <ul>
	 * <li>Agrega una cotizacion global a la base de datos.
	 * </ul>
	 * @param cotizacionGlobal la cotizacion global a agregar.
	 * @return true si se creo exitosamente, false ya existía una cotizacion global con ese id o hubo una excepción al crearlo.
	 */
	public boolean crea(CotizacionGlobal cotizacionGlobal);
	
	/**
	 * CotizacionGlobal <code>recupera</code>(String folio)
	 * <ul>
	 * <li>Recupera una cotizacion global a partir de su folio de la base de datos.
	 * </ul>
	 * @param folio el folio de la cotizacion global a recuperar.
	 * @return una referencia a la cotizacion global o null si no se encontró.
	 */
	public CotizacionGlobal recupera(String folio);
	
	/**
	 * boolean <code>actualiza</code>(CotizacionGlobal cotizacionGlobal)
	 * <ul>
	 * <li>Actualiza los valores de una cotizacion global existente en la base de datos.
	 * </ul>
	 * @param cotizacionGlobal la cotizacion global a actualizar.
	 * @return true si se actualizo correctamente o no existía la cotización global en la base de datos, false si hubo una excepción al actualizarla.
	 */
	public boolean actualiza(CotizacionGlobal cotizacionGlobal);

	/**
	 * boolean <code>borra</code>(CotizacionGlobal cotizacionGlobal)
	 * <ul>
	 * <li>Borra la cotizacion global proporcionada de la base de datos.
	 * </ul>
	 * @param cotizacionGlobal la cotizacion global a borrar.
	 * @return true si se borró exitosamente o no existía la cotización global en la base de datos, false si hubo una excepción al borrarla.
	 */
	public boolean borra(CotizacionGlobal cotizacionGlobal);
	
	/**
	 * ArrayList &lt; CotizacionGlobal &gt; <code>recuperaTodas</code>()
	 * <ul>
	 * <li>Regresa una lista con todas las cotizaciones globales en la base de datos.
	 * </ul>
	 * @return un ArrayList con todas las cotizaciones globales en la base de datos.
	 * o null si no hay cotizaciones globales en la base de datos.
	 */
	public ArrayList<CotizacionGlobal> recuperaTodas();
}
