package mx.uam.ayd.proyecto.datos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import mx.uam.ayd.proyecto.negocio.dominio.Cliente;

/**
 * Implementación del DAO Para la entidad Cliente.
 */
public class DAOClienteBD implements DAOCliente {
	
	static Logger log = Logger.getRootLogger();

	@Override
	public boolean crea(Cliente cliente) {
		String queryCrea = "INSERT INTO APP.CLIENTE VALUES (" +
			"'" + cliente.getCodigoCliente() + "'," +
			"'" + cliente.getNombre() + "'," +
			"'" + cliente.getRfc() + "'," +
			"'" + cliente.getDireccion()+ "'," +
			"'" + cliente.getTelefono() + "'," +
			"'" + cliente.getCorreoElectronico() + "'" +
		")";
		
		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryCrea);

			return true;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al crear", ex.getRealException());

            return false;
        }

        catch(SQLException e){
    	    log.error("Excepcion de la base de datos al crear", e);

    	    return false;
        }	
	}

	@Override
	public Cliente recupera(String codigoCliente){
		String queryRecupera = "SELECT * FROM APP.CLIENTE "
				+ "WHERE codigo_cliente = '" + codigoCliente + "'";

		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();	
			ResultSet rst = statement.executeQuery(queryRecupera);
			
			if(rst.next()) {
				Cliente cliente = new Cliente();
				cliente.setCodigoCliente(rst.getString("codigo_cliente"));
				cliente.setNombre(rst.getString("nombre"));
				cliente.setRfc(rst.getString("rfc"));
				cliente.setDireccion(rst.getString("direccion"));
				cliente.setTelefono(rst.getString("telefono"));
				cliente.setCorreoElectronico(rst.getString("correo_electronico"));

				return cliente;
			}

			else
				return null;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar", ex.getRealException());

            return null;
		}

    	catch(SQLException e) {
    		log.error("Excepcion de la base de datos al recuperar", e);

    		return null;
    	}		
	}

	@Override
	public boolean actualiza(Cliente cliente) {
		String queryActualiza = "UPDATE APP.CLIENTE SET "
			+ "nombre = '" + cliente.getNombre() + "', "
			+ "rfc = '" + cliente.getRfc() + "', "
			+ "direccion = '" + cliente.getDireccion() + "', "
			+ "telefono = '" + cliente.getTelefono() + "', "
			+ "correo_electronico = '" + cliente.getCorreoElectronico() + "' "
			+ "WHERE codigo_cliente = '" + cliente.getCodigoCliente() + "'";
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryActualiza);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al actualizar",ex.getRealException());

            return false;
		
		}

		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al actualizar",e);

	    	return false;
	    }

		return true;
	}

	@Override
	public boolean borra(Cliente cliente) {	
		String queryBorra = "DELETE FROM APP.CLIENTE "
				+ "WHERE codigo_cliente = '" + cliente.getCodigoCliente() + "'";
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryBorra);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al borrar",ex.getRealException());

            return false;
		}

        catch(SQLException e){
    		log.error("Excepcion de la base de datos al borrar",e);

    		return false;
        }

        return true;
	}

	@Override
	public ArrayList<Cliente> recuperaTodos() {
		String queryRecuperaTodos = "SELECT * FROM APP.CLIENTE";
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();
		Cliente cliente = null;

		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();			
			ResultSet rst = statement.executeQuery(queryRecuperaTodos);
			
			while(rst.next()) {
				cliente = new Cliente();
				cliente.setCodigoCliente(rst.getString("codigo_cliente"));
				cliente.setNombre(rst.getString("nombre"));
				cliente.setRfc(rst.getString("rfc"));
				cliente.setDireccion(rst.getString("direccion"));
				cliente.setTelefono(rst.getString("telefono"));
				cliente.setCorreoElectronico(rst.getString("correo_electronico"));

				clientes.add(cliente);
			}
			
			if (clientes.isEmpty())
				return null;

			else
				return clientes;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar todos", ex.getRealException());

            return null;
		}

   		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al recuperar todos", e);

	    	return null;
    	}	
	}
}
