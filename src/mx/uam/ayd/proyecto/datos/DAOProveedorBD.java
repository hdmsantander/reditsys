package mx.uam.ayd.proyecto.datos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import mx.uam.ayd.proyecto.negocio.dominio.Proveedor;

/**
 * Implementación del DAO Para la entidad Proveedor.
 */
public class DAOProveedorBD implements DAOProveedor {
	static Logger log = Logger.getRootLogger();

	@Override
	public boolean crea(Proveedor proveedor) {
		String queryCrea = "INSERT INTO APP.PROVEEDOR VALUES (" +
			proveedor.getNumeroProveedor() + ", " +
			"'" + proveedor.getNombre() + "'," +
			"'" + proveedor.getRfc() + "'," +
			"'" + proveedor.getDireccion()+ "'," +
			"'" + proveedor.getTelefono() + "'," +
			"'" + proveedor.getCorreoElectronico() + "', " +
			"'" + proveedor.getDescripcion() + "'" +
		")";
		
		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryCrea);

			return true;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al crear", ex.getRealException());

            return false;
        }

        catch(SQLException e){
    	    log.error("Excepcion de la base de datos al crear", e);

    	    return false;
        }	
	}

	@Override
	public Proveedor recupera(int numeroProvedor) {
		String queryRecupera = "SELECT * FROM APP.PROVEEDOR "
				+ "WHERE id_proveedor = " + numeroProvedor;

		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();	
			ResultSet rst = statement.executeQuery(queryRecupera);
			
			if(rst.next()) {
				Proveedor proveedor = new Proveedor();
				proveedor.setNumeroProveedor(rst.getInt("id_proveedor"));
				proveedor.setNombre(rst.getString("nombre"));
				proveedor.setRfc(rst.getString("rfc"));
				proveedor.setDireccion(rst.getString("direccion"));
				proveedor.setTelefono(rst.getString("telefono"));
				proveedor.setCorreoElectronico(rst.getString("correo_electronico"));
				proveedor.setDescripcion(rst.getString("descripcion"));

				return proveedor;
			}

			else
				return null;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar", ex.getRealException());

            return null;
		}

    	catch(SQLException e) {
    		log.error("Excepcion de la base de datos al recuperar", e);

    		return null;
    	}		
	}

	@Override
	public boolean actualiza(Proveedor proveedor) {
		String queryActualiza = "UPDATE APP.PROVEEDOR SET "
			+ "nombre = '" + proveedor.getNombre() + "', "
			+ "rfc = '" + proveedor.getRfc() + "', "
			+ "direccion = '" + proveedor.getDireccion() + "', "
			+ "telefono = '" + proveedor.getTelefono() + "', "
			+ "correo_electronico = '" + proveedor.getCorreoElectronico() + "', "
			+ "descripcion = '" + proveedor.getDescripcion() + "' "
			+ "WHERE id_proveedor = " + proveedor.getNumeroProveedor();
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryActualiza);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al actualizar",ex.getRealException());

            return false;
		
		}

		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al actualizar",e);

	    	return false;
	    }

		return true;
	}

	@Override
	public boolean borra(Proveedor proveedor) {	
		String queryBorra = "DELETE FROM APP.PROVEEDOR "
				+ "WHERE id_proveedor = " + proveedor.getNumeroProveedor();
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryBorra);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al borrar",ex.getRealException());

            return false;
		}

        catch(SQLException e){
    		log.error("Excepcion de la base de datos al borrar",e);

    		return false;
        }

        return true;
	}

	@Override
	public ArrayList<Proveedor> recuperaTodos() {
		String queryRecuperaTodos = "SELECT * FROM APP.PROVEEDOR";
		ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
		Proveedor proveedor = null;

		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();			
			ResultSet rst = statement.executeQuery(queryRecuperaTodos);
			
			while(rst.next()) {
				proveedor = new Proveedor();
				proveedor.setNumeroProveedor(rst.getInt("id_proveedor"));
				proveedor.setNombre(rst.getString("nombre"));
				proveedor.setRfc(rst.getString("rfc"));
				proveedor.setDireccion(rst.getString("direccion"));
				proveedor.setTelefono(rst.getString("telefono"));
				proveedor.setCorreoElectronico(rst.getString("correo_electronico"));
				proveedor.setDescripcion(rst.getString("descripcion"));

				proveedores.add(proveedor);
			}
			
			if (proveedores.isEmpty())
				return null;

			else
				return proveedores;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar todos", ex.getRealException());

            return null;
		}

   		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al recuperar todos", e);

	    	return null;
    	}	
	}
}