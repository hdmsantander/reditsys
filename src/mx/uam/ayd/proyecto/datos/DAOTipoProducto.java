package mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.negocio.dominio.TipoProducto;

/**
 * DAO Para la entidad TipoProducto.
 */
public interface DAOTipoProducto {
	
	/**
	 * boolean <code>crea</code>(TipoProducto tipoProducto)
	 * <ul>
	 * <li>Agrega un tipo de producto a la base de datos.
	 * </ul>
	 * @param tipoProducto el tipo de producto a agregar.
	 * @return true si se creo exitosamente, false ya existía un tipo de producto con ese id o hubo una excepción al crearlo.
	 */
	public boolean crea(TipoProducto tipoProducto);
	
	/**
	 * TipoProducto <code>recupera</code>(int numeroTipoProducto)
	 * <ul>
	 * <li>Recupera un tipo de producto a partir de su numero de tipo de producto de la base de datos.
	 * </ul>
	 * @param numeroTipoProducto el numero del tipo de producto a buscar.
	 * @return una referencia al tipo de producto o null si no se encontró.
	 */
	public TipoProducto recupera(int numeroTipoProducto);
	
	/**
	 * boolean <code>actualiza</code>(TipoProducto tipoProducto)
	 * <ul>
	 * <li>Actualiza los valores de un tipo de producto existente en la base de datos.
	 * </ul>
	 * @param tipoProducto el tipo de producto a actualizar.
	 * @return true si se actualizo correctamente o no existía el tipo de producto en la base de datos, false si hubo una excepción al actualizarlo.
	 */
	public boolean actualiza(TipoProducto tipoProducto);

	/**
	 * boolean <code>borra</code>(TipoProducto tipoProducto)
	 * <ul>
	 * <li>Borra el tipo de producto proporcionado de la base de datos.
	 * </ul>
	 * @param tipoProducto el tipo de producto a borrar
	 * @return true si se borró exitosamente o no existía el tipo de producto en la base de datos, false si hubo una excepción al borrarlo.
	 */
	public boolean borra(TipoProducto tipoProducto);
	
	/**
	 * ArrayList &lt; TipoProducto &gt; <code>recuperaTodos</code>()
	 * <ul>
	 * <li>Regresa una lista con todos los tipos de producto en la base de datos.
	 * </ul>
	 * @return un ArrayList con todos los tipos de producto en la base de datos
	 * o null si no hay tipos de producto en la base de datos.
	 */
	public ArrayList<TipoProducto> recuperaTodos();
}
