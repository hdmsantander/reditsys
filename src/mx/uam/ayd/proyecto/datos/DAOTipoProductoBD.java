package mx.uam.ayd.proyecto.datos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import mx.uam.ayd.proyecto.negocio.dominio.TipoProducto;

/**
 * Implementación del DAO Para la entidad TipoProducto.
 */
public class DAOTipoProductoBD implements DAOTipoProducto {
	static Logger log = Logger.getRootLogger();

	@Override
	public boolean crea(TipoProducto tipoProducto) {
		String queryCrea = "INSERT INTO APP.TIPO_PRODUCTO VALUES (" +
			tipoProducto.getNumeroTipoProducto() +", " +
			"'" + tipoProducto.getNombre() + "'" +
		")";
		
		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryCrea);

			return true;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al crear", ex.getRealException());

            return false;
        }

        catch(SQLException e){
    	    log.error("Excepcion de la base de datos al crear", e);

    	    return false;
        }	
	}

	@Override
	public TipoProducto recupera(int numeroTipoProducto) {
		String queryRecupera = "SELECT * FROM APP.TIPO_PRODUCTO "
				+ "WHERE id_tipo_producto = " + numeroTipoProducto;

		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();	
			ResultSet rst = statement.executeQuery(queryRecupera);
			
			if(rst.next()) {
				TipoProducto tipoProducto = new TipoProducto();
				tipoProducto.setNumeroTipoProducto(rst.getInt("id_tipo_producto"));
				tipoProducto.setNombre(rst.getString("nombre"));

				return tipoProducto;
			}

			else
				return null;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar", ex.getRealException());

            return null;
		}

    	catch(SQLException e) {
    		log.error("Excepcion de la base de datos al recuperar", e);

    		return null;
    	}		
	}

	@Override
	public boolean actualiza(TipoProducto tipoProducto) {
		String queryActualiza = "UPDATE APP.TIPO_PRODUCTO SET "
			+ "nombre = '" + tipoProducto.getNombre() + "' "
			+ "WHERE id_tipo_producto = " + tipoProducto.getNumeroTipoProducto();
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryActualiza);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al actualizar",ex.getRealException());

            return false;
		
		}

		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al actualizar",e);

	    	return false;
	    }

		return true;
	}

	@Override
	public boolean borra(TipoProducto tipoProducto) {	
		String queryBorra = "DELETE FROM APP.TIPO_PRODUCTO "
				+ "WHERE id_tipo_producto = " + tipoProducto.getNumeroTipoProducto();
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryBorra);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al borrar",ex.getRealException());

            return false;
		}

        catch(SQLException e){
    		log.error("Excepcion de la base de datos al borrar",e);

    		return false;
        }

        return true;
	}

	@Override
	public ArrayList<TipoProducto> recuperaTodos() {
		String queryRecuperaTodos = "SELECT * FROM APP.TIPO_PRODUCTO";
		ArrayList<TipoProducto> tiposProductos = new ArrayList<TipoProducto>();
		TipoProducto tipoProducto = null;

		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();			
			ResultSet rst = statement.executeQuery(queryRecuperaTodos);
			
			while(rst.next()) {
				tipoProducto = new TipoProducto();
				tipoProducto.setNumeroTipoProducto(rst.getInt("id_tipo_producto"));
				tipoProducto.setNombre(rst.getString("nombre"));

				tiposProductos.add(tipoProducto);
			}
			
			if (tiposProductos.isEmpty())
				return null;

			else
				return tiposProductos;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar todos", ex.getRealException());

            return null;
		}

   		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al recuperar todos", e);

	    	return null;
    	}	
	}
}