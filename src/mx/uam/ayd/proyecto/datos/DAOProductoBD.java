package mx.uam.ayd.proyecto.datos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;
import mx.uam.ayd.proyecto.negocio.dominio.Producto;

/**
 * Implementación del DAO Para la entidad Producto.
 */
public class DAOProductoBD implements DAOProducto {
	static Logger log = Logger.getRootLogger();

	@Override
	public boolean crea(Producto producto) {
		String queryCrea = "INSERT INTO APP.PRODUCTO VALUES (" +
			producto.getNumeroProducto() + ", " +
			"'" + producto.getDescripcion() + "'," +
			"" + producto.getCantidad() + "," +
			"" + producto.getPrecioCostoUnitario()+ "," +
			"" + producto.getPrecioVentaUnitario() + "," +
			"'" + producto.getSubCotizacion().getSubFolio() + "', " +
			"" + producto.getTipoProducto().getNumeroTipoProducto() + ", " +
			"" + producto.getProveedor().getNumeroProveedor()  + "" +
		")";
		
		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryCrea);

			return true;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al crear", ex.getRealException());

            return false;
        }

        catch(SQLException e){
    	    log.error("Excepcion de la base de datos al crear", e);

    	    return false;
        }	
	}

	@Override
	public Producto recupera(int numeroProducto) {
		String queryRecupera = "SELECT * FROM APP.PRODUCTO "
				+ "WHERE id_producto = " + numeroProducto;

		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();	
			ResultSet rst = statement.executeQuery(queryRecupera);
			
			if(rst.next()) {
				Producto producto = new Producto();
				producto.setNumeroProducto(rst.getInt("id_producto"));
				producto.setDescripcion(rst.getString("descripcion"));
				producto.setCantidad(rst.getInt("cantidad"));
				producto.setPrecioCostoUnitario(rst.getFloat("precio_costo_unitario"));
				producto.setPrecioVentaUnitario(rst.getFloat("precio_venta_unitario"));

				return producto;
			}

			else
				return null;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar", ex.getRealException());

            return null;
		}

    	catch(SQLException e) {
    		log.error("Excepcion de la base de datos al recuperar", e);

    		return null;
    	}		
	}

	@Override
	public boolean actualiza(Producto producto) {
		String queryActualiza = "UPDATE APP.PRODUCTO SET "
			+ "descripcion = '" + producto.getDescripcion() + "', "
			+ "cantidad = " + producto.getCantidad() + ", "
			+ "precio_costo_unitario = " + producto.getPrecioCostoUnitario() + ", "
			+ "precio_venta_unitario = " + producto.getPrecioVentaUnitario() + ", "
			+ "id_tipo_producto = " + producto.getTipoProducto().getNumeroTipoProducto() + ", "
			+ "id_proveedor = " + producto.getProveedor().getNumeroProveedor() + " "
			+ "WHERE id_producto = " + producto.getNumeroProducto();
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryActualiza);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al actualizar",ex.getRealException());

            return false;
		
		}

		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al actualizar",e);

	    	return false;
	    }

		return true;
	}

	@Override
	public boolean borra(Producto producto) {	
		String queryBorra = "DELETE FROM APP.PRODUCTO "
				+ "WHERE id_producto = " + producto.getNumeroProducto();
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryBorra);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al borrar",ex.getRealException());

            return false;
		}

        catch(SQLException e){
    		log.error("Excepcion de la base de datos al borrar",e);

    		return false;
        }

        return true;
	}

	@Override
	public ArrayList<Producto> recuperaTodos() {
		String queryRecuperaTodos = "SELECT * FROM APP.PRODUCTO";
		ArrayList<Producto> productos = new ArrayList<Producto>();
		Producto producto = null;

		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();			
			ResultSet rst = statement.executeQuery(queryRecuperaTodos);
			
			while(rst.next()) {
				producto = new Producto();
				producto.setNumeroProducto(rst.getInt("id_producto"));
				producto.setDescripcion(rst.getString("descripcion"));
				producto.setCantidad(rst.getInt("cantidad"));
				producto.setPrecioCostoUnitario(rst.getFloat("precio_costo_unitario"));
				producto.setPrecioVentaUnitario(rst.getFloat("precio_venta_unitario"));

				productos.add(producto);
			}
			
			if (productos.isEmpty())
				return null;

			else
				return productos;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar todos", ex.getRealException());

            return null;
		}

   		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al recuperar todos", e);

	    	return null;
    	}	
	}

	@Override
	public ArrayList<Producto> recuperaTodos(SubCotizacion subCotizacion) {
		String queryRecuperaTodos = "SELECT * FROM APP.PRODUCTO "
				+ "WHERE subfolio = '" + subCotizacion.getSubFolio() +"'";
		ArrayList<Producto> productos = new ArrayList<Producto>();
		Producto producto = null;

		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();			
			ResultSet rst = statement.executeQuery(queryRecuperaTodos);
			
			while(rst.next()) {
				producto = new Producto();
				producto.setNumeroProducto(rst.getInt("id_producto"));
				producto.setDescripcion(rst.getString("descripcion"));
				producto.setCantidad(rst.getInt("cantidad"));
				producto.setPrecioCostoUnitario(rst.getFloat("precio_costo_unitario"));
				producto.setPrecioVentaUnitario(rst.getFloat("precio_venta_unitario"));

				productos.add(producto);
			}
			
			if (productos.isEmpty())
				return null;

			else
				return productos;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar todos", ex.getRealException());

            return null;
		}

   		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al recuperar todos", e);

	    	return null;
    	}	
	}
}