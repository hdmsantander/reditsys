package mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.negocio.dominio.Proveedor;

/**
 * DAO Para la entidad Proveedor.
 */
public interface DAOProveedor {
	
	/**
	 * boolean <code>crea</code>(Proveedor proveedor)
	 * <ul>
	 * <li>Agrega un proveedor a la base de datos.
	 * </ul>
	 * @param proveedor el proveedor a agregar.
	 * @return true si se creo exitosamente, false ya existía un proveedor con ese id o hubo una excepción al crearlo.
	 */
	public boolean crea(Proveedor proveedor);
	
	/**
	 * Proveedor <code>recupera</code>(int numeroProveedor)
	 * <ul>
	 * <li>Recupera un proveedor a partir del numero de proveedor de la base de datos.
	 * </ul>
	 * @param numeroProveedor el numero de proveedor del proveedor a recuperar.
	 * @return una referencia al proveedor o null si no se encontró.
	 */
	public Proveedor recupera(int numeroProveedor);
	
	/**
	 * boolean <code>actualiza</code>(Proveedor proveedor)
	 * <ul>
	 * <li>Actualiza los valores de un proveedor existente en la base de datos.
	 * </ul>
	 * @param proveedor el proveedor a actualizar.
	 * @return true si se actualizo correctamente o no existía el proveedor en la base de datos, false si hubo una excepción al actualizarlo.
	 */
	public boolean actualiza(Proveedor proveedor);

	/**
	 * boolean <code>borra</code>(Proveedor proveedor)
	 * <ul>
	 * <li>Borra el proveedor proporcionado de la base de datos.
	 * </ul>
	 * @param proveedor el proveedor a borrar.
	 * @return true si se borró exitosamente o no existía el proveedor en la base de datos, false si hubo una excepción al borrarlo.
	 */
	public boolean borra(Proveedor proveedor);
	
	/**
	 * ArrayList &lt; Proveedor &gt; <code>recuperaTodas</code>()
	 * <ul>
	 * <li>Regresa una lista con todos los proveedores en la base de datos.
	 * </ul>
	 * @return un ArrayList con todos los proveedores en la base de datos.
	 * o null si no hay proveedores en la base de datos.
	 */
	public ArrayList<Proveedor> recuperaTodos();
}
