package mx.uam.ayd.proyecto.datos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;

/**
 * Implementación del DAO Para la entidad Cliente.
 */
public class DAOCotizacionGlobalBD implements DAOCotizacionGlobal {
	static Logger log = Logger.getRootLogger();

	@Override
	public boolean crea(CotizacionGlobal cotizacionGlobal) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String fechaCreacion = sdf.format(cotizacionGlobal.getFechaCreacion() ); 
		String fechaLimite = sdf.format(cotizacionGlobal.getFechaLimite() ); 
		String fechaEntrega = sdf.format(cotizacionGlobal.getFechaEntrega() );
		String queryCrea = "INSERT INTO APP.COTIZACION_GLOBAL VALUES (" +
			"'" + cotizacionGlobal.getFolio() + "'," +
			"'" + cotizacionGlobal.getNumeroSiniestro() + "'," +
			"'" + cotizacionGlobal.getEstado() + "'," +
			"'" + fechaCreacion + "'," +
			"'" + fechaLimite + "', " +
			"'" + fechaEntrega + "'," +
			"" + cotizacionGlobal.getNumeroPiezasTotal()+ "," +
			"" + cotizacionGlobal.getPrecioCostoTotal() + "," +
			"" + cotizacionGlobal.getPrecioVentaTotal() + ", " +
			"'" + cotizacionGlobal.getMarcaAuto() + "'," +
			"'" + cotizacionGlobal.getModeloAuto()+ "'," +
			"'" + cotizacionGlobal.getCliente().getCodigoCliente() + "'" +
		")";
		
		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryCrea);

			return true;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al crear", ex.getRealException());

            return false;
        }

        catch(SQLException e){
    	    log.error("Excepcion de la base de datos al crear", e);

    	    return false;
        }	
	}

	@Override
	public CotizacionGlobal recupera(String folio) {
		String queryRecupera = "SELECT * FROM APP.COTIZACION_GLOBAL "
				+ "WHERE folio = '" + folio + "'";

		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();	
			ResultSet rst = statement.executeQuery(queryRecupera);
			
			if(rst.next()) {
				CotizacionGlobal cotizacionGlobal = new CotizacionGlobal();
				cotizacionGlobal.setFolio(rst.getString("folio"));
				cotizacionGlobal.setNumeroSiniestro(rst.getString("numero_siniestro"));
				cotizacionGlobal.setEstado(rst.getString("estado"));
				cotizacionGlobal.setFechaCreacion(rst.getDate("fecha_creacion"));
				cotizacionGlobal.setFechaLimite(rst.getDate("fecha_limite"));
				cotizacionGlobal.setFechaEntrega(rst.getDate("fecha_entrega"));
				cotizacionGlobal.setNumeroPiezasTotal(rst.getInt("numero_piezas_total"));
				cotizacionGlobal.setPrecioCostoTotal(rst.getDouble("precio_costo_total"));
				cotizacionGlobal.setPrecioVentaTotal(rst.getDouble("precio_venta_total"));
				cotizacionGlobal.setMarcaAuto(rst.getString("marca_auto"));
				cotizacionGlobal.setModeloAuto(rst.getString("modelo_auto"));

				return cotizacionGlobal;
			}

			else
				return null;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar", ex.getRealException());

            return null;
		}

    	catch(SQLException e) {
    		log.error("Excepcion de la base de datos al recuperar", e);

    		return null;
    	}		
	}

	@Override
	public boolean actualiza(CotizacionGlobal cotizacionGlobal) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String fechaCreacion = sdf.format(cotizacionGlobal.getFechaCreacion() ); 
		String fechaLimite = sdf.format(cotizacionGlobal.getFechaLimite() ); 
		String fechaEntrega = sdf.format(cotizacionGlobal.getFechaEntrega() ); 

		String queryActualiza = "UPDATE APP.COTIZACION_GLOBAL SET "
			+ "numero_siniestro = '" + cotizacionGlobal.getNumeroSiniestro() + "', "
			+ "estado = '" + cotizacionGlobal.getEstado() + "', "
			+ "fecha_creacion = '" + fechaCreacion + "', "
			+ "fecha_limite = '" + fechaLimite + "', "
			+ "fecha_entrega = '" + fechaEntrega + "', "
			+ "numero_piezas_total = " + cotizacionGlobal.getNumeroPiezasTotal()+ ", "
			+ "precio_costo_total = " + cotizacionGlobal.getPrecioCostoTotal() + ", "
			+ "precio_venta_total = " + cotizacionGlobal.getPrecioVentaTotal()+ ", "
			+ "marca_auto = '" + cotizacionGlobal.getMarcaAuto()  + "', "
			+ "modelo_auto = '" + cotizacionGlobal.getModeloAuto() + "', "
			+ "codigo_cliente = '" + cotizacionGlobal.getCliente().getCodigoCliente() + "' "
			+ "WHERE folio = '" + cotizacionGlobal.getFolio() + "'";
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryActualiza);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al actualizar",ex.getRealException());

            return false;
		
		}

		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al actualizar",e);

	    	return false;
	    }

		return true;
	}

	@Override
	public boolean borra(CotizacionGlobal cotizacionGlobal) {	
		String queryBorra = "DELETE FROM APP.COTIZACION_GLOBAL "
				+ "WHERE folio = '" + cotizacionGlobal.getFolio() + "'";
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryBorra);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al borrar",ex.getRealException());

            return false;
		}

        catch(SQLException e){
    		log.error("Excepcion de la base de datos al borrar",e);

    		return false;
        }

        return true;
	}

	@Override
	public ArrayList<CotizacionGlobal> recuperaTodas() {
		String queryRecuperaTodos = "SELECT * FROM APP.COTIZACION_GLOBAL";
		ArrayList<CotizacionGlobal> cotizacionesGlobales = new ArrayList<CotizacionGlobal>();
		CotizacionGlobal cotizacionGlobal = null;

		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();			
			ResultSet rst = statement.executeQuery(queryRecuperaTodos);
			
			while(rst.next()) {
				cotizacionGlobal = new CotizacionGlobal();
				cotizacionGlobal.setFolio(rst.getString("folio"));
				cotizacionGlobal.setNumeroSiniestro(rst.getString("numero_siniestro"));
				cotizacionGlobal.setEstado(rst.getString("estado"));
				cotizacionGlobal.setFechaCreacion(rst.getDate("fecha_creacion"));
				cotizacionGlobal.setFechaLimite(rst.getDate("fecha_limite"));
				cotizacionGlobal.setFechaEntrega(rst.getDate("fecha_entrega"));
				cotizacionGlobal.setNumeroPiezasTotal(rst.getInt("numero_piezas_total"));
				cotizacionGlobal.setPrecioCostoTotal(rst.getDouble("precio_costo_total"));
				cotizacionGlobal.setPrecioVentaTotal(rst.getDouble("precio_venta_total"));
				cotizacionGlobal.setMarcaAuto(rst.getString("marca_auto"));
				cotizacionGlobal.setModeloAuto(rst.getString("modelo_auto"));

				cotizacionesGlobales.add(cotizacionGlobal);
			}
			
			if (cotizacionesGlobales.isEmpty())
				return null;

			else
				return cotizacionesGlobales;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar todos", ex.getRealException());

            return null;
		}

   		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al recuperar todos", e);

	    	return null;
    	}	
	}
}