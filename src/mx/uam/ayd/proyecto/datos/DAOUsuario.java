package mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.negocio.dominio.Usuario;

/**
 * DAO Para la entidad Usuario.
 */
public interface DAOUsuario {
	
	/**
	 * boolean <code>crea</code>(Usuario usuario)
	 * <ul>
	 * <li>Agrega un usuario a la base de datos.
	 * </ul>
	 * @param usuario el usuario a agregar.
	 * @return true si se creo exitosamente, false ya existía un usuario con ese id o hubo una excepción al crearlo.
	 */
	public boolean crea(Usuario usuario);
	
	/**
	 * boolean <code>actualiza</code>(Usuario usuario)
	 * <ul>
	 * <li>Actualiza los valores de un usuario existente en la base de datos.
	 * </ul>
	 * @param usuario el usuario a actualizar.
	 * @return true si se actualizo correctamente o no existía el usuario en la base de datos, false si hubo una excepción al actualizarlo.
	 */
	public boolean actualiza(Usuario usuario);

	/**
	 * boolean <code>borra</code>(Usuario usuario)
	 * <ul>
	 * <li>Borra el usuario proporcionado de la base de datos.
	 * </ul>
	 * @param usuario el usuario a borrar de la base de datos.
	 * @return true si se borró exitosamente o no existía el usuario en la base de datos, false si hubo una excepción al borrarlo.
	 */
	public boolean borra(Usuario usuario);
	
	/**
	 * Usuario <code>recupera</code>(String idUsuario)
	 * <ul>
	 * <li>Busca un usuario en la base de datos a partir de su id de usuario.
	 * </ul>
	 * @param idUsuario el id del usuario a buscar.
	 * @return una referencia al usuario o null si no se encontró.
	 */
	public Usuario recupera(String idUsuario);
	
	/**
	 * ArrayList &lt; Usuario &lt; <code>recuperaTodos</code>()
	 * <ul>
	 * <li>Recupera todos los usuarios actualmente en la base de datos y los regresa en un ArrayList de usuarios.
	 * </ul>
	 * @return un ArrayList con todos los usuarios en la base de datos
	 * o null si no hay usuarios en la base de datos.
	 */
	public ArrayList<Usuario> recuperaTodos();
}
