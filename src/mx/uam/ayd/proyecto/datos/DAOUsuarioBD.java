package mx.uam.ayd.proyecto.datos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import mx.uam.ayd.proyecto.negocio.dominio.Usuario;

/**
 * Implementación del DAO Para la entidad Usuario.
 */
public class DAOUsuarioBD implements DAOUsuario {
	
	static Logger log = Logger.getRootLogger();

	@Override
	public boolean crea(Usuario usuario) {
		int privilegios;
		
		if (usuario.isPrivilegiado())
			privilegios = 1;
		else
			privilegios = 0;
		
		Connection connection = ManejadorBaseDatos.getConnection();
		try {
			Statement statement = connection.createStatement();
			String queryCrea = "INSERT INTO APP.USUARIO VALUES (" +
			"'" + usuario.getIdUsuario() + "'," +
			"'" + usuario.getNombre() + "'," +
			"'" + usuario.getApellidoPaterno() + "'," +
			"'" + usuario.getApellidoMaterno()+ "',"   +
			"'" + usuario.getRfc() + "'," +
			"'" + usuario.getDireccion() + "'," +
			"'" + usuario.getTelefono() + "'," +
			"'" + usuario.getCorreoElectronico() + "'," +
			"'" + usuario.getContrasena() + "'," +
			privilegios +
			")";
			
			statement.execute(queryCrea);
			return true;
		}catch(DatabaseException ex)
	    {
            log.error("Excepcion de la base de datos al crear",ex.getRealException());
            return false;
    }
    catch(SQLException e){
    	log.error("Excepcion de la base de datos al crear",e);
    	return false;
    }	
		
	}

	@Override
	public Usuario recupera(String nombre){
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			String queryRecupera = "SELECT * FROM APP.USUARIO WHERE id_usuario = '" + nombre + "'";
			
			// Obteniendo el rst correspondiente a la query hecha al sistema del usuario proporcionado en nombre.
			ResultSet rst = statement.executeQuery(queryRecupera);
			
			// Obtenemos el primer elemento del rst.
			boolean haySiguiente = rst.next();
			
			if (haySiguiente) {
				// Creamos nuevo usuario y lo regresamos.
				return new Usuario(rst.getString("id_usuario"),rst.getString("nombre"),rst.getString("apellido_paterno"),rst.getString("apellido_materno"),rst.getString("rfc"),rst.getString("direccion"),rst.getString("telefono"),rst.getString("correo_electronico"),rst.getString("contrasena"),rst.getBoolean("privilegiado"));
			}
			else
				return null;
			
		}catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar",ex.getRealException());
            return null;
		}
    catch(SQLException e){
    	log.error("Excepcion de la base de datos al recuperar",e);
    	return null;
    }	
		
	}

	@Override
	public boolean actualiza(Usuario usuario) {
		int privilegios;
		
		if (usuario.isPrivilegiado())
			privilegios = 1;
		else
			privilegios = 0;
		
		String queryActualiza = "UPDATE APP.USUARIO SET " + 
				"nombre = '" + usuario.getNombre() + "' " +
				",apellido_paterno = '" + usuario.getApellidoPaterno() + "' " +
				",apellido_materno = '" + usuario.getApellidoMaterno()+ "' "   +
				",rfc = '" + usuario.getRfc() + "'" +
				",direccion = '" + usuario.getDireccion() + "'" +
				",telefono = '" + usuario.getTelefono() + "'" +
				",correo_electronico = '" + usuario.getCorreoElectronico() + "'" +
				",contrasena = '" + usuario.getContrasena() + "'" +
				",privilegiado = " + privilegios  +
				"  WHERE id_usuario = '" + usuario.getIdUsuario() + "'";
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryActualiza);
			
		}catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al actualizar",ex.getRealException());
            return false;
		}catch(SQLException e){
	    	log.error("Excepcion de la base de datos al actualizar",e);
	    	return false;
	    }	
		return true;
	}

	@Override
	public boolean borra(Usuario usuario) {
					
		String queryBorra = "DELETE FROM APP.USUARIO WHERE id_usuario = '" + usuario.getIdUsuario() + "'";
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			// Regresamos el resultado de la query.
			return statement.execute(queryBorra);
			
		}catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al borrar",ex.getRealException());
            return false;
		}
    catch(SQLException e){
    	log.error("Excepcion de la base de datos al borrar",e);
    	return false;
    }	
	}

	@Override
	public ArrayList<Usuario> recuperaTodos() {
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			String queryRecuperaTodos = "SELECT * FROM APP.USUARIO";
			
			// Obteniendo el rst correspondiente a la query hecha al sistema del usuario proporcionado en nombre.
			ResultSet rst = statement.executeQuery(queryRecuperaTodos);
			
			// Iteramos sobre rst para llenar el ArrayList de Usuarios.
			while(rst.next()) {
				usuarios.add(new Usuario(rst.getString("id_usuario"),rst.getString("nombre"),rst.getString("apellido_paterno"),rst.getString("apellido_materno"),rst.getString("rfc"),rst.getString("direccion"),rst.getString("telefono"),rst.getString("correo_electronico"),rst.getString("contrasena"),rst.getBoolean("privilegiado")));
			}
			
			if (usuarios.isEmpty())
				return null;
			else
				return usuarios;	
			
		}catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar todos",ex.getRealException());
            return null;
		}
    catch(SQLException e){
    	log.error("Excepcion de la base de datos al recuperar todos",e);
    	return null;
    }	
	}

}
