package mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;

/**
 * DAO Para la entidad SubCotizacion.
 */
public interface DAOSubCotizacion {
	
	/**
	 * boolean <code>crea</code>(SubCotizacion subCotizacion)
	 * <ul>
	 * <li>Agrega una sub cotizacion a la base de datos.
	 * </ul>
	 * @param subCotizacion la sub cotizacion a agregar.
	 * @return true si se creo exitosamente, false ya existía una subcotizacion con ese id o hubo una excepción al crearlo.
	 */
	public boolean crea(SubCotizacion subCotizacion);
	
	/**
	 * SubCotizacion <code>recupera</code>(String subFolio)
	 * <ul>
	 * <li>Recupera una sub cotizacion a partir de su sub folio de la base de datos.
	 * </ul>
	 * @param subFolio el sub folio de la sub cotizacion a recuperar.
	 * @return una referencia a la sub cotizacion o null si no se encontró.
	 */
	public SubCotizacion recupera(String subFolio);
	
	/**
	 * boolean <code>actualiza</code>(SubCotizacion subCotizacion)
	 * <ul>
	 * <li>Actualiza los valores de una sub cotizacion existente en la base de datos.
	 * </ul>
	 * @param subCotizacion la sub cotizacion a actualizar.
	 * @return true si se actualizo correctamente o no existía la sub cotización en la base de datos, false si hubo una excepción al actualizarla.
	 */
	public boolean actualiza(SubCotizacion subCotizacion);

	/**
	 * boolean <code>borra</code>(SubCotizacion subCotizacion)
	 * <ul>
	 * <li>Borra la sub cotizacion proporcionada de la base de datos.
	 * </ul>
	 * @param subCotizacion la sub cotizacion a borrar.
	 * @return true si se borró exitosamente o no existía la sub cotización en la base de datos, false si hubo una excepción al borrarla.
	 */
	public boolean borra(SubCotizacion subCotizacion);
	
	/**
	 * ArrayList &lt; SubCotizacion &gt; <code>recuperaTodas</code>()
	 * <ul>
	 * <li>Regresa una lista con todas las sub cotizaciones en la base de datos.
	 * </ul>
	 * @return un ArrayList con todas las subcotizaciones en la base de datos.
	 * o null si no hay subcotizaciones en la base de datos.
	 */
	public ArrayList<SubCotizacion> recuperaTodas();
	
	/**
	 * ArrayList &lt; SubCotizacion &gt; <code>recuperaTodas</code>(String folioCotizacionGlobal)
	 * <ul>
	 * <li>Regresa una lista con todas las sub cotizaciones en la base de datos, que tienen el mismo folio de cotización global.
	 * </ul>
	 * @param folioCotizacionGlobal el folio de la cotización global por el cual filtrar.
	 * @return un ArrayList con todas las sub cotizaciones en la base de datos que tienen el mismo folio de cotización global,
	 * o null si no hay sub cotizaciones en la base de datos con ese folio.
	 */
	public ArrayList<SubCotizacion> recuperaTodas(String folioCotizacionGlobal);
}
