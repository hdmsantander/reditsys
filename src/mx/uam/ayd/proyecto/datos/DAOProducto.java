package mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;
import mx.uam.ayd.proyecto.negocio.dominio.Producto;

/**
 * DAO Para la entidad Producto.
 */
public interface DAOProducto {
	/**
	 * boolean <code>crea</code>(Producto producto, SubCotizacion subCotizacion)
	 * <ul>
	 * <li>Agrega un producto a la base de datos.
	 * </ul>
	 * @param producto el proveedor a agregar.
	 * @return true si se creo exitosamente, false ya existía un producto con ese id o hubo una excepción al crearlo.
	 */
	public boolean crea(Producto producto);
	
	/**
	 * Producto <code>recupera</code>(int numeroProducto)
	 * <ul>
	 * <li>Recupera un producto a partir del numero de producto de la base de datos.
	 * </ul>
	 * @param numeroProducto el numero de producto del producto a recuperar.
	 * @return una referencia al producto o null si no se encontró.
	 */
	public Producto recupera(int numeroProducto);
	
	/**
	 * boolean <code>actualiza</code>(Producto producto)
	 * <ul>
	 * <li>Actualiza los valores de un producto existente en la base de datos.
	 * </ul>
	 * @param producto el producto a actualizar.
	 * @return true si se actualizo correctamente o no existía el producto en la base de datos, false si hubo una excepción al actualizarlo.
	 */
	public boolean actualiza(Producto producto);

	/**
	 * boolean <code>borra</code>(Producto producto)
	 * <ul>
	 * <li>Borra el producto proporcionado de la base de datos.
	 * </ul>
	 * @param producto el producto a borrar.
	 * @return true si se borró exitosamente o no existía el producto en la base de datos, false si hubo una excepción al borrarlo.
	 */
	public boolean borra(Producto producto);
	
	/**
	 * ArrayList &lt; Producto &gt; <code>recuperaTodos</code>()
	 * <ul>
	 * <li>Regresa una lista con todos los productos en la base de datos.
	 * </ul>
	 * @return un ArrayList con todos los productos en la base de datos.
	 * o null si no hay productos en la base de datos.
	 */
	public ArrayList<Producto> recuperaTodos();

	/**
	 * ArrayList &lt; Producto &gt; <code>recuperaTodos</code>(SubCotizacion subCotizacion)
	 * <ul>
	 * <li>Regresa una lista con todos los productos en la base de datos, que tienen el mismo sub folio de sub cotización.
	 * </ul>
	 * @param subCotizacion la subcotización por la cual realizar el filtrado de productos.
	 * @return un ArrayList con todos las productos en la base de datos que tienen el mismo sub folio de sub cotización,
	 * o null si no hay productos en la base de datos con ese sub folio.
	 */
	public ArrayList<Producto> recuperaTodos(SubCotizacion subCotizacion);
}
