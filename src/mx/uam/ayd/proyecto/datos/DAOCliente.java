package mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.negocio.dominio.Cliente;

/**
 * DAO Para la entidad Cliente.
 */
public interface DAOCliente {
	
	/**
	 * boolean <code>crea</code>(Cliente cliente)
	 * <ul>
	 * <li>Agrega un cliente a la base de datos.
	 * </ul>
	 * @param cliente el cliente a agregar.
	 * @return true si se creo exitosamente, false ya existía un cliente con ese id o hubo una excepción al crearlo.
	 */
	public boolean crea(Cliente cliente);
	
	/**
	 * Cliente <code>recupera</code>(String codigoCliente)
	 * <ul>
	 * <li>Recupera un cliente a partir del numero de cliente de la base de datos.
	 * </ul>
	 * @param codigoCliente el numero de cliente del proveedor a recuperar.
	 * @return una referencia al cliente o null si no se encontró.
	 */
	public Cliente recupera(String codigoCliente);
	
	/**
	 * boolean <code>actualiza</code>(Cliente cliente)
	 * <ul>
	 * <li>Actualiza los valores de un cliente existente en la base de datos.
	 * </ul>
	 * @param cliente el cliente a actualizar.
	 * @return true si se actualizo correctamente o no existía el cliente en la base de datos, false si hubo una excepción al actualizarlo.
	 */
	public boolean actualiza(Cliente cliente);

	/**
	 * boolean <code>borra</code>(Cliente cliente)
	 * <ul>
	 * <li>Borra el cliente proporcionado de la base de datos.
	 * </ul>
	 * @param cliente el cliente a borrar.
	 * @return true si se borró exitosamente o no existía el cliente en la base de datos, false si hubo una excepción al borrarlo.
	 */
	public boolean borra(Cliente cliente);
	
	/**
	 * ArrayList &lt; Cliente &gt;  <code>recuperaTodos</code>()
	 * <ul>
	 * <li>Regresa una lista con todos los cliente en la base de datos.
	 * </ul>
	 * @return un ArrayList con todos los cliente en la base de datos.
	 * o null si no hay cliente en la base de datos.
	 */
	public ArrayList<Cliente> recuperaTodos();
}
