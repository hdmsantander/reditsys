package mx.uam.ayd.proyecto.datos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;

/**
 * Implementación del DAO Para la entidad SubCotizacion.
 */
public class DAOSubCotizacionBD implements DAOSubCotizacion {
	static Logger log = Logger.getRootLogger();

	@Override
	public boolean crea(SubCotizacion subCotizacion) {
		String queryCrea = "INSERT INTO APP.SUBCOTIZACION VALUES (" +
			"'" + subCotizacion.getSubFolio() + "'," +
			"'" + subCotizacion.getEstado() + "'," +
			"" + subCotizacion.getNumeroPiezas()+ "," +
			"" + subCotizacion.getPrecioCostoSubTotal() + "," +
			"" + subCotizacion.getPrecioVentaSubTotal() + ", " +
			"'" + subCotizacion.getCotizacionGlobal().getFolio() + "', " +
			"'" + subCotizacion.getUsuario().getIdUsuario() + "'" +
		")";
		
		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryCrea);

			return true;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al crear", ex.getRealException());

            return false;
        }

        catch(SQLException e){
    	    log.error("Excepcion de la base de datos al crear", e);

    	    return false;
        }	
	}

	@Override
	public SubCotizacion recupera(String subFolio) {
		String queryRecupera = "SELECT * FROM APP.SUBCOTIZACION "
				+ "WHERE sub_folio = '" + subFolio + "'";

		try {
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();	
			ResultSet rst = statement.executeQuery(queryRecupera);
			
			if(rst.next()) {
				SubCotizacion subCotizacion = new SubCotizacion();
				subCotizacion.setSubFolio(rst.getString("sub_folio"));
				subCotizacion.setEstado(rst.getString("estado"));
				subCotizacion.setNumeroPiezas(rst.getInt("numero_piezas"));
				subCotizacion.setPrecioCostoSubTotal(rst.getDouble("precio_costo_subtotal"));
				subCotizacion.setPrecioVentaSubTotal(rst.getDouble("precio_venta_subtotal"));

				return subCotizacion;
			}

			else
				return null;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar", ex.getRealException());

            return null;
		}

    	catch(SQLException e) {
    		log.error("Excepcion de la base de datos al recuperar", e);

    		return null;
    	}		
	}

	@Override
	public boolean actualiza(SubCotizacion subCotizacion) {
		String queryActualiza = "UPDATE APP.SUBCOTIZACION SET "
			+ "estado = '" + subCotizacion.getEstado()+ "', "
			+ "numero_piezas = " + subCotizacion.getNumeroPiezas() + ", "
			+ "precio_costo_subtotal = " + subCotizacion.getPrecioCostoSubTotal() + ", "
			+ "precio_venta_subtotal = " + subCotizacion.getPrecioVentaSubTotal() + ", "
			+ "folio = '" + subCotizacion.getCotizacionGlobal().getFolio() + "', "
			+ "id_usuario = '" + subCotizacion.getUsuario().getIdUsuario() + "' "
			+ "WHERE sub_folio = '" + subCotizacion.getSubFolio() + "'";
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryActualiza);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al actualizar",ex.getRealException());

            return false;
		
		}

		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al actualizar",e);

	    	return false;
	    }

		return true;
	}

	@Override
	public boolean borra(SubCotizacion subCotizacion) {	
		String queryBorra = "DELETE FROM APP.SUBCOTIZACION "
				+ "WHERE sub_folio = '" + subCotizacion.getSubFolio() + "'";
		
		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			statement.execute(queryBorra);
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al borrar",ex.getRealException());

            return false;
		}

        catch(SQLException e){
    		log.error("Excepcion de la base de datos al borrar",e);

    		return false;
        }

        return true;
	}

	@Override
	public ArrayList<SubCotizacion> recuperaTodas() {
		String queryRecuperaTodos = "SELECT * FROM APP.SUBCOTIZACION";
		ArrayList<SubCotizacion> subCotizaciones = new ArrayList<SubCotizacion>();
		SubCotizacion subCotizacion = null;

		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();			
			ResultSet rst = statement.executeQuery(queryRecuperaTodos);
			
			while(rst.next()) {
				subCotizacion = new SubCotizacion();
				subCotizacion.setSubFolio(rst.getString("sub_folio"));
				subCotizacion.setEstado(rst.getString("estado"));
				subCotizacion.setNumeroPiezas(rst.getInt("numero_piezas"));
				subCotizacion.setPrecioCostoSubTotal(rst.getDouble("precio_costo_subtotal"));
				subCotizacion.setPrecioVentaSubTotal(rst.getDouble("precio_venta_subtotal"));

				subCotizaciones.add(subCotizacion);
			}
			
			if (subCotizaciones.isEmpty())
				return null;

			else
				return subCotizaciones;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar todos", ex.getRealException());

            return null;
		}

   		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al recuperar todos", e);

	    	return null;
    	}	
	}

	@Override
	public ArrayList<SubCotizacion> recuperaTodas(String folioCotizacionGlobal) {
		String queryRecuperaTodos = "SELECT * FROM APP.SUBCOTIZACION WHERE folio = '" + folioCotizacionGlobal + "'";
		ArrayList<SubCotizacion> subCotizaciones = new ArrayList<SubCotizacion>();
		SubCotizacion subCotizacion = null;

		try {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();			
			ResultSet rst = statement.executeQuery(queryRecuperaTodos);
			
			while(rst.next()) {
				subCotizacion = new SubCotizacion();
				subCotizacion.setSubFolio(rst.getString("sub_folio"));
				subCotizacion.setEstado(rst.getString("estado"));
				subCotizacion.setNumeroPiezas(rst.getInt("numero_piezas"));
				subCotizacion.setPrecioCostoSubTotal(rst.getDouble("precio_costo_subtotal"));
				subCotizacion.setPrecioVentaSubTotal(rst.getDouble("precio_venta_subtotal"));

				subCotizaciones.add(subCotizacion);
			}
			
			if (subCotizaciones.isEmpty())
				return null;

			else
				return subCotizaciones;
		}

		catch(DatabaseException ex) {
            log.error("Excepcion de la base de datos al recuperar todos", ex.getRealException());

            return null;
		}

   		catch(SQLException e){
	    	log.error("Excepcion de la base de datos al recuperar todos", e);

	    	return null;
    	}	
	}
}