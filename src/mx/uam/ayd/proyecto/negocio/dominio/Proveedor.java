package mx.uam.ayd.proyecto.negocio.dominio;

/**
 * Clase de la entidad Proveedor.
 */
public class Proveedor {
	private int numeroProveedor;
	private String nombre;
	private String rfc;
	private String direccion;
	private String telefono;
	private String correoElectronico;
	private String descripcion;
	
	public Proveedor() {}

	/**
	 * @param numeroProveedor el número de proveedor
	 * @param nombre el nombre
	 * @param rfc el rfc
	 * @param direccion la dirección
	 * @param telefono el teléfono
	 * @param correoElectronico el correo electrónico
	 * @param descripcion la descripción
	 */
	public Proveedor(int numeroProveedor, String nombre, String rfc, String direccion, String telefono,
			String correoElectronico, String descripcion) {
		this.numeroProveedor = numeroProveedor;
		this.nombre = nombre;
		this.rfc = rfc;
		this.direccion = direccion;
		this.telefono = telefono;
		this.correoElectronico = correoElectronico;
		this.descripcion = descripcion;
	}

	/**
	 * @return the numeroProveedor
	 */
	public int getNumeroProveedor() {
		return numeroProveedor;
	}

	/**
	 * @param numeroProveedor the numeroProveedor to set
	 */
	public void setNumeroProveedor(int numeroProveedor) {
		this.numeroProveedor = numeroProveedor;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * @param rfc the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the correoElectronico
	 */
	public String getCorreoElectronico() {
		return correoElectronico;
	}

	/**
	 * @param correoElectronico the correoElectronico to set
	 */
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}