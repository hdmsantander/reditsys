package mx.uam.ayd.proyecto.negocio.dominio;

/**
 * Clase de la entidad Producto.
 */
public class Producto {
	private int numeroProducto;
	private String descripcion;
	private int cantidad;
	private double precioCostoUnitario;
	private double precioVentaUnitario;
	private SubCotizacion subCotizacion;
	private TipoProducto tipoProducto;
	private Proveedor proveedor;
	
	public Producto() {}
	
	/**
	 * @param numeroProducto el numero de producto
	 * @param descripcion la descripción del producto
	 * @param cantidad la cantidad
	 * @param precioCostoUnitario el costo unitario
	 * @param precioVentaUnitario el valor unitario
	 * @param tipoProducto el tipo de producto
	 * @param proveedor el proveedor
	 * @param subCotizacion la subcotizacion correspondiente
	 */
	public Producto(int numeroProducto, String descripcion, int cantidad, double precioCostoUnitario,
			double precioVentaUnitario, SubCotizacion subCotizacion, TipoProducto tipoProducto, Proveedor proveedor) {
		this.numeroProducto = numeroProducto;
		this.descripcion = descripcion;
		this.cantidad = cantidad;
		this.precioCostoUnitario = precioCostoUnitario;
		this.precioVentaUnitario = precioVentaUnitario;
		this.subCotizacion = subCotizacion;
		this.tipoProducto = tipoProducto;
		this.proveedor = proveedor;
	}

	/**
	 * @return the numeroProducto
	 */
	public int getNumeroProducto() {
		return numeroProducto;
	}

	/**
	 * @param numeroProducto the numeroProducto to set
	 */
	public void setNumeroProducto(int numeroProducto) {
		this.numeroProducto = numeroProducto;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the precioCostoUnitario
	 */
	public double getPrecioCostoUnitario() {
		return precioCostoUnitario;
	}

	/**
	 * @param precioCostoUnitario the precioCostoUnitario to set
	 */
	public void setPrecioCostoUnitario(double precioCostoUnitario) {
		this.precioCostoUnitario = precioCostoUnitario;
	}

	/**
	 * @return the precioVentaUnitario
	 */
	public double getPrecioVentaUnitario() {
		return precioVentaUnitario;
	}

	/**
	 * @param precioVentaUnitario the precioVentaUnitario to set
	 */
	public void setPrecioVentaUnitario(double precioVentaUnitario) {
		this.precioVentaUnitario = precioVentaUnitario;
	}
	
	public void setSubCotizacion(SubCotizacion subCotizacion) {
		this.subCotizacion = subCotizacion;
	}
	
	public SubCotizacion getSubCotizacion() {
		return this.subCotizacion;
	}

	/**
	 * @return the tipoProducto
	 */
	public TipoProducto getTipoProducto() {
		return tipoProducto;
	}

	/**
	 * @param tipoProducto the tipoProducto to set
	 */
	public void setTipoProducto(TipoProducto tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	/**
	 * @return the proveedor
	 */
	public Proveedor getProveedor() {
		return proveedor;
	}

	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}	
}