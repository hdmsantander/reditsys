package mx.uam.ayd.proyecto.negocio.dominio;

/**
 * Clase de la entidad Cliente.
 */
public class Cliente {
	private String codigoCliente;
	private String nombre;
	private String rfc;
	private String direccion;
	private String telefono;
	private String correoElectronico;
	
	public Cliente() {}

	/**
	 * @param codigoCliente el código del cliente.
	 * @param nombre el nombre que tiene.
	 * @param rfc el rfc.
	 * @param direccion la dirección.
	 * @param telefono el teléfono.
	 * @param correoElectronico el correo electrónico correspondiente.
	 */
	public Cliente(String codigoCliente, String nombre, String rfc, String direccion, String telefono,
			String correoElectronico) {
		this.codigoCliente = codigoCliente;
		this.nombre = nombre;
		this.rfc = rfc;
		this.direccion = direccion;
		this.telefono = telefono;
		this.correoElectronico = correoElectronico;
	}

	/**
	 * @return the codigoCliente
	 */
	public String getCodigoCliente() {
		return codigoCliente;
	}

	/**
	 * @param codigoCliente the codigoCliente to set
	 */
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * @param rfc the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the correoElectronico
	 */
	public String getCorreoElectronico() {
		return correoElectronico;
	}

	/**
	 * @param correoElectronico the correoElectronico to set
	 */
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}	
}