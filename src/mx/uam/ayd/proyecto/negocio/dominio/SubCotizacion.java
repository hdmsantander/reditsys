package mx.uam.ayd.proyecto.negocio.dominio;

/**
 * Clase de la entidad SubCotizacion.
 */
public class SubCotizacion {
	private String subFolio;
	private String estado;
	private int numeroPiezas;
	private double precioCostoSubTotal;
	private double precioVentaSubTotal;
	private Usuario usuario;
	private CotizacionGlobal cotizacionGlobal;
	private Producto [] productos;
	
	public SubCotizacion() {}

	/**
	 * @param subFolio el folio
	 * @param estado el estado de la sub cotizacion
	 * @param numeroPiezas el número de piezas
	 * @param precioCostoSubTotal el costo subtotal
	 * @param precioVentaSubTotal el precio de venta subtotal
	 * @param usuario el usuario
	 * @param cotizacionGlobal la cotización global relacionada
	 * @param productos los productos relacionados
	 */
	public SubCotizacion(String subFolio, String estado, int numeroPiezas, double precioCostoSubTotal,
			double precioVentaSubTotal, Usuario usuario, CotizacionGlobal cotizacionGlobal, Producto[] productos) {
		this.subFolio = subFolio;
		this.estado = estado;
		this.numeroPiezas = numeroPiezas;
		this.precioCostoSubTotal = precioCostoSubTotal;
		this.precioVentaSubTotal = precioVentaSubTotal;
		this.usuario = usuario;
		this.cotizacionGlobal = cotizacionGlobal;
		this.productos = productos;
	}

	/**
	 * @return the subFolio
	 */
	public String getSubFolio() {
		return subFolio;
	}

	/**
	 * @param subFolio the subFolio to set
	 */
	public void setSubFolio(String subFolio) {
		this.subFolio = subFolio;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the numeroPiezas
	 */
	public int getNumeroPiezas() {
		return numeroPiezas;
	}

	/**
	 * @param numeroPiezas the numeroPiezas to set
	 */
	public void setNumeroPiezas(int numeroPiezas) {
		this.numeroPiezas = numeroPiezas;
	}

	/**
	 * @return the precioCostoSubTotal
	 */
	public double getPrecioCostoSubTotal() {
		return precioCostoSubTotal;
	}

	/**
	 * @param precioCostoSubTotal the precioCostoSubTotal to set
	 */
	public void setPrecioCostoSubTotal(double precioCostoSubTotal) {
		this.precioCostoSubTotal = precioCostoSubTotal;
	}

	/**
	 * @return the precioVentaSubTotal
	 */
	public double getPrecioVentaSubTotal() {
		return precioVentaSubTotal;
	}

	/**
	 * @param precioVentaSubTotal the precioVentaSubTotal to set
	 */
	public void setPrecioVentaSubTotal(double precioVentaSubTotal) {
		this.precioVentaSubTotal = precioVentaSubTotal;
	}

	/**
	 * @return the usuario
	 */
	public Usuario getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the cotizacionGlobal
	 */
	public CotizacionGlobal getCotizacionGlobal() {
		return cotizacionGlobal;
	}

	/**
	 * @param cotizacionGlobal the cotizacionGlobal to set
	 */
	public void setCotizacionGlobal(CotizacionGlobal cotizacionGlobal) {
		this.cotizacionGlobal = cotizacionGlobal;
	}

	/**
	 * @return the productos
	 */
	public Producto[] getProductos() {
		return productos;
	}

	/**
	 * @param productos the productos to set
	 */
	public void setProductos(Producto[] productos) {
		this.productos = productos;
	}
}