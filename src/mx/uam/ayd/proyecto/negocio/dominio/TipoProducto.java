package mx.uam.ayd.proyecto.negocio.dominio;

/**
 * Clase de la entidad TipoProducto.
 */
public class TipoProducto {
	private int numeroTipoProducto;
	private String nombre;
	
	public TipoProducto() {}

	/**
	 * @param numeroTipoProducto el número del tipo de producto.
	 * @param nombre el nombre del producto.
	 */
	public TipoProducto(int numeroTipoProducto, String nombre) {
		this.numeroTipoProducto = numeroTipoProducto;
		this.nombre = nombre;
	}

	/**
	 * @return the numeroTipoProducto
	 */
	public int getNumeroTipoProducto() {
		return numeroTipoProducto;
	}

	/**
	 * @param numeroTipoProducto the numeroTipoProducto to set
	 */
	public void setNumeroTipoProducto(int numeroTipoProducto) {
		this.numeroTipoProducto = numeroTipoProducto;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}