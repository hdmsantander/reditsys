package mx.uam.ayd.proyecto.negocio.dominio;

import java.util.Date;

/**
 * Clase de la entidad CotizacionGlobal.
 */
public class CotizacionGlobal {
	private String folio;
	private String numeroSiniestro;
	private String estado;
	private Date fechaCreacion;
	private Date fechaLimite;
	private Date fechaEntrega;
	private int numeroPiezasTotal;
	private double precioCostoTotal;
	private double precioVentaTotal;
	private String marcaAuto;
	private String modeloAuto;
	private Cliente cliente;
	private SubCotizacion [] subCotizaciones;
	
	public CotizacionGlobal() {}
	
	/**
	 * @param folio el folio.
	 * @param numeroSiniestro el número del siniestro.
	 * @param estado el estado.
	 * @param fechaCreacion la fecha de la creación.
	 * @param fechaLimite la fecha límite.
	 * @param fechaEntrega la fecha de entrega.
	 * @param numeroPiezasTotal el número de piezas en total.
	 * @param precioCostoTotal el costo total.
	 * @param precioVentaTotal el precio total de la venta.
	 * @param marcaAuto la marca del auto.
	 * @param modeloAuto el modelo del auto.
	 * @param cliente el cliente.
	 * @param subCotizaciones las subcotizaciones de la cotizacion global.
	 */
	public CotizacionGlobal(String folio, String numeroSiniestro, String estado, Date fechaCreacion, Date fechaLimite,
			Date fechaEntrega, int numeroPiezasTotal, double precioCostoTotal, double precioVentaTotal,
			String marcaAuto, String modeloAuto, Cliente cliente, SubCotizacion[] subCotizaciones) {
		this.folio = folio;
		this.numeroSiniestro = numeroSiniestro;
		this.estado = estado;
		this.fechaCreacion = fechaCreacion;
		this.fechaLimite = fechaLimite;
		this.fechaEntrega = fechaEntrega;
		this.numeroPiezasTotal = numeroPiezasTotal;
		this.precioCostoTotal = precioCostoTotal;
		this.precioVentaTotal = precioVentaTotal;
		this.marcaAuto = marcaAuto;
		this.modeloAuto = modeloAuto;
		this.cliente = cliente;
		this.subCotizaciones = subCotizaciones;
	}



	/**
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * @param folio the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * @return the fechaLimite
	 */
	public Date getFechaLimite() {
		return fechaLimite;
	}

	/**
	 * @param fechaLimite the fechaLimite to set
	 */
	public void setFechaLimite(Date fechaLimite) {
		this.fechaLimite = fechaLimite;
	}

	/**
	 * @return the fechaEntrega
	 */
	public Date getFechaEntrega() {
		return fechaEntrega;
	}

	/**
	 * @param fechaEntrega the fechaEntrega to set
	 */
	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	/**
	 * @return the numeroPiezasTotal
	 */
	public int getNumeroPiezasTotal() {
		return numeroPiezasTotal;
	}

	/**
	 * @param numeroPiezasTotal the numeroPiezasTotal to set
	 */
	public void setNumeroPiezasTotal(int numeroPiezasTotal) {
		this.numeroPiezasTotal = numeroPiezasTotal;
	}

	/**
	 * @return the precioCostoTotal
	 */
	public double getPrecioCostoTotal() {
		return precioCostoTotal;
	}

	/**
	 * @param precioCostoTotal the precioCostoTotal to set
	 */
	public void setPrecioCostoTotal(double precioCostoTotal) {
		this.precioCostoTotal = precioCostoTotal;
	}

	/**
	 * @return the precioVentaTotal
	 */
	public double getPrecioVentaTotal() {
		return precioVentaTotal;
	}

	/**
	 * @param precioVentaTotal the precioVentaTotal to set
	 */
	public void setPrecioVentaTotal(double precioVentaTotal) {
		this.precioVentaTotal = precioVentaTotal;
	}

	/**
	 * @return the marcaAuto
	 */
	public String getMarcaAuto() {
		return marcaAuto;
	}

	/**
	 * @param marcaAuto the marcaAuto to set
	 */
	public void setMarcaAuto(String marcaAuto) {
		this.marcaAuto = marcaAuto;
	}

	/**
	 * @return the modeloAuto
	 */
	public String getModeloAuto() {
		return modeloAuto;
	}

	/**
	 * @param modeloAuto the modeloAuto to set
	 */
	public void setModeloAuto(String modeloAuto) {
		this.modeloAuto = modeloAuto;
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the subCotizaciones
	 */
	public SubCotizacion[] getSubCotizaciones() {
		return subCotizaciones;
	}

	/**
	 * @param subCotizaciones the subCotizaciones to set
	 */
	public void setSubCotizaciones(SubCotizacion[] subCotizaciones) {
		this.subCotizaciones = subCotizaciones;
	}
}
