package mx.uam.ayd.proyecto.negocio;

import java.util.ArrayList;
import mx.uam.ayd.proyecto.negocio.dominio.Usuario;

/**
 * Módulo de servicio para los usuarios.
 */
public interface ServicioUsuario {
	
	
	//void ServicioUsuarioImpl(DAOUsuario dao);
	
	 /**
    *Metodo ComparaContrasena
    *Compara la contraseña ingresada por el usuario con la que se tiene en la base de datos
    *regresa un true en caso de que sean igual, false en caso contrario
    *@param u el usuario a ingresar
    *@param contra la contraseña a verificar
    *@return true si la operación fue exitosa, false si no.
    **/

public boolean ComparaContrasena(Usuario u,String contra);

/**
 * Metodo validaUusuario
   *Valida la existencia de un usuario, si no existe regresa false
   *llama al metodo ComparaContrasena, si el metodo regresa true, se regresa un true, caso contrario false
   *@param ContrasenaCapturada contraseña capturada para su verificación
   *@param idusuario el id del usuario a validar.
   *@return true si tuvo éxito la operación, false si no.
**/

boolean validaUsuario(String idusuario,String ContrasenaCapturada);

    /**
    *Regresa una lista con todos los usuarios
    * 
    *@return un arraylist con todos los usuarios.
    * */

public ArrayList<Usuario> recuperaTodosUsuarios();

public boolean ActualizaUsuario(Usuario u);

public boolean borraUsuario(Usuario u);

public boolean crearU(Usuario u);

public Usuario RecuperaUsuario(String idusuario);

}
