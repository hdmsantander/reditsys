package mx.uam.ayd.proyecto.negocio;

import mx.uam.ayd.proyecto.datos.DAOCotizacionGlobal;
import mx.uam.ayd.proyecto.datos.DAOSubCotizacion;
import mx.uam.ayd.proyecto.datos.DAOProducto;
import mx.uam.ayd.proyecto.datos.DAOProveedor;
import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;
import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;
import mx.uam.ayd.proyecto.negocio.dominio.Cliente;
import mx.uam.ayd.proyecto.negocio.dominio.Usuario;
import mx.uam.ayd.proyecto.negocio.dominio.Producto;
import mx.uam.ayd.proyecto.negocio.dominio.TipoProducto;
import mx.uam.ayd.proyecto.negocio.dominio.Proveedor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Set;

public class ServicioCotizacionGlobalImpl implements ServicioCotizacionGlobal {
	private DAOCotizacionGlobal daoCotizacionGlobal;
	private DAOSubCotizacion daoSubCotizacion;
	private DAOProducto daoProducto;
	private DAOProveedor daoProveedor;
	
	public ServicioCotizacionGlobalImpl(DAOCotizacionGlobal daoCotizacionGlobal) {
		// Creamos conexion al DAO
		this.daoCotizacionGlobal = daoCotizacionGlobal;
	}
	
	public ServicioCotizacionGlobalImpl(DAOCotizacionGlobal daoCotizacionGlobal, DAOSubCotizacion daoSubCotizacion,
			DAOProducto daoProducto, DAOProveedor daoProveedor) {
		// Creamos conexion al DAO
		this.daoCotizacionGlobal = daoCotizacionGlobal;
		this.daoSubCotizacion = daoSubCotizacion;
		this.daoProducto = daoProducto;
		this.daoProveedor = daoProveedor;
	}

	public boolean validaExisteCotizacionGlobal(String folio) {
		try {
			CotizacionGlobal cotizacionGlobal = daoCotizacionGlobal.recupera(folio);
			
			if(cotizacionGlobal != null) 
				return true;

			else 
				return false;
		}

		catch(Exception e) {
			return false;
		}
	}

	public boolean creaCotizacionGlobal(CotizacionGlobal cotizacionGlobal) {
		if(cotizacionGlobal == null) {
			return false;
		}
		try {
			return daoCotizacionGlobal.crea(cotizacionGlobal);
		} catch(Exception e) {
			return false;
		}
	}
	
	public boolean creaCotizacionGlobal(Cliente cliente, String folio, String numeroSiniestro, String fechaCreacion,
			String fechaLimite, String marcaAuto, String modeloAuto, Hashtable<String, 
			ArrayList<Object []>> listaProductosAgregados, int cantidadProductosAgregados){

			CotizacionGlobal cotizacionGlobal = null;
			SubCotizacion subCotizacion [] = null;
			Producto producto [] = null;
			Proveedor proveedorDefecto = null;
			// Contadores
			int i = 0;
			int j = 0;
			int k = 0;
			// Booleanos
			boolean cotizacionGlobalCreada = false;
			boolean subCotizacionCreada = false;
			boolean productoCreado = false;

			try {
				// Cotizacion Global
				cotizacionGlobal = new CotizacionGlobal();
				Date fechaCreacionAuxiliar = new SimpleDateFormat("yyyy-MM-dd").parse(fechaCreacion);
				Date fechaLimiteAuxiliar = new SimpleDateFormat("yyyy-MM-dd").parse(fechaLimite);

				cotizacionGlobal.setFolio(folio);
				cotizacionGlobal.setNumeroSiniestro(numeroSiniestro);
				cotizacionGlobal.setEstado("Nueva");								// Valor por defecto
				cotizacionGlobal.setFechaCreacion(fechaCreacionAuxiliar);
				cotizacionGlobal.setFechaLimite(fechaLimiteAuxiliar);
				cotizacionGlobal.setFechaEntrega(fechaLimiteAuxiliar);				// Valor por defecto
				cotizacionGlobal.setNumeroPiezasTotal(cantidadProductosAgregados);
				cotizacionGlobal.setPrecioCostoTotal(0.0);							// Valor por defecto
				cotizacionGlobal.setPrecioVentaTotal(0.0);							// Valor por defecto
				cotizacionGlobal.setMarcaAuto(marcaAuto);
				cotizacionGlobal.setModeloAuto(modeloAuto);
				cotizacionGlobal.setCliente(cliente);

				// SubCotizacion
				subCotizacion = new SubCotizacion[listaProductosAgregados.size()];
			    Set<String> llaves = listaProductosAgregados.keySet();
			    producto = new Producto[cantidadProductosAgregados];
			    proveedorDefecto = daoProveedor.recupera(0);

			    for(String idUsuario: llaves){
			    	subCotizacion[i] = new SubCotizacion();

			    	ArrayList<Object []> listaTemporal = listaProductosAgregados.get(idUsuario);
					Object datosTemporales[] = listaTemporal.get(0);
		        	Usuario usuario = (Usuario) datosTemporales[0];

					subCotizacion[i].setSubFolio(folio + "-" + (i + 1));
					subCotizacion[i].setEstado("Nueva");						// Valor por defecto
					subCotizacion[i].setNumeroPiezas(listaTemporal.size());
					subCotizacion[i].setPrecioCostoSubTotal(0.0);				// Valor por defecto
					subCotizacion[i].setPrecioVentaSubTotal(0.0);				// Valor por defecto
					subCotizacion[i].setUsuario(usuario);
					subCotizacion[i].setCotizacionGlobal(cotizacionGlobal);

					// Productos
		        	for(j = 0; j < listaTemporal.size(); j ++) {
		        		producto[k] = new Producto();
		        		datosTemporales = listaTemporal.get(j);
		        		
		        		usuario = (Usuario) datosTemporales[0];
		        		TipoProducto tipoProducto = (TipoProducto) datosTemporales[1];
		        		int numeroProducto = (Integer) datosTemporales[2];
		        		int cantidadProducto = (Integer) datosTemporales[3];
		        		String descripcionProducto = (String) datosTemporales[4];

						producto[k].setNumeroProducto(numeroProducto);
						producto[k].setDescripcion(descripcionProducto);
						producto[k].setCantidad(cantidadProducto);			
						producto[k].setPrecioCostoUnitario(0.0);				// Valor por defecto
						producto[k].setPrecioVentaUnitario(0.0);				// Valor por defecto
						producto[k].setSubCotizacion(subCotizacion[i]);
						producto[k].setTipoProducto(tipoProducto);
						producto[k].setProveedor(proveedorDefecto);
		    	    	
		    	    	k ++;
		        	}

		        	i ++;
				}
			    
				// Inserta la cotizacion global en la base de datos
				cotizacionGlobalCreada = daoCotizacionGlobal.crea(cotizacionGlobal);

				// Si ocurrio algun problema durante la insercion de la cotizacion global
				if(!cotizacionGlobalCreada)
					return false;

				// Inserta todas las subcotizaciones en la bsee de datos
				for(i = 0; i < subCotizacion.length; i ++){
					subCotizacionCreada = daoSubCotizacion.crea(subCotizacion[i]);

					// Si ocurrio algun problema durante la insercion de alguna subcotizacion
					if(!subCotizacionCreada) {
						// Borra la cotizacion global de la base de datos
						daoCotizacionGlobal.borra(cotizacionGlobal);
						
						// Borra todas las subcotizaciones insertadas hasta el momento de la base de datos
						for(j = 0; j < i; j ++)
							daoSubCotizacion.borra(subCotizacion[j]);						
						
						return false;
					}
				}

				// Inserta todos los productos en la base de datos
				for(i = 0; i < producto.length; i ++){
					productoCreado = daoProducto.crea(producto[i]);

					// Si ocurrio algun problema durante la insercion de algun producto
					if(!productoCreado){
						// Borra la cotizacion global de la base de datos
						daoCotizacionGlobal.borra(cotizacionGlobal);
						
						// Borra todas las subcotizaciones insertadas en la base de datos
						for( j = 0; j < subCotizacion.length; j ++)
							daoSubCotizacion.borra(subCotizacion[j]);

						// Borra todos los productos insertados hasta el momento de la base de datos
						for(j = 0; j < i; j ++)
							daoProducto.borra(producto[j]);

						return false;
					}
				}
			}
			
	        catch(Exception e){
	        	System.out.println("Excepcion: " + e);
	        	
	    	    return false;
	        }

		return true;
	}	

	public  ArrayList<CotizacionGlobal> buscaCotizacionGlobal(String folio, String siniestro, String nombreCliente, String estado, Date fechaInicio, Date fechaFin) {
		try {
			ArrayList<CotizacionGlobal> todasCotGb = daoCotizacionGlobal.recuperaTodas();
			System.out.println("Antes del filtrado " + todasCotGb);
			if(todasCotGb.isEmpty()) {
				return todasCotGb;
			}
			ArrayList<CotizacionGlobal> coincidencias = filtraCotizacionGlobal(folio,siniestro,nombreCliente,estado,fechaInicio,fechaFin,todasCotGb);
			System.out.println("Despues del filtrado " + coincidencias);
			return coincidencias;
		} catch (Exception e){
			return new ArrayList<CotizacionGlobal>();
		}
	}


	public ArrayList<CotizacionGlobal> filtraCotizacionGlobal(String folio, String siniestro, String nombreCliente, String estado, Date fechaInicio, Date fechaFin, ArrayList<CotizacionGlobal> todasCotGb) {
			try {
				int n, vacios = 6;
				System.out.println("Aun no trueno 1");
				//Revisa si el folio coincide en alguna, borrando de la lista a las que no
				if(!(folio.equals("") || folio.equals(null))) {
					vacios -= 1;
					n = todasCotGb.size();
					for(int i = 0; i < n; i++) {
						if(!(todasCotGb.get(i).getFolio().equals(folio))) {
							todasCotGb.remove(i);
							i -= 1;
							n -= 1;
						}
					}
				}
				System.out.println("Aun no trueno 2");
				//De las restantes, revisa si el numero de siniestro coincide en alguna
				if(!(siniestro.equals("") || siniestro.equals(null))) {
					vacios -= 1;
					n = todasCotGb.size();
					for(int i = 0; i < n; i++) {
						if(!(todasCotGb.get(i).getNumeroSiniestro().equals(siniestro))) {
							todasCotGb.remove(i);
							i -= 1;
							n -= 1;
						}
					}
				}
				System.out.println("Aun no trueno 3");
				//Repite lo anterior con el nombre del cliente
				if(!(nombreCliente.equals("") || nombreCliente.equals(null))) {
					vacios -= 1;
                    n  = todasCotGb.size();
					for(int i = 0; i < n; i++) {
						if(!(todasCotGb.get(i).getCliente().getNombre().equals(folio))) {
							todasCotGb.remove(i);
							i -= 1;
							n -= 1;
						}
					}
				}
				System.out.println("Aun no trueno 4");
				//Luego revisa el estado
				if(!(estado.equals("") || estado.equals(null))) {
					vacios -= 1;
					n = todasCotGb.size();
					for(int i = 0; i < n; i++) {
						if(!(todasCotGb.get(i).getEstado().equals(estado))) {
							todasCotGb.remove(i);
							i -= 1;
							n -= 1;
						}
					}
				}
				System.out.println("Aun no trueno 5" + todasCotGb);
				//Filtra con la fecha de inicio
				if(fechaInicio != null) {
					vacios -= 1;
					//System.out.println("Entre");
					n = todasCotGb.size();
					for(int i = 0; i < n; i++) {
						if(!(todasCotGb.get(i).getFechaCreacion().equals(fechaInicio))) {
							todasCotGb.remove(i);
							i -= 1;
							n -= 1;
						}
					}
				}
				System.out.println("Aun no trueno 6" + todasCotGb);
				//Finalmente, revisa la fecha de entrega
				if(fechaFin != null) {
					vacios -= 1;
					n = todasCotGb.size();
					for(int i = 0; i < n; i++) {
						if(!(todasCotGb.get(i).getFechaEntrega().equals(fechaFin))) {
							todasCotGb.remove(i);
							i -= 1;
							n -= 1;
						}
					}
				}
				System.out.println("Termine el filtrado");
				if(vacios < 6) {
					return todasCotGb;
				} else {
					return new ArrayList<CotizacionGlobal>();
				}

			} catch(Exception e) {
				System.out.println("Entro en catch");
				return new ArrayList<CotizacionGlobal>();
				//return todasCotGb;
			}
	}


	public boolean actualizaCotizacionGlobal(CotizacionGlobal cotizacionGlobal) {
		try {
			return daoCotizacionGlobal.actualiza(cotizacionGlobal);
		} catch(Exception e) {
			return false;
		}
	}


    public boolean borraCotizacionGlobal(CotizacionGlobal cotizacionGlobal) {
		try {
			return daoCotizacionGlobal.borra(cotizacionGlobal);
		} catch(Exception e) {
			return false;
		}
	}
    
    public ArrayList<CotizacionGlobal> RecuperaTodasCotizacionesNuevas(String estado){
    	
    	ArrayList<CotizacionGlobal> cotizacionesGlobales=new ArrayList<CotizacionGlobal>();
    	cotizacionesGlobales=daoCotizacionGlobal.recuperaTodas();
    	ArrayList<CotizacionGlobal> cotizacionesNuevas=new ArrayList<CotizacionGlobal>();
    	if(cotizacionesGlobales!=null){
    	for(int i=0;i<cotizacionesGlobales.size();i++){
    		if(cotizacionesGlobales.get(i).getEstado().equals(estado));
    		cotizacionesNuevas.add(cotizacionesGlobales.get(i));
    	}
    	return cotizacionesNuevas;
    	}else{
    		return null;
    	}
    }

	
}
