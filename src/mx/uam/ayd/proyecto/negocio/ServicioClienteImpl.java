package mx.uam.ayd.proyecto.negocio;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.datos.DAOCliente;
import mx.uam.ayd.proyecto.negocio.dominio.Cliente;

/**
 * Implementación del módulo de servicio para los clientes.
 */
public class ServicioClienteImpl implements ServicioCliente{
	
	DAOCliente daoCliente;
	private Cliente cliente;
	private ArrayList<Cliente> clientes;

	public ServicioClienteImpl(DAOCliente daoCliente){
		this.daoCliente = daoCliente;
	}
		
	public boolean crea(Cliente cliente) {
		boolean creado;
		creado = daoCliente.crea(cliente);
			
		return creado;
	}
	
	public Cliente recuperaCliente(String codigoCliente) {
		cliente = daoCliente.recupera(codigoCliente);
		
		return cliente;
	}
	
	public boolean actualizaCliente(Cliente cliente) {
		boolean actualizado;
		actualizado = daoCliente.actualiza(cliente);
			
		return actualizado;
	}
	
	public boolean borraCliente(Cliente cliente) {
		boolean borrado;
		borrado = daoCliente.borra(cliente);
			
		return borrado;
	}
	
	public ArrayList<Cliente> recuperaTodoClientes() {
		clientes = daoCliente.recuperaTodos();
		
		return clientes;
	}
}
