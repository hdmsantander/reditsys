package mx.uam.ayd.proyecto.negocio;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.datos.DAOCotizacionGlobal;
import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;
import mx.uam.ayd.proyecto.negocio.dominio.Producto;
import mx.uam.ayd.proyecto.datos.DAOSubCotizacion;
import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;

/**
 * Implementación del módulo de servicio para las sub cotizaciones.
 */
public class ServicioSubCotizacionImpl implements ServicioSubCotizacion {
	private DAOSubCotizacion daoSC;
    private DAOCotizacionGlobal daoCGl;

	public ServicioSubCotizacionImpl(DAOSubCotizacion dao, DAOCotizacionGlobal daoGlobal) {
		// Creamos conexion al DAO
		this.daoSC = dao;
        this.daoCGl = daoGlobal;
	}

	public ArrayList<SubCotizacion> recuperaTodas(String folio) {
        try {
            //El DAO debe tener la opcion de ponerle el folio global para recuperar solo esas
            ArrayList<SubCotizacion> subC = daoSC.recuperaTodas(folio);
            return subC;
        } catch(Exception e) {
            return new ArrayList<SubCotizacion>();
        }
    }

	public SubCotizacion recuperaSubCotizacion(String subfolio) {
        try {
            SubCotizacion sC = daoSC.recupera(subfolio);
            return sC;
        } catch(Exception e) {
            return null;
        }
    }


	public CotizacionGlobal recuperaCotizacion(String folio) {
        try {
            CotizacionGlobal cotG = daoCGl.recupera(folio);
            return cotG;
        } catch(Exception e) {
            return null;
        }
    }
	

	public boolean recuperaYActualizaEstado(String subfolio,String estado) {
		return false;
    }

	public SubCotizacion actualizaEstado(SubCotizacion subCot,String estado) {
		return null;
    }

	public boolean borraSubCotizacion(String subfolio) {
		return false;
    }

	public ArrayList<Producto> obtenListaProductos(SubCotizacion subCot) {
        return null;
    }

	public boolean validaSubCotizacionCerrada(SubCotizacion subCot) {
		return false;
    }

	public boolean actualizaSubCotizacion(SubCotizacion subCot) {
		return false;
    }

}
