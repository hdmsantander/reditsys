package mx.uam.ayd.proyecto.negocio;

import java.util.ArrayList;


/**
 * Modulo de servicio para los clientes.
 */
import mx.uam.ayd.proyecto.negocio.dominio.Cliente;

/**
 * Módulo de servicio para los clientes.
 */
public interface ServicioCliente {	
	public boolean crea(Cliente cliente);
	
	public Cliente recuperaCliente(String codigoCliente);
	
	public boolean actualizaCliente(Cliente cliente);
	
	public boolean borraCliente(Cliente cliente);	
	
	/**
	 * Metodo recuperaTodoClientes
	 * metodo que regresa una lista de todos los clientes
	 * @return Lista de Clientes: clientes
	 **/
	public ArrayList<Cliente> recuperaTodoClientes();
}
