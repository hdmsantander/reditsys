package mx.uam.ayd.proyecto.negocio;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.datos.DAOUsuario;
import mx.uam.ayd.proyecto.negocio.dominio.Usuario;

/**
 * Implementación del módulo de servicio para los usuarios.
 */
public class ServicioUsuarioImpl implements ServicioUsuario{
	
	private DAOUsuario daoUsuario;
	private Usuario user;

	public  ServicioUsuarioImpl(DAOUsuario dao) {
		this.daoUsuario=dao;
		
	}
	
	public boolean ComparaContrasena(Usuario u,String contra) {
		
		if(u.getContrasena().equals(contra)){
			return true;
		}else{
			return false;
		}
		
	}
	
	public boolean validaUsuario(String idusuario,String ContrasenaCapturada){
		
		user=daoUsuario.recupera(idusuario);
		boolean valido;
		
	    if(user==null) {
	    	return false;
	    }else {
	    	valido=ComparaContrasena(user,ContrasenaCapturada);
	    }
	    
	    if(valido==true) {
	    	return true;
	    }
	    else {
	    	return false;
	    }
	}
	
	public Usuario RecuperaUsuario(String idusuario){
		user=daoUsuario.recupera(idusuario);
		return user;
		
	}
	
	public ArrayList<Usuario> recuperaTodosUsuarios(){
		
			ArrayList<Usuario> todosUsuario = daoUsuario.recuperaTodos();
		    return todosUsuario;
	}
	
	public boolean ActualizaUsuario(Usuario u){
		boolean actualizado;
		actualizado=daoUsuario.actualiza(u);
		if(actualizado==true) {
		return true;
		}else {
			return false;
		}
	}
	
	public boolean borraUsuario(Usuario u) {
		boolean borrado;
		borrado=daoUsuario.borra(u);
		if(borrado==true) {
			return true;
			}else {
				return false;
		}
	}
	
	public boolean crearU(Usuario u) {
		boolean creado;
		creado=daoUsuario.crea(u);
		if(creado==true) {
			return true;
			}else {
				return false;
		}
		
	}//fin metodo
	
	
}
