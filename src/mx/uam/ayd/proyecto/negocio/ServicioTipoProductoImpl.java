package mx.uam.ayd.proyecto.negocio;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.datos.DAOTipoProducto;
import mx.uam.ayd.proyecto.negocio.dominio.TipoProducto;

/**
 * Implementación del módulo de servicio para los tipos de producto.
 */
public class ServicioTipoProductoImpl implements ServicioTipoProducto{
	
	private ArrayList<TipoProducto> tipos;
	DAOTipoProducto daoTipo;
	
	public ServicioTipoProductoImpl(DAOTipoProducto dao){
		
		this.daoTipo=dao;
		
	}//fin metodo
	
	public ArrayList<TipoProducto> recuperaTodoTiposProducto() {
		tipos=daoTipo.recuperaTodos();
		return tipos;
	}//fin metodo
	
	public boolean crea(TipoProducto tipo) {
		boolean creado;
		creado=daoTipo.crea(tipo);
		if(creado==true) {
			return true;
			
		}else{
			return false;
		}
	}//fin metodo
	
	public boolean actualizaTipoProducto(TipoProducto tipo) {
		boolean actualizado;
		actualizado=daoTipo.actualiza(tipo);
		if(actualizado==true) {
			return true;
			
		}else{
			return false;
		}
	}//fim metodo
	
	public boolean borraTipoProducto(TipoProducto tipo) {
		boolean borrado;
		borrado=daoTipo.borra(tipo);
		if(borrado==true) {
			return true;
			
		}else{
			return false;
		}
	}//fin metodo
}
