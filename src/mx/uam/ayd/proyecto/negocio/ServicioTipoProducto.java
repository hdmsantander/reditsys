package mx.uam.ayd.proyecto.negocio;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.negocio.dominio.TipoProducto;

/**
 * Módulo de servicio para los tipos de producto.
 */
public interface ServicioTipoProducto {
	
	/**
	 * Metodo recuperaTodoTiposProducto
	 * metodo que regrea una lista de los tipos de producto
	 * @return Lista de TipoProducto: tipos
	 **/
	public ArrayList<TipoProducto> recuperaTodoTiposProducto();
	
	public boolean actualizaTipoProducto(TipoProducto tipo);
	
	public boolean borraTipoProducto(TipoProducto tipo);
	
	public boolean crea(TipoProducto tipo);
}
