package mx.uam.ayd.proyecto.negocio;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import mx.uam.ayd.proyecto.negocio.dominio.Cliente;
import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;

/**
 * Módulo de servicio para las cotizaciones globales.
 */
public interface ServicioCotizacionGlobal {

	/**
	 * boolean <code>validaExisteCotizacionGlobal</code>(String folio)
	 * <ul>
	 * <li>Valida que exista una cotización global con el folio proporcionado.
	 * </ul>
	 * @param folio el folio a validar.
	 * @return true si existe, false si no existe en la base de datos o hubo errores al agregarla.
	 */
	public boolean validaExisteCotizacionGlobal(String folio);
	
	/**
	 * boolean <code>creaCotizacionGlobal</code>(CotizacionGlobal cotizacionGlobal)
	 * <ul> 
	 * <li>Permite agregar una nueva cotizacion global mientras no exista ya otra igual.
	 * </ul>
	 * @param cotizacionGlobal La cotizacion global a agregar.
	 * @return true si la operación tuvo éxito, false si ya existía en la base de datos o hubo errores al agregarla.
	 */
	public boolean creaCotizacionGlobal(CotizacionGlobal cotizacionGlobal); 
	
	/**
	 * Permite agregar una nueva cotizacion global mientras no exista ya otra igual.
	 * 	
	 * @param cliente el cliente
	 * @param folio el folio deseado
	 * @param numeroSiniestro el número de siniestro
	 * @param fechaCreacion la fecha de creación
	 * @param fechaLimite la fecha límite
	 * @param marcaAuto la marca del auto
	 * @param modeloAuto el modelo del auto
	 * @param listaProductosAgregados la lista que contiene los productos
	 * @param cantidadProductosAgregados la cantidad de productos agregados
	 * @return true si la creación fue exitosa, false si no.
	 */
	public boolean creaCotizacionGlobal(Cliente cliente, String folio, String numeroSiniestro, String fechaCreacion,
			String fechaLimite, String marcaAuto, String modeloAuto, Hashtable<String, 
			ArrayList<Object []>> listaProductosAgregados, int cantidadProductosAgregados);
	
	/**
	 * Permite recuperar las Cotizaciones Globales que coincidan con la busqueda
	 * 
	 * @param folio el folio a recuperar
	 * @param siniestro el siniestro a recuperar
	 * @param nombreCliente el nombre del cliente a recuperar
	 * @param estado el estado a recuperar
	 * @param fechaInicio la fecha de inicio a recuperar
	 * @param fechaFin la fecha final a recuperar
	 * @return un arraylist con las cotizaciones globales que satisfacen los criterios de búsqueda.
	 */
	public  ArrayList<CotizacionGlobal> buscaCotizacionGlobal(String folio, String siniestro, String nombreCliente, String estado, Date fechaInicio, Date fechaFin);

	/**
	 * Permite filtrar las Cotizaciones Globales que regrese el DAO en el metodo recuperaTodos
	 * 
	 * @param folio el folio a filtrar
	 * @param siniestro el siniestro a filtrar
	 * @param nombreCliente el nombre del cliente a filtrar
	 * @param estado el estado a filtrar
	 * @param fechaInicio la fecha de inicio a filtrar
	 * @param fechaFin la fecha final a filtrar
	 * @param todasCotGb un arraylist con todas las cotizaciones globales a filtrar.
	 * @return el arraylist resultado del filtrado
	 */
	public  ArrayList<CotizacionGlobal> filtraCotizacionGlobal(String folio, String siniestro, String nombreCliente, String estado, Date fechaInicio, Date fechaFin, ArrayList<CotizacionGlobal> todasCotGb);

    /**
     * boolean <code>actualizaCotizacionGlobal</code>(CotizacionGlobal cotizacionGlobal)
     * <ul>
	 * <li>Actualiza una cotizacion global
	 * </ul>
	 * @param cotizacionGlobal la cotizacion global a actualizar.
	 * @return true si fue exitosa la operación, false si no.
	 */
	public boolean actualizaCotizacionGlobal(CotizacionGlobal cotizacionGlobal);
	
	/**
	 * boolean <code>borraCotizacionGlobal</code>(CotizacionGlobal cotizacionGlobal)
	 * <ul>
	 * <li>Borra una cotizacion global
	 * </ul>
	 * @param cotizacionGlobal la cotizacion global a borrar.
	 * @return true si fue exitosa la operación, false si no.
	 */
	public boolean borraCotizacionGlobal(CotizacionGlobal cotizacionGlobal);
	
	/**
	 * ArrayList &lt; CotizacionGlobal &gt; <code>RecuperaTodasCotizacionesNuevas</code>(String estado)
	 * <ul>
	 * <li>Recupera Todas las Cotizaciones
	 * </ul>
	 * @param estado el estado deseado a filtrar.
	 * @return un arraylist con las cotizaciones globales.
	 */
	 public ArrayList<CotizacionGlobal> RecuperaTodasCotizacionesNuevas(String estado);


}
