package mx.uam.ayd.proyecto.negocio;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;
import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;
import mx.uam.ayd.proyecto.negocio.dominio.Producto;

/**
 * Módulo de servicio para las sub cotizaciones.
 */
public interface ServicioSubCotizacion {

	/**
	 * Regresa todas las Sub Cotizaciones a partir del folio de su Cotizacion Global
	 * 
	 * @param folio el folio de la cotizacion global para filtrar
	 * @return un arreglo con todas las subcotizaciones.
	 */
	public ArrayList<SubCotizacion> recuperaTodas(String folio);

	/**
	 * Regresa una Sub Cotizacion a partir de su subfolio
	 * 
	 * @param subfolio el subfolio de la sub cotizacion a recuperar.
	 * @return SubCotizacion o null
	 */
	public SubCotizacion recuperaSubCotizacion(String subfolio);

    /**
	 * Regresa una Cotizacion Global a partir de su folio
	 * 
	 * @param folio el folio por el cual filtrar para recuperar la cotizacion
	 * @return CotizacionGlobal o null
	 */
	public CotizacionGlobal recuperaCotizacion(String folio);

    /**
	 * Actualiza una Sub Cotizacion con un nuevo estado a partir de su subfolio y el nuevo estado
	 * 
	 * @param subfolio el subfolio de la subcotizacion a actualizar.
	 * @param estado el estado a colocar.
	 * @return True o False
	 */
	public boolean recuperaYActualizaEstado(String subfolio,String estado);

   /**
	 * Actualiza el estado de una Sub Cotizacion
	 * 
	 * @param subCot la subcotizacion a actualizar.
	 * @param estado el estado de la subcotizacion a colocar.
	 * @return SubCotizacion o null
	 */
	public SubCotizacion actualizaEstado(SubCotizacion subCot,String estado);

       /**
	 * Borra una subCotizacion a Partir de su folio
	 * 
	 * @param subfolio el subfolio de la cotizacion a borrar.
	 * @return True o False
	 */
	public boolean borraSubCotizacion(String subfolio);

	/**
	 * Regresa todos los productos de una Sub Cotizacion
	 * 
	 * @param subCot la subcotizacion de la cual obtener la lista de productos.
	 * @return un arraylist con la lista de los productos que tienen esa subcotización asignada.
	 */
	public ArrayList<Producto> obtenListaProductos(SubCotizacion subCot);

	/**
	 * Revisa si la Sub Cotizacion no ha sido marcada como cerrada
	 * 
	 * @param subCot la subcotizacion a validar si está marcada como cerrada
	 * @return True o False
	 */
	public boolean validaSubCotizacionCerrada(SubCotizacion subCot);

	/**
	 * Guarda los cambios hechos a la Sub Cotizacion
	 * 
	 * @param subCot la subcotizacion a actualizar.
	 * @return True o False
	 */
	public boolean actualizaSubCotizacion(SubCotizacion subCot);

}
