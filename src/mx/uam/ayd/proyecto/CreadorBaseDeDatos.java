package mx.uam.ayd.proyecto;


import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import mx.uam.ayd.proyecto.datos.DatabaseException;
import mx.uam.ayd.proyecto.datos.ManejadorBaseDatos;

/**
 * Crea e inicializa la base de datos con algunos valores por defecto.
 */
public class CreadorBaseDeDatos {
	
	// Obtenemos el logger.
	static Logger log = Logger.getRootLogger();

	
	/**
	 * Al ejecutar este metodo se crea la base de datos. NOTA: solo se puede crear una vez.
	 * Para crearla de nuevo es necesario borrar la carpeta que la contiene.
	 * 
	 * @param args los argumentos del método principal. (No usados).
	 */
	public static void main(String args[]) {
		try
	    {
			log.info("Creando base de datos");
		
			Connection connection = ManejadorBaseDatos.getConnection();
	
			Statement statement = connection.createStatement();
	        
	        String cadenaCliente = "CREATE TABLE CLIENTE (" + 
	        		"  codigo_cliente VARCHAR(10) NOT NULL," + 
	        		"  nombre VARCHAR(20) NOT NULL," + 
	        		"  rfc VARCHAR(13) NOT NULL," + 
	        		"  direccion VARCHAR(50) NOT NULL," + 
	        		"  telefono VARCHAR(10) NOT NULL," + 
	        		"  correo_electronico VARCHAR(20) NOT NULL," + 
	        		"  PRIMARY KEY (codigo_cliente)" + 
	        		")";
	        
	        String cadenaProveedor = "CREATE TABLE PROVEEDOR (" + 
	        		"  id_proveedor INT," + 
	        		"  nombre VARCHAR(20)," + 
	        		"  rfc VARCHAR(13)," + 
	        		"  direccion VARCHAR(50)," + 
	        		"  telefono VARCHAR(10)," + 
	        		"  correo_electronico VARCHAR(20)," + 
	        		"  descripcion VARCHAR(50)," + 
	        		"  PRIMARY KEY (id_proveedor)" + 
	        		")";
	        
	        String cadenaUsuario = "CREATE TABLE USUARIO (" + 
	        		"  id_usuario VARCHAR(20)," + 
	        		"  nombre VARCHAR(50)," + 
	        		"  apellido_paterno VARCHAR(20)," + 
	        		"  apellido_materno VARCHAR(20)," + 
	        		"  rfc VARCHAR(13)," + 
	        		"  direccion VARCHAR(50)," + 
	        		"  telefono VARCHAR(10)," + 
	        		"  correo_electronico VARCHAR(20)," + 
	        		"  contrasena VARCHAR(65)," + 
	        		"  privilegiado INT," + 
	        		"  PRIMARY KEY (id_usuario)" + 
	        		")";
	        
	        String cadenaTipoProducto = "CREATE TABLE TIPO_PRODUCTO (" + 
	        		"  id_tipo_producto INT," + 
	        		"  nombre VARCHAR(50)," + 
	        		"  PRIMARY KEY (id_tipo_producto)" + 
	        		")";
	        
	        String cadenaCotizacionGlobal = "CREATE TABLE COTIZACION_GLOBAL (" + 
	        		"  folio VARCHAR(10)," + 
	        		"  numero_siniestro VARCHAR(15)," + 
	        		"  estado VARCHAR(20)," + 
	        		"  fecha_creacion DATE," + 
	        		"  fecha_limite DATE," + 
	        		"  fecha_entrega DATE," + 
	        		"  numero_piezas_total INT," + 
	        		"  precio_costo_total FLOAT," + 
	        		"  precio_venta_total FLOAT," + 
	        		"  marca_auto VARCHAR(20)," + 
	        		"  modelo_auto VARCHAR(20)," + 
	        		"  codigo_cliente VARCHAR(10)," + 
	        		"  PRIMARY KEY (folio)," + 
	        		"  FOREIGN KEY (codigo_cliente) REFERENCES CLIENTE(codigo_cliente)" + 
	        		")";
	        
	        String cadenaProducto = "CREATE TABLE PRODUCTO (" + 
	        		"  id_producto INT," + 
	        		"  descripcion VARCHAR(50)," + 
	        		"  cantidad INT," + 
	        		"  precio_costo_unitario FLOAT," + 
	        		"  precio_venta_unitario FLOAT," + 
	        		"  subfolio VARCHAR(10)," + 
	        		"  id_tipo_producto INT," + 
	        		"  id_proveedor INT," + 
	        		"  PRIMARY KEY (id_producto)," + 
	        		"  FOREIGN KEY (subfolio) REFERENCES  SUBCOTIZACION(sub_folio)," + 
	        		"  FOREIGN KEY (id_tipo_producto) REFERENCES TIPO_PRODUCTO(id_tipo_producto)," + 
	        		"  FOREIGN KEY (id_proveedor) REFERENCES PROVEEDOR(id_proveedor)" + 
	        		")";
	        
	        String cadenaSubCotizacion = "CREATE TABLE SUBCOTIZACION (" + 
	        		"  sub_folio VARCHAR(10)," + 
	        		"  estado VARCHAR(20)," + 
	        		"  numero_piezas INT," + 
	        		"  precio_costo_subtotal FLOAT," + 
	        		"  precio_venta_subtotal FLOAT," + 
	        		"  folio VARCHAR(10)," + 
	        		"  id_usuario VARCHAR(20)," + 
	        		"  PRIMARY KEY (sub_folio)," + 
	        		"  FOREIGN KEY (folio) REFERENCES COTIZACION_GLOBAL(folio)," + 
	        		"  FOREIGN KEY (id_usuario) REFERENCES USUARIO(id_usuario)" + 
	        		")";
	        log.info("Creando tabla Cliente");
	        statement.execute(cadenaCliente);
	        log.info("Creando tabla Proveedor");
	        statement.execute(cadenaProveedor);
	        log.info("Creando tabla Usuario");
	        statement.execute(cadenaUsuario);
	        log.info("Creando tabla TipoProducto");
	        statement.execute(cadenaTipoProducto);
	        log.info("Creando tabla CotizacionGlobal");
	        statement.execute(cadenaCotizacionGlobal);
	        log.info("Creando tabla SubCotizacion");
	        statement.execute(cadenaSubCotizacion);
	        log.info("Creando tabla Producto");
	        statement.execute(cadenaProducto);
	        
	        ///////Incersion de datos inciales en las tablas//////
	       
	        // Clientes
	        String insercionCliente1 = "INSERT INTO CLIENTE (codigo_cliente,nombre,rfc,direccion,telefono,correo_electronico) " +
	        " VALUES ('001','Nicolas Zavala','NIZR760506BX6','Av.Rojo Gomez #223 Col.Progreso Del.Iztapalapa','5145832279','soporteNic@gmail.com')"; 
	        String insercionCliente2 = "INSERT INTO CLIENTE (codigo_cliente,nombre,rfc,direccion,telefono,correo_electronico) " +
	        " VALUES ('002','Mariano Ortiz','MAOT990913BK9','San Pedrito #53 Col.Revolucion Del.Cuauhtemoc','5520114019','mario0r@yahoo.com')"; 
	        String insercionCliente3 = "INSERT INTO CLIENTE (codigo_cliente,nombre,rfc,direccion,telefono,correo_electronico) " +
	        " VALUES ('003','Karen Medina','KAMP910128419','Zaragoza Col Reforma Del Iztapalapa.','5513967721','karim@gmail.com')";
	        String insercionCliente4 = "INSERT INTO CLIENTE (codigo_cliente,nombre,rfc,direccion,telefono,correo_electronico) " +
	        " VALUES ('004','Yolanda Ramirez','YORS9606069G5','San Isidro #88 Col.Santa Ursula Del.Tlalpan','5608960129','yoli_rmzgmail.com')";
	        String insercionCliente5 = "INSERT INTO CLIENTE (codigo_cliente,nombre,rfc,direccion,telefono,correo_electronico) " +
	        " VALUES ('005','Fernanda Pastrana','FEPR921008ML4','Av.Revolucion #129 Col.Tepalcates Del.Iztapalapa','5507933328','fer_pas@gmail.com')";
	        
	        // Proveedores
	        String insercionProveedor0 = "INSERT INTO PROVEEDOR (id_proveedor,nombre,rfc,direccion,telefono,correo_electronico,descripcion) " +
	        " VALUES (0,'-','-','-','-','-','-')"; 
	        String insercionProveedor1 = "INSERT INTO PROVEEDOR (id_proveedor,nombre,rfc,direccion,telefono,correo_electronico,descripcion) " +
	        " VALUES (1111,'Refac. California','EVON831601VG3','Eje Central 654 Del.Cuauhtemoc','2722349246','ref_cal@gmail.com','Revisar primero en distintos productos.')"; 
	        String insercionProveedor2 = "INSERT INTO PROVEEDOR (id_proveedor,nombre,rfc,direccion,telefono,correo_electronico,descripcion) " +
	        " VALUES (1112,'Masterefacciones','MIDF923011GS1','Uruguay #203 Del.Cuauhtemoc','5526316297','masteref@hotmail.com','Revisar para refacciones de motor.')";
	        String insercionProveedor3 = "INSERT INTO PROVEEDOR (id_proveedor,nombre,rfc,direccion,telefono,correo_electronico,descripcion) " + 
	        " VALUES (1113,'Refac24','RADM790505Z01','Zacatecas #996 Del.Cuauhtemoc','5518346127','refac_24@outlook.com','Revisar para refacciones de llantas.')";
	        
	        // Tipos de productos
	        String insercionTipoProducto1 = "INSERT INTO TIPO_PRODUCTO (id_tipo_producto,nombre) " + 
	        " VALUES (101,'llanta')"; 
	        String insercionTipoProducto2 = "INSERT INTO TIPO_PRODUCTO (id_tipo_producto,nombre) " +
	        " VALUES (102,'motor')"; 
	        String insercionTipoProducto3 = "INSERT INTO TIPO_PRODUCTO (id_tipo_producto,nombre) " +
	        " VALUES (103,'enfriamiento')"; 
	        String insercionTipoProducto4 = "INSERT INTO TIPO_PRODUCTO (id_tipo_producto,nombre) " +
	        " VALUES (104,'carroceria')"; 
	        String insercionTipoProducto5 = "INSERT INTO TIPO_PRODUCTO (id_tipo_producto,nombre) " +
	        " VALUES (105,'vidrios')";
	        String insercionTipoProducto6 = "INSERT INTO TIPO_PRODUCTO (id_tipo_producto,nombre) " +
	        " VALUES (106,'cofre')";        
	        
	        
	        // Usuarios //
	        /*ID_usuario: 8901  Contrasea: abc123
	         * ID_usuario: 8902  Contrasea: dfg456
	         * ID_usuario: 8903  Contrasea: qwe789
	         * ID_usuario: 8904 Contrasea: zxc012*/
	       String insercionUsuarios1 = "INSERT INTO USUARIO (id_usuario,nombre,apellido_paterno,apellido_materno,rfc,direccion,telefono,correo_electronico,contrasena,privilegiado)" + 
	        		"VALUES ('8901','Javier','Avilez','Monrroy','JAAM720202YM3','Romulo #22 Del.Benito Juarez','5589712223','javi_doo@outlook.com','6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118090',1)"; 
	       String insercionUsuarios2 = "INSERT INTO USUARIO (id_usuario,nombre,apellido_paterno,apellido_materno,rfc,direccion,telefono,correo_electronico,contrasena,privilegiado)" + 
	        		"VALUES ('8902','Daniela','Vazquez','Cervantes','DAVC950707V32','Romulo # 46 Del.Benito Juarez','5614168010','dany1123@gmail.com','a14a53e9457c4cba50ff30512a790f18288680af41abd4947755041f2f3634ba',0)";
	       String insercionUsuarios3 = "INSERT INTO USUARIO (id_usuario,nombre,apellido_paterno,apellido_materno,rfc,direccion,telefono,correo_electronico,contrasena,privilegiado)" + 
	        		"VALUES ('8903','Marco','Pedraza','Monzon','MAPM831021GV9','San Pedro #99 Col.Flores Del.Cuauhtemoc','5573697314','mar_ped@gmail.com','a86d4dc106ac83cb6135dc70178f221974776c106e0855ba16b20081bf808655',0)";
	       String insercionUsuarios4 = "INSERT INTO USUARIO (id_usuario,nombre,apellido_paterno,apellido_materno,rfc,direccion,telefono,correo_electronico,contrasena,privilegiado)" + 
	        		"VALUES ('8904','Margarita','Barreto','Barrientos','MABB9904273K6','Tenores #77 Del.Coyoacan','5573198079','correo@hotmail.com','4f89ef716c45a150c7f7b984aa71f484510dcb446243da114be4fb27a7d1f937',0)";
	        
	       // Cotizaciones globales //
	       String insercionCotizacionGlobal1 = "INSERT INTO APP.COTIZACION_GLOBAL (folio, numero_siniestro, estado, fecha_creacion, fecha_limite, fecha_entrega, numero_piezas_total, precio_costo_total, precio_venta_total, marca_auto, modelo_auto, codigo_cliente) " +
	    		   " VALUES ('90901', '9000', 'Nueva', '2019-01-01', '2019-02-02', '2020-03-03', 10, 20.20, 20.20, 'nissan', 'modelo', '001')";
	       String insercionCotizacionGlobal2 = "INSERT INTO APP.COTIZACION_GLOBAL (folio, numero_siniestro, estado, fecha_creacion, fecha_limite, fecha_entrega, numero_piezas_total, precio_costo_total, precio_venta_total, marca_auto, modelo_auto, codigo_cliente) " +
	    		   " VALUES ('90902', '9000', 'Pendiente', '2019-01-01', '2019-02-02', '2020-03-03', 10, 20.20, 20.20, 'nissan', 'modelo', '001')";
	       String insercionCotizacionGlobal3 = "INSERT INTO APP.COTIZACION_GLOBAL (folio, numero_siniestro, estado, fecha_creacion, fecha_limite, fecha_entrega, numero_piezas_total, precio_costo_total, precio_venta_total, marca_auto, modelo_auto, codigo_cliente) " +
	    	       " VALUES ('90903', '9000', 'Cerrada', '2019-01-01', '2019-02-02', '2020-03-03', 10, 20.20, 20.20, 'nissan', 'modelo', '001')";
	       
	       // Sub cotizaciones //
			String insercionSubCotizacion1 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('20201', 'Nueva', 10, 10.20, 30.10, '90901', '8901')";
			String insercionSubCotizacion2 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('20202', 'Enviada', 10, 10.20, 30.10, '90901', '8901')";
			String insercionSubCotizacion3 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('20203', 'Aceptada', 10, 10.20, 30.10, '90901', '8901')";
			String insercionSubCotizacion4 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('20204', 'Rechazada', 10, 10.20, 30.10, '90901', '8901')";
			String insercionSubCotizacion5 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('20205', 'Pedida', 10, 10.20, 30.10, '90901', '8901')";
			String insercionSubCotizacion6 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('20206', 'Cerrada', 10, 10.20, 30.10, '90901', '8901')";
			String insercionSubCotizacion7 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('20207', 'Nueva', 10, 10.20, 30.10, '90903', '8901')";
			String insercionSubCotizacion8 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('20208', 'Enviada', 10, 10.20, 30.10, '90903', '8901')";
			String insercionSubCotizacion9 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('20209', 'Aceptada', 10, 10.20, 30.10, '90903', '8901')";
			String insercionSubCotizacion10 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('202010', 'Rechazada', 10, 10.20, 30.10, '90903', '8901')";
			String insercionSubCotizacion11 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('202011', 'Pedida', 10, 10.20, 30.10, '90903', '8901')";
			String insercionSubCotizacion12 = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) " +
					" VALUES ('202012', 'Cerrada', 10, 10.20, 30.10, '90903', '8901')";
			
			// Productos //
			String insercionProducto1 = "INSERT INTO APP.PRODUCTO (id_producto, descripcion, cantidad, precio_costo_unitario, precio_venta_unitario, subfolio, id_tipo_producto, id_proveedor) " +
					" VALUES (32321, 'llanta blanca', 10, 10.20, 30.10, '20201', 101, 1111)";
			String insercionProducto2 = "INSERT INTO APP.PRODUCTO (id_producto, descripcion, cantidad, precio_costo_unitario, precio_venta_unitario, subfolio, id_tipo_producto, id_proveedor) " +
					" VALUES (32322, 'llanta negra', 10, 10.20, 30.10, '20202', 101, 1111)";
			String insercionProducto3 = "INSERT INTO APP.PRODUCTO (id_producto, descripcion, cantidad, precio_costo_unitario, precio_venta_unitario, subfolio, id_tipo_producto, id_proveedor) " +
					" VALUES (32323, 'llanta azul', 10, 10.20, 30.10, '20203', 101, 1111)";
			String insercionProducto4 = "INSERT INTO APP.PRODUCTO (id_producto, descripcion, cantidad, precio_costo_unitario, precio_venta_unitario, subfolio, id_tipo_producto, id_proveedor) " +
					" VALUES (32324, 'llanta roja', 10, 10.20, 30.10, '20204', 101, 1111)";
			String insercionProducto5 = "INSERT INTO APP.PRODUCTO (id_producto, descripcion, cantidad, precio_costo_unitario, precio_venta_unitario, subfolio, id_tipo_producto, id_proveedor) " +
					" VALUES (32325, 'llanta blanca', 10, 10.20, 30.10, '20205', 101, 1111)";
			String insercionProducto6 = "INSERT INTO APP.PRODUCTO (id_producto, descripcion, cantidad, precio_costo_unitario, precio_venta_unitario, subfolio, id_tipo_producto, id_proveedor) " +
					" VALUES (32326, 'llanta blanca', 10, 10.20, 30.10, '20206', 101, 1111)";
			
	        log.info("Insercion en tabla cliente");
	        statement.execute(insercionCliente1);	        
	        statement.execute(insercionCliente2);
	        statement.execute(insercionCliente3);
	        statement.execute(insercionCliente4);
	        statement.execute(insercionCliente5);
	        
	        log.info("Insercion en tabla proveedor");
	        statement.execute(insercionProveedor0);
	        statement.execute(insercionProveedor1);
	        statement.execute(insercionProveedor2);
	        statement.execute(insercionProveedor3);
	        
	       log.info("Insercion en tabla tipoProducto");
	        statement.execute(insercionTipoProducto1);
	        statement.execute(insercionTipoProducto2);
	        statement.execute(insercionTipoProducto3);
	        statement.execute(insercionTipoProducto4);
	        statement.execute(insercionTipoProducto5);
	        statement.execute(insercionTipoProducto6);
	        
	        log.info("Insercion en tabla usuario");
	        statement.execute(insercionUsuarios1);
	        statement.execute(insercionUsuarios2);
	        statement.execute(insercionUsuarios3);
	        statement.execute(insercionUsuarios4);
	        
	        log.info("Insercion en tabla Cotizacion Global");
	        statement.execute(insercionCotizacionGlobal1);
	        /*statement.execute(insercionCotizacionGlobal2);
	        statement.execute(insercionCotizacionGlobal3);*/

	        log.info("Insercion en tabla Sub Cotizacion");
	        statement.execute(insercionSubCotizacion1);
	        /*statement.execute(insercionSubCotizacion2);
	        statement.execute(insercionSubCotizacion3);
	        statement.execute(insercionSubCotizacion4);
	        statement.execute(insercionSubCotizacion5);
	        statement.execute(insercionSubCotizacion6);
	        statement.execute(insercionSubCotizacion7);
	        statement.execute(insercionSubCotizacion8);
	        statement.execute(insercionSubCotizacion9);
	        statement.execute(insercionSubCotizacion10);
	        statement.execute(insercionSubCotizacion11);
	        statement.execute(insercionSubCotizacion12);*/
	        
	        /*log.info("Insercion en tabla Producto");
	        statement.execute(insercionProducto1);
	        statement.execute(insercionProducto2);
	        statement.execute(insercionProducto3);
	        statement.execute(insercionProducto4);
	        statement.execute(insercionProducto5);
	        statement.execute(insercionProducto6);*/
	        
	        ManejadorBaseDatos.shutdown();
	    }
	    catch(DatabaseException ex)
	    {
	            log.error("Excepcion de la base de datos",ex.getRealException());
	    }
	    catch(SQLException e){
	    	log.error("Excepcion de la base de datos",e);
	    }	
	}

}
