package mx.uam.ayd.proyecto.presentacion;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import mx.uam.ayd.proyecto.negocio.ServicioUsuario;

import mx.uam.ayd.proyecto.negocio.dominio.Usuario;

/**
 * Control de la búsqueda de cotizaciones globales.
 */
public class ControlLoginUsuario {
	
	
	private ServicioUsuario servicioUsuario;
	private Usuario usuario;
	

	public ControlLoginUsuario(ServicioUsuario servicioUsuario2) {
		// TODO Auto-generated constructor stub
		this.servicioUsuario=servicioUsuario2;
		
	}

	/**
	 * Incializador
	 */
	public void inicia(){
		
		
		VentanaLoginUsuario ventanaUsuario=new VentanaLoginUsuario(this);
		ventanaUsuario.setVisible(true);
		
	}
	
	public boolean  validaDatos(String idUsuario,String contrasena) throws NoSuchAlgorithmException{
		boolean validado;
	
		MessageDigest md = MessageDigest.getInstance("SHA-256");
	    String text=contrasena;
        md.update(text.getBytes(StandardCharsets.UTF_8));
	    byte[] digest = md.digest();
        String hex = String.format("%064x", new BigInteger(1, digest));
	    System.out.println(hex);
	    
	    validado=servicioUsuario.validaUsuario(idUsuario, hex);
		if(validado==true){
			usuario=servicioUsuario.RecuperaUsuario(idUsuario);
			return true;
		}else{
			return false;
		}
	}//fin metodo
	
	public boolean MuestraVentanaPrincipal() throws IOException{
		ControlVentanaPrincipal controlVentanaPrincipal=new ControlVentanaPrincipal(usuario);
		controlVentanaPrincipal.iniciai();
		return true;
	}

}
