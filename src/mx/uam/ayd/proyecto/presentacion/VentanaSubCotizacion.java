package mx.uam.ayd.proyecto.presentacion;

import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * Ventana de la interfaz principal de las subcotizaciones.
 */
public class VentanaSubCotizacion extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private final Font fuenteTitulo = new Font("Arial Black", Font.BOLD, 16);
	private final Font fuenteBotones = new Font("Arial Black", Font.BOLD, 14);
	private final Font fuenteLBTXT = new Font("Arial Black", Font.BOLD, 12);
	private String[] columnas = {"No.","Tipo","Codigo","Cantidad","Costo","Venta","Proveedor"};

	private JPanel jContentPane = null;

	//Este incluye el folio
	private JLabel jLabelTitulo = null;
	//Datos del Empleado
	private JLabel jLabelDatosEmpleado = null;
	private JLabel jLabelUsuario = null;
	private JLabel jLabelNombre = null;
	private JLabel jLabelAPat = null;
	private JLabel jLabelAmat = null;
	private JLabel jLabelRFC = null;
	private JLabel jLabelDireccion = null;
	private JLabel jLabelTelefono = null;
	private JLabel jLabelCorreoElectronico = null;
	//Datos de la cotizacion
	private JLabel jLabelDatosSubCotizacion = null;
	private JLabel jLabelEstado = null;
	private JLabel jLabelNumPiezas = null;
	private JLabel jLabelSubCosto = null;
	private JLabel jLabelSubVenta = null;
	private JLabel jLabelPiezas = null;

	private JButton jButtonBorrar = null;
	private JButton jButtonGuardar = null;
	
	//Datos del Empleado
	private JTextField jTextFieldUsuario = null;
	private JTextField jTextFieldNombre = null;
	private JTextField jTextFieldAPat = null;
	private JTextField jTextFieldAmat = null;
	private JTextField jTextFieldRFC = null;
	private JTextField jTextFieldDireccion = null;
	private JTextField jTextFieldTelefono = null;
	private JTextField jTextFieldCorreoElectronico = null;
	//Datos de la cotizacion
	private JTextField jTextFieldEstado = null;
	private JTextField jTextFieldNumPiezas = null;
	private JTextField jTextFieldSubCosto = null;
	private JTextField jTextFieldSubVenta = null;
	
	private JComboBox jComboBoxEstado = null;
	
	private JScrollPane jScrollPanePiezas = null;
	private JTable jTablePiezas = null;

	private ControlSubCotizacion controlSub;

	/**
	 * This is the default constructor
	 * @param ctrl el control de la subcotizacion
	 */
	public VentanaSubCotizacion(ControlSubCotizacion ctrl) {
		super();
		initialize();
		controlSub = ctrl;
	}

	/**
	 * This method initializes this
	 */
	private void initialize() {
		this.setSize(580,600);
		this.setContentPane(getJContentPane());
		this.setTitle("REDITSYS - Sub Cotizacion");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			//JLabelTitulo
			jLabelTitulo = new JLabel("Cotizacion RDS1234-1",SwingConstants.CENTER);
			jLabelTitulo.setBounds(new Rectangle(155,10,270,30));
			jLabelTitulo.setFont(fuenteTitulo);
			//JLabelDatosEmpleado
			jLabelDatosEmpleado = new JLabel("Datos del Empleado:");
			jLabelDatosEmpleado.setBounds(new Rectangle(10,50,175,20));
			jLabelDatosEmpleado.setFont(fuenteBotones);
			//JLabelUsuario
			jLabelUsuario = new JLabel("Usuario:",SwingConstants.LEFT);
			jLabelUsuario.setBounds(new Rectangle(10,80,100,15));
			jLabelUsuario.setFont(fuenteLBTXT);
			jTextFieldUsuario = new JTextField();
			jTextFieldUsuario.setBounds(new Rectangle(10,95,100,20));
			jTextFieldUsuario.setFont(fuenteLBTXT);
			jTextFieldUsuario.setEnabled(false);
			//JLabelNombre
			jLabelNombre = new JLabel("Nombre:",SwingConstants.LEFT);
			jLabelNombre.setBounds(new Rectangle(160,80,100,15));
			jLabelNombre.setFont(fuenteLBTXT);
			jTextFieldNombre = new JTextField();
			jTextFieldNombre.setBounds(new Rectangle(160,95,100,20));
			jTextFieldNombre.setFont(fuenteLBTXT);
			jTextFieldNombre.setEnabled(false);
			//JLabelApat
			jLabelAPat = new JLabel("Apellido p.:",SwingConstants.LEFT);
			jLabelAPat.setBounds(new Rectangle(310,80,100,15));
			jLabelAPat.setFont(fuenteLBTXT);
			jTextFieldAPat = new JTextField();
			jTextFieldAPat.setBounds(new Rectangle(310,95,100,20));
			jTextFieldAPat.setFont(fuenteLBTXT);
			jTextFieldAPat.setEnabled(false);
			//JLabelAmat
			jLabelAmat = new JLabel("Apellido m.:",SwingConstants.LEFT);
			jLabelAmat.setBounds(new Rectangle(460,80,100,15));
			jLabelAmat.setFont(fuenteLBTXT);
			jTextFieldAmat = new JTextField();
			jTextFieldAmat.setBounds(new Rectangle(460,95,100,20));
			jTextFieldAmat.setFont(fuenteLBTXT);
			jTextFieldAmat.setEnabled(false);
			//RFC
			jLabelRFC = new JLabel("RFC:",SwingConstants.LEFT);
			jLabelRFC.setBounds(new Rectangle(10,130,60,15));
			jLabelRFC.setFont(fuenteLBTXT);
			jTextFieldRFC = new JTextField();
			jTextFieldRFC.setBounds(new Rectangle(10,145,100,20));
			jTextFieldRFC.setFont(fuenteLBTXT);
			jTextFieldRFC.setEnabled(false);
			//Telefono
			jLabelTelefono = new JLabel("Telefono:",SwingConstants.LEFT);
			jLabelTelefono.setBounds(new Rectangle(160,130,100,15));
			jLabelTelefono.setFont(fuenteLBTXT);
			jTextFieldTelefono = new JTextField();
			jTextFieldTelefono.setBounds(new Rectangle(160,145,100,20));
			jTextFieldTelefono.setFont(fuenteLBTXT);
			jTextFieldTelefono.setEnabled(false);
			//Correo
			jLabelCorreoElectronico = new JLabel("Correo:",SwingConstants.LEFT);
			jLabelCorreoElectronico.setBounds(new Rectangle(310,130,60,15));
			jLabelCorreoElectronico.setFont(fuenteLBTXT);
			jTextFieldCorreoElectronico = new JTextField();
			jTextFieldCorreoElectronico.setBounds(new Rectangle(310,145,250,20));
			jTextFieldCorreoElectronico.setFont(fuenteLBTXT);
			jTextFieldCorreoElectronico.setEnabled(false);
			//Direccion
			jLabelDireccion = new JLabel("Direccion:",SwingConstants.LEFT);
			jLabelDireccion.setBounds(new Rectangle(10,180,100,15));
			jLabelDireccion.setFont(fuenteLBTXT);
			jTextFieldDireccion = new JTextField();
			jTextFieldDireccion.setBounds(new Rectangle(10,195,250,20));
			jTextFieldDireccion.setFont(fuenteLBTXT);
			jTextFieldDireccion.setEnabled(false);
			//Datos de Sub Cotizacion
			jLabelDatosSubCotizacion = new JLabel("Datos de Sub Cotizacion:");
			jLabelDatosSubCotizacion.setBounds(new Rectangle(10,230,220,20));
			jLabelDatosSubCotizacion.setFont(fuenteBotones);
			//Datos de las SubCotizaciones
			jLabelPiezas = new JLabel("Piezas:");
			jLabelPiezas.setBounds(new Rectangle(10,260,200,20));
			jLabelPiezas.setFont(fuenteLBTXT);
			//JLabelEstado
			jLabelEstado = new JLabel("Estado:",SwingConstants.LEFT);
			jLabelEstado.setBounds(new Rectangle(10,460,60,15));
			jLabelEstado.setFont(fuenteLBTXT);
			jComboBoxEstado = new JComboBox();
			jComboBoxEstado.addItem("---");
			jComboBoxEstado.addItem("Nueva");
			jComboBoxEstado.addItem("Enviada");
			jComboBoxEstado.addItem("Aceptada");
			jComboBoxEstado.addItem("Rechazada");
			jComboBoxEstado.addItem("Pedida");
			jComboBoxEstado.addItem("Cerrada");
			jComboBoxEstado.setBounds(new Rectangle(10,475,100,20));
			jComboBoxEstado.setFont(fuenteLBTXT);
			//Fecha Inicio
			jLabelNumPiezas = new JLabel("Piezas:",SwingConstants.LEFT);
			jLabelNumPiezas.setBounds(new Rectangle(160,460,100,15));
			jLabelNumPiezas.setFont(fuenteLBTXT);
			jTextFieldNumPiezas = new JTextField();
			jTextFieldNumPiezas.setBounds(new Rectangle(160,475,100,20));
			jTextFieldNumPiezas.setFont(fuenteLBTXT);
			jTextFieldNumPiezas.setEnabled(false);
			//Fecha Objetivo
			jLabelSubCosto = new JLabel("Costo:",SwingConstants.LEFT);
			jLabelSubCosto.setBounds(new Rectangle(310,460,100,15));
			jLabelSubCosto.setFont(fuenteLBTXT);
			jTextFieldSubCosto = new JTextField();
			jTextFieldSubCosto.setBounds(new Rectangle(310,475,100,20));
			jTextFieldSubCosto.setFont(fuenteLBTXT);
			jTextFieldSubCosto.setEnabled(false);
			//Fecha Fin
			jLabelSubVenta = new JLabel("Venta:",SwingConstants.LEFT);
			jLabelSubVenta.setBounds(new Rectangle(460,460,100,15));
			jLabelSubVenta.setFont(fuenteLBTXT);
			jTextFieldSubVenta = new JTextField();
			jTextFieldSubVenta.setBounds(new Rectangle(460,475,100,20));
			jTextFieldSubVenta.setFont(fuenteLBTXT);
			jTextFieldSubVenta.setEnabled(false);
			//Agregar todo
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(jLabelTitulo, null);
			jContentPane.add(jLabelDatosEmpleado,null);
			jContentPane.add(jLabelUsuario,null);
			jContentPane.add(jTextFieldUsuario,null);
			jContentPane.add(jLabelNombre,null);
			jContentPane.add(jTextFieldNombre,null);
			jContentPane.add(jLabelAPat,null);
			jContentPane.add(jTextFieldAPat,null);
			jContentPane.add(jLabelAmat,null);
			jContentPane.add(jTextFieldAmat,null);
			jContentPane.add(jLabelRFC,null);
			jContentPane.add(jTextFieldRFC,null);
			jContentPane.add(jLabelDireccion,null);
			jContentPane.add(jTextFieldDireccion,null);
			jContentPane.add(jLabelTelefono,null);
			jContentPane.add(jTextFieldTelefono,null);
			jContentPane.add(jLabelCorreoElectronico,null);
			jContentPane.add(jTextFieldCorreoElectronico,null);
			jContentPane.add(jLabelDatosSubCotizacion,null);
			jContentPane.add(jLabelEstado,null);
			jContentPane.add(jComboBoxEstado,null);
			jContentPane.add(jLabelPiezas,null);
			jContentPane.add(jLabelNumPiezas,null);
			jContentPane.add(jTextFieldNumPiezas,null);
			jContentPane.add(jLabelSubCosto,null);
			jContentPane.add(jTextFieldSubCosto,null);
			jContentPane.add(jLabelSubVenta,null);
			jContentPane.add(jTextFieldSubVenta,null);
			jContentPane.add(getScrollPanePiezas(),null);
			jContentPane.add(getJButtonBorrar(), null);
			jContentPane.add(getJButtonGuardar(), null);
		}
		return jContentPane;
	}
	
	/**
	 * This method initializes jTablePiezas
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JScrollPane getScrollPanePiezas() {
		//DefaultTableModel modelo = new DefaultTableModel();
		jScrollPanePiezas = new JScrollPane(getTablePiezas());
		jScrollPanePiezas.setBounds(10,280,550,170);
		return jScrollPanePiezas;
	}
	
	private JTable getTablePiezas() {
		Object[][] filas = {
			{"1","Motor","1123","1","$3,500.00","$4,000.00","Toyota"},
			{"","","",null,"",null,""},
			{"","","",null,"",null,""},
			{"","","",null,"",null,""},
			{"","","",null,"",null,""},
			{"","","",null,"",null,""},
			{"","","",null,"",null,""},
			{"","","",null,"",null,""},
			{"","","",null,"",null,""},
		};

		jTablePiezas = new JTable(filas,columnas);
		jTablePiezas.setBounds(0,0,550,200);
		jTablePiezas.setFont(fuenteLBTXT);
		
		return jTablePiezas;
	}

	/**
	 * This method initializes jButtonBuscarCotizacionGlobal	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonGuardar() {
		if (jButtonGuardar == null) {
			jButtonGuardar = new JButton();
			jButtonGuardar.setBounds(new Rectangle(10, 520, 150, 30));
			jButtonGuardar.setText("Guardar");
			jButtonGuardar.setFont(fuenteBotones);
			jButtonGuardar.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Invoca al control principal
					controlSub.guardar();
				}
			});
		}
		return jButtonGuardar;
	}

	/**
	 * This method initializes jButtonCambioVentanaCotizaciones	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonBorrar() {
		if (jButtonBorrar == null) {
			jButtonBorrar = new JButton();
			jButtonBorrar.setBounds(new Rectangle(310, 520, 150, 30));
			jButtonBorrar.setText("Borrar");
			jButtonBorrar.setFont(fuenteBotones);
			jButtonBorrar.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					controlSub.borrar();
				}
			});
		}
		return jButtonBorrar;
	}
}
