package mx.uam.ayd.proyecto.presentacion;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import javax.swing.JMenuItem;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Ventana de la búsqueda de cotizaciones globales.
 */
public class VentanaLoginUsuario extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtIDDeUsuario;
	private JTextField txtContrasena;
	private JButton btnIngresar;
	private ControlLoginUsuario controlLoginUsuario;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 * @param controlU el control de login de usuario.
	 */
	public VentanaLoginUsuario(ControlLoginUsuario controlU) {
		setTitle("Reditsys");
		this.controlLoginUsuario=controlU;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtIDDeUsuario = new JTextField();
		txtIDDeUsuario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (txtIDDeUsuario.getText().equals("ID de Usuario")) {
					txtIDDeUsuario.setText("");
		            
		        }
			}
		});
		txtIDDeUsuario.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtIDDeUsuario.setText("");
			}
		});
		txtIDDeUsuario.setText("ID de Usuario");
		txtIDDeUsuario.setBounds(93, 105, 201, 20);
		contentPane.add(txtIDDeUsuario);
		txtIDDeUsuario.setColumns(10);
		
		txtContrasena = new JTextField();
		txtContrasena.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (txtContrasena.getText().equals("Contrasena")) {
					txtContrasena.setText("");
		            
		        }

			}
		});
		txtContrasena.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtContrasena.setText("");
			}
		});
		txtContrasena.setText("Contrasena");
		txtContrasena.setBounds(93, 146, 201, 20);
		contentPane.add(txtContrasena);
		txtContrasena.setColumns(10);
		
		btnIngresar = new JButton("Ingresar");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean validado=false;
				if(txtIDDeUsuario.getText().isEmpty()||txtContrasena.getText().isEmpty()){
					JOptionPane.showMessageDialog (null, "Es necesario llenar los campos");
				}//fin if
				else{
					try {
						validado=controlLoginUsuario.validaDatos(txtIDDeUsuario.getText(),txtContrasena.getText());
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(validado==false){
					JOptionPane.showMessageDialog (null, "Usuario o contrasea incorrectos");
				}
				else{
					try {
						controlLoginUsuario.MuestraVentanaPrincipal();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					dispose();
				}
			}
		});
		btnIngresar.setBounds(153, 198, 89, 23);
		contentPane.add(btnIngresar);
		
		JMenuItem mntmRedtisaSaDe = new JMenuItem("REDITSA S.A DE C.V");
		mntmRedtisaSaDe.setFont(new Font("Baskerville Old Face", Font.ITALIC, 27));
		mntmRedtisaSaDe.setBounds(46, 24, 314, 22);
		contentPane.add(mntmRedtisaSaDe);
		
		JMenuItem mntmIngreoAReditsys = new JMenuItem("Ingreo a REDITSYS");
		mntmIngreoAReditsys.setFont(new Font("Arabic Typesetting", Font.ITALIC, 25));
		mntmIngreoAReditsys.setBounds(93, 57, 183, 22);
		contentPane.add(mntmIngreoAReditsys);
	}
}

