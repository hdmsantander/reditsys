package mx.uam.ayd.proyecto.presentacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;

import mx.uam.ayd.proyecto.datos.DAOCotizacionGlobalBD;
import mx.uam.ayd.proyecto.datos.ManejadorBaseDatos;
import mx.uam.ayd.proyecto.negocio.ServicioCotizacionGlobalImpl;
import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;

/**
 * La clase ControlPrincipal controla la aplicacion
 *
 */
public class ControlBusquedaCotizacionGlobal {

	// Ventana principal
	private VentanaBusquedaCotizacionGlobal ventana;
	private ControlCotizacionGlobal controlGlobal;
	private ServicioCotizacionGlobalImpl servicioGlobal;
	private DAOCotizacionGlobalBD daoGlobal;
	
	//private ControlNuevaCotizacionGlobal controlNuevaCG;
	//private ControlListarCotizacionGlobal controlListarCG;
	
	private ControlCrearCotizacionGlobal controlCrearCotizacionGlobal;
		
	public ControlBusquedaCotizacionGlobal() {
		System.out.println("Control iniciado");
		daoGlobal = new DAOCotizacionGlobalBD();
		controlGlobal = new ControlCotizacionGlobal();
		servicioGlobal = new ServicioCotizacionGlobalImpl(daoGlobal);
	}
	
	public ControlBusquedaCotizacionGlobal(ControlCrearCotizacionGlobal controlCrearCotizacionGlobal) {
		System.out.println("Control iniciado");
		daoGlobal = new DAOCotizacionGlobalBD();
		controlGlobal = new ControlCotizacionGlobal();
		servicioGlobal = new ServicioCotizacionGlobalImpl(daoGlobal);
		this.controlCrearCotizacionGlobal = controlCrearCotizacionGlobal;
	}
	
	/**
	 * Arranca el control principal y por lo tanto la aplicacion.
	 *
	 */
	public void inicia() {
		// Crea la ventana y la muestra
		ventana = new VentanaBusquedaCotizacionGlobal(this,controlGlobal);
		ventana.setVisible(true);
		//ventana.setDefaultCloseOperation(ventana.EXIT_ON_CLOSE);	
	}

	/**
	 * Arranca la historia de usuario de crear cotizacion global
	 */
	public void agregarCotizacionGlobal() {
		//controlAgregarLibro.inicia();
		System.out.println("Boton Agregar Cotizacion");
		controlCrearCotizacionGlobal.inicia();		
	}

	/**
	 * Arranca la historia de usuario de buscar cotizacion global
	 */
	public void buscarCotizacionGlobal(String folio, String siniestro, String nombreCliente, String estado, String fechaInicio, String fechaFin) {
		System.out.println("Boton Buscar Cotizacion");
		if(estado.equals("---")) {
			estado = "";
		}
		String regexp = "\\d{4}-\\d{1,2}-\\d{1,2}";
		Date inicio = null;
		if(Pattern.matches(regexp, fechaInicio)) {
			try {
				//Anio, mes, dia
				Pattern patron = Pattern.compile("(\\d+)\\-(\\d+)-(\\d+)");
				Matcher matcher = patron.matcher(fechaInicio);
				matcher.find();
				inicio = new Date(Integer.parseInt(matcher.group(1)),Integer.parseInt(matcher.group(2)),Integer.parseInt(matcher.group(3)));
				//System.out.println("Coincide uwu");
			} catch(Exception e) {
				inicio = new Date(0,0,0);
			}
		}
		System.out.println("Fecha de inicio a enviar " + inicio);
		Date fin = null;
		if(Pattern.matches(regexp, fechaFin)) {
			try {
				//Anio, mes, dia
				Pattern patron = Pattern.compile("(\\d+)\\-(\\d+)-(\\d+)");
				Matcher matcher = patron.matcher(fechaFin);
				matcher.find();
				fin = new Date(Integer.parseInt(matcher.group(1)),Integer.parseInt(matcher.group(2)),Integer.parseInt(matcher.group(3)));
				//System.out.println("Coincide uwu");
			} catch(Exception e) {
				fin = new Date(0,0,0);
			}
		}
		System.out.println("Fecha de fin a enviar " + fin);
		ArrayList<CotizacionGlobal> coincidencias = servicioGlobal.buscaCotizacionGlobal(folio,siniestro,nombreCliente,estado,inicio,fin);
		/*Object[][] filas = {
			{"RDS1234","El Aguila","AG27064",3,"Nueva",new JButton("Ver mas")},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null}
		};*/
		
		Object[][] filas = new Object[coincidencias.size()][6];
		for(int i = 0; i < coincidencias.size();i++) {
			CotizacionGlobal coinc = coincidencias.get(i);
			filas[i][0] = coinc.getFolio();
			//filas[i][1] = coinc.getCliente().getNombre();
			filas[i][1] = "Nombre";
			filas[i][2] = coinc.getNumeroSiniestro();
			//filas[i][3] = coinc.getSubCotizaciones().length;
			filas[i][3] = 1;
			filas[i][4] = coinc.getEstado();
			filas[i][5] = new JButton("Ver mas");
		}
		ventana.muestraCoincidencias(filas);
	}
		
	/**
	 * Hace el cambio de la ventana de busqueda a la ventana principal
	 */
	public void muestraVentanaPrincipal() {
		//controlPrincipal.inicia();
		System.out.println("Haz el regreso a la ventana principal");
		
	}
	
	/**
	 * Limpia los campos de la Ventana de Busqueda
	 */
	public void reiniciaVentanaBusqueda() {
		System.out.println("Limpia campos");
		ventana.limpia();
		
	}
	
	/**
	 * Termina la aplicación
	 */
	public void termina() {
		ManejadorBaseDatos.shutdown();
		System.exit(0);
	}
	
}
