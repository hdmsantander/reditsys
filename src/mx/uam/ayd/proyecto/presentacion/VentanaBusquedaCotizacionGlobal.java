package mx.uam.ayd.proyecto.presentacion;


import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;
import javax.swing.JFrame;


import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Component;
import java.awt.Font;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class VentanaBusquedaCotizacionGlobal extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private final Font fuenteTitulo = new Font("Arial Black", Font.BOLD, 16);
	private final Font fuenteBotones = new Font("Arial Black", Font.BOLD, 14);
	private final Font fuenteLBTXT = new Font("Arial Black", Font.BOLD, 12);
	private final Class[] tiposColumnas = new Class[]{java.lang.String.class,java.lang.String.class,java.lang.String.class,int.class,java.lang.String.class,JButton.class};
	private final String[] columnas = {"Folio","Cliente","Siniestro","Sub Cotizaciones","Estado","Detalles"};

	private JPanel jContentPane = null;

	private JLabel jLabelTitulo = null;
	private JLabel jLabelImagen = null;
	private JLabel jLabelParametros = null;
	private JLabel jLabelFolio = null;
	private JLabel jLabelCliente = null;
	private JLabel jLabelSiniestro = null;
	private JLabel jLabelEstado = null;
	private JLabel jLabelFechaInicio = null;
	private JLabel jLabelFechaFin = null;
	private JLabel jLabelCoincidencias = null;

	private JButton jButtonCrearCotizacionGlobal = null;
	private JButton jButtonBuscarCotizacionGlobal = null;
	private JButton jButtonCambioVentanaCotizaciones = null;
	private JButton jButtonCambioVentanaPrincipal = null;
	
	private JTextField jTextFieldFolio = null;
	private JTextField jTextFieldCliente = null;
	private JTextField jTextFieldSiniestro = null;
	private JTextField jTextFieldFechaInicio = null;
	private JTextField jTextFieldFechaFin = null;
	
	private JComboBox jComboBoxEstado = null;
	
	private JScrollPane jScrollPaneCoincidencias = null;
	private JTable jTableCoincidencias = null;
	
	private ControlBusquedaCotizacionGlobal controlBusca;
	private ControlCotizacionGlobal controlGlobal;

	/**
	 * This is the default constructor
	 */
	public VentanaBusquedaCotizacionGlobal(ControlBusquedaCotizacionGlobal ctrl,ControlCotizacionGlobal ctrl2) {
		super();
		initialize();
		controlBusca = ctrl;
		controlGlobal = ctrl2;
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(800,600);
		this.setContentPane(getJContentPane());
		this.setTitle("REDITSYS - Cotizaciones");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			//JLabelTitulo
			jLabelTitulo = new JLabel("Busqueda de Cotizaciones",SwingConstants.CENTER);
			jLabelTitulo.setBounds(new Rectangle(340,10,270,30));
			jLabelTitulo.setFont(fuenteTitulo);
			//JLabelImagen
			jLabelImagen = new JLabel("Inserte\nLogo\nAqui",SwingConstants.CENTER);
			jLabelImagen.setBounds(new Rectangle(10,10,150,150));
			jLabelImagen.setFont(fuenteTitulo);
			//JLabelParametros
			jLabelParametros = new JLabel("Parametros de busqueda:",SwingConstants.LEFT);
			jLabelParametros.setBounds(new Rectangle(200,60,200,15));
			jLabelParametros.setFont(fuenteLBTXT);
			//JLabelFolio
			jLabelFolio = new JLabel("Folio:",SwingConstants.LEFT);
			jLabelFolio.setBounds(new Rectangle(200,90,50,15));
			jLabelFolio.setFont(fuenteLBTXT);
			jTextFieldFolio = new JTextField();
			jTextFieldFolio.setBounds(new Rectangle(250,88,100,20));
			jTextFieldFolio.setFont(fuenteLBTXT);
			//JLabelCliente
			jLabelCliente = new JLabel("Cliente:",SwingConstants.LEFT);
			jLabelCliente.setBounds(new Rectangle(400,90,60,15));
			jLabelCliente.setFont(fuenteLBTXT);
			jTextFieldCliente = new JTextField();
			jTextFieldCliente.setBounds(new Rectangle(460,88,100,20));
			jTextFieldCliente.setFont(fuenteLBTXT);
			//JLabelSiniestro
			jLabelSiniestro = new JLabel("Siniestro:",SwingConstants.LEFT);
			jLabelSiniestro.setBounds(new Rectangle(600,90,80,15));
			jLabelSiniestro.setFont(fuenteLBTXT);
			jTextFieldSiniestro = new JTextField();
			jTextFieldSiniestro.setBounds(new Rectangle(680,88,100,20));
			jTextFieldSiniestro.setFont(fuenteLBTXT);
			//JLabelEstado
			jLabelEstado = new JLabel("Estado:",SwingConstants.LEFT);
			jLabelEstado.setBounds(new Rectangle(200,120,60,15));
			jLabelEstado.setFont(fuenteLBTXT);
			jComboBoxEstado = new JComboBox();
			jComboBoxEstado.addItem("---");
			jComboBoxEstado.addItem("Nueva");
			jComboBoxEstado.addItem("Pendiente");
			jComboBoxEstado.addItem("Cerrada");
			jComboBoxEstado.setBounds(new Rectangle(260,118,100,20));
			jComboBoxEstado.setFont(fuenteLBTXT);
			//JLabelFechaInicio
			jLabelFechaInicio = new JLabel("Inicio:",SwingConstants.LEFT);
			jLabelFechaInicio.setBounds(new Rectangle(400,120,60,15));
			jLabelFechaInicio.setFont(fuenteLBTXT);
			jTextFieldFechaInicio = new JTextField("yyyy-mm-dd");
			jTextFieldFechaInicio.setBounds(new Rectangle(460,118,100,20));
			jTextFieldFechaInicio.setFont(fuenteLBTXT);
			jTextFieldFechaInicio.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					jTextFieldFechaInicio.setText("");
				}
			});
			//JLabelFechaFin
			jLabelFechaFin = new JLabel("Fin:",SwingConstants.LEFT);
			jLabelFechaFin.setBounds(new Rectangle(600,120,40,15));
			jLabelFechaFin.setFont(fuenteLBTXT);
			jTextFieldFechaFin = new JTextField("yyyy-mm-dd");
			jTextFieldFechaFin.setBounds(new Rectangle(640,118,100,20));
			jTextFieldFechaFin.setFont(fuenteLBTXT);
			jTextFieldFechaFin.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					jTextFieldFechaFin.setText("");
				}
			});
			//JLabelCoincidencias
			jLabelCoincidencias = new JLabel("Coincidencias encontradas:",SwingConstants.LEFT);
			jLabelCoincidencias.setBounds(new Rectangle(200,200,220,15));
			jLabelCoincidencias.setFont(fuenteLBTXT);
			//Agregar todo
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(jLabelTitulo, null);
			jContentPane.add(jLabelImagen,null);
			jContentPane.add(jLabelParametros,null);
			jContentPane.add(jLabelFolio,null);
			jContentPane.add(jTextFieldFolio,null);
			jContentPane.add(jLabelCliente,null);
			jContentPane.add(jTextFieldCliente,null);
			jContentPane.add(jLabelSiniestro,null);
			jContentPane.add(jTextFieldSiniestro,null);
			jContentPane.add(jLabelEstado,null);
			jContentPane.add(jComboBoxEstado,null);
			jContentPane.add(jLabelFechaInicio,null);
			jContentPane.add(jTextFieldFechaInicio,null);
			jContentPane.add(jLabelFechaFin,null);
			jContentPane.add(jTextFieldFechaFin,null);
			jContentPane.add(jLabelCoincidencias,null);
			jContentPane.add(getScrollPaneCoincidencias(),null);
			jContentPane.add(getJButtonCrearCotizacionGlobal(), null);
			jContentPane.add(getJButtonBuscarCotizacionGlobal(), null);
			jContentPane.add(getJButtonCambioVentanaCotizaciones(), null);
			jContentPane.add(getJButtonCambioVentanaPrincipal(), null);
		}
		return jContentPane;
	}
	
	/**
	 * This method initializes jTableCoincidencias
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JScrollPane getScrollPaneCoincidencias() {
		//DefaultTableModel modelo = new DefaultTableModel();
		jScrollPaneCoincidencias = new JScrollPane(getTableCoincidencias());
		jScrollPaneCoincidencias.setBounds(200,220,580,290);
		return jScrollPaneCoincidencias;
	}
	
	private JTable getTableCoincidencias() {
		jTableCoincidencias = new JTable();
		jTableCoincidencias.setBounds(0,0,580,300);
		jTableCoincidencias.setFont(fuenteLBTXT);
		Object[][] filas = {};
	    //Definicion del TableModel
		jTableCoincidencias.setModel(new javax.swing.table.DefaultTableModel(filas,columnas)); 

		return jTableCoincidencias;
	}

	/**
	 * This method initializes jButtonCrearCotizacionGlobal	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonCrearCotizacionGlobal() {
		if (jButtonCrearCotizacionGlobal == null) {
			jButtonCrearCotizacionGlobal = new JButton();
			jButtonCrearCotizacionGlobal.setBounds(new Rectangle(390, 520, 180, 30));
			jButtonCrearCotizacionGlobal.setText("Crear Cotizacion");
			jButtonCrearCotizacionGlobal.setFont(fuenteBotones);
			jButtonCrearCotizacionGlobal.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Invoca al control principal
					controlBusca.agregarCotizacionGlobal();
				}
			});
		}
		return jButtonCrearCotizacionGlobal;
	}

	/**
	 * This method initializes jButtonBuscarCotizacionGlobal	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonBuscarCotizacionGlobal() {
		if (jButtonBuscarCotizacionGlobal == null) {
			jButtonBuscarCotizacionGlobal = new JButton();
			jButtonBuscarCotizacionGlobal.setBounds(new Rectangle(430, 150, 100, 30));
			jButtonBuscarCotizacionGlobal.setText("Buscar");
			jButtonBuscarCotizacionGlobal.setFont(fuenteBotones);
			jButtonBuscarCotizacionGlobal.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Invoca al control principal
					String buscaFolio = jTextFieldFolio.getText();
					String buscaCliente = jTextFieldCliente.getText();
					String buscaSiniestro = jTextFieldSiniestro.getText();
					String buscaEstado = jComboBoxEstado.getSelectedItem().toString();
					String buscaInicio = jTextFieldFechaInicio.getText();
					String buscaFin = jTextFieldFechaFin.getText();
					controlBusca.buscarCotizacionGlobal(buscaFolio,buscaSiniestro,buscaCliente,buscaEstado,buscaInicio,buscaFin);
				}
			});
		}
		return jButtonBuscarCotizacionGlobal;
	}

	/**
	 * This method initializes jButtonCambioVentanaCotizaciones	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonCambioVentanaCotizaciones() {
		if (jButtonCambioVentanaCotizaciones == null) {
			jButtonCambioVentanaCotizaciones = new JButton();
			jButtonCambioVentanaCotizaciones.setBounds(new Rectangle(10, 210, 150, 30));
			jButtonCambioVentanaCotizaciones.setText("Cotizaciones");
			jButtonCambioVentanaCotizaciones.setFont(fuenteBotones);
			jButtonCambioVentanaCotizaciones.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Invoca al control principal
					controlBusca.reiniciaVentanaBusqueda();
				}
			});
		}
		return jButtonCambioVentanaCotizaciones;
	}

	/**
	 * This method initializes jButtonCambioVentanaPrincipal
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonCambioVentanaPrincipal() {
		if (jButtonCambioVentanaPrincipal == null) {
			jButtonCambioVentanaPrincipal = new JButton();
			jButtonCambioVentanaPrincipal.setBounds(new Rectangle(10, 170, 150, 30));
			jButtonCambioVentanaPrincipal.setText("Perfil");
			jButtonCambioVentanaPrincipal.setFont(fuenteBotones);
			jButtonCambioVentanaPrincipal.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Invoca al control principal
					controlBusca.muestraVentanaPrincipal();
				}
			});
		}
		return jButtonCambioVentanaPrincipal;
	}
	
	void muestraCoincidencias(Object[][] filas) {
		if(filas.length > 0) {
			jTableCoincidencias.setModel(new javax.swing.table.DefaultTableModel(filas,columnas) {
		    	//Permite conocer los tipos de cada columna en el TableModel
		        Class[] tipos = tiposColumnas;
		        @Override
		        public Class getColumnClass(int columnIndex) {
		            //Este metodo es invocado por el CellRenderer para saber que dibujar en la celda
		            return tipos[columnIndex];
		        }
	
		        @Override
		        public boolean isCellEditable(int row, int column) {
		            //Evita que la columna que contiene el boton se pueda editar
		            //return !(this.getColumnClass(column).equals(JButton.class));
		        	//Evita la edicion para cada celda
		        	return false;
		        }
		    });
	
		    // El objetivo de la siguiente línea es indicar el CellRenderer que será utilizado para dibujar el botón
		    jTableCoincidencias.setDefaultRenderer(JButton.class, new TableCellRenderer() {
		    public Component getTableCellRendererComponent(JTable jtable, Object objeto, boolean estaSeleccionado, boolean tieneElFoco, int fila, int columna) {
		            /**
		             * Este metodo retorna el objeto que se va a dibujar en la celda. Esto significa que se dibujará
		             * en la celda el objeto que devuelva el TableModel. También significa que este renderer permite
		             * dibujar cualquier objeto gráfico en la tabla, pues retorna el objeto tal y como lo recibe.
		             */
		            return (Component) objeto;
		        }
		    });
	
		    /**
		     * El listener permite saber cuando fue pulsada la celda que contiene el boton.
		     * Noten que el clic se captura sobre la JTable, no el sobre el JButton.
		     * Otra forma de hacer esto era poner un CellRenderer que hiciera parecer que la celda contiene un botón.
		     * Es posible capturar el clic del botón, pero el efecto es el mismo y esta es la forma mas simple
		     */
		    jTableCoincidencias.addMouseListener(new MouseAdapter() {
		        @Override
		        public void mouseClicked(MouseEvent e) {
		            int fila = jTableCoincidencias.rowAtPoint(e.getPoint());
		            int columna = jTableCoincidencias.columnAtPoint(e.getPoint());
	              /**
	               * Verifica si se hizo clic sobre la celda que contiene el boton, si hubiera mas de un boton 
	               * por fila habria que revisar tambien el contenido del boton o el nombre de la columna
		           */
		           if (jTableCoincidencias.getModel().getColumnClass(columna).equals(JButton.class)) {
		               //Para que se vea mas profesional, que diga cotizacion a la que se va a ingresar
		               StringBuilder sb = new StringBuilder();
		               for (int i = 0; i < jTableCoincidencias.getModel().getColumnCount(); i++) {
		                   if (!jTableCoincidencias.getModel().getColumnClass(i).equals(JButton.class)) {
		                        sb.append("\n").append(jTableCoincidencias.getModel().getColumnName(i)).append(": ").append(jTableCoincidencias.getModel().getValueAt(fila, i));
		                    }
		                }
		                JOptionPane.showMessageDialog(null, "Ingreso a la Cotizacion Global: " + sb.toString());
		                controlGlobal.inicia();
		            }
		        }
		    });
		    JOptionPane.showMessageDialog(null, "Se encontraron " + filas.length + " coincidencias", "Busqueda exitosa", JOptionPane.INFORMATION_MESSAGE);
		    jTableCoincidencias.repaint();
		} else {
			JOptionPane.showMessageDialog(null, "No se encontraron coincidencias, favor de verificar", "Error", JOptionPane.ERROR_MESSAGE);
			Object[][] filas2 = {};
			jTableCoincidencias.setModel(new javax.swing.table.DefaultTableModel(filas2,columnas));
			jTableCoincidencias.repaint();
		}
	}
	
	void limpia() {
		jTextFieldFolio.setText("");
		jTextFieldCliente.setText("");
		jTextFieldSiniestro.setText("");
		jComboBoxEstado.setSelectedItem("---");
		jTextFieldFechaInicio.setText("yyyy-mm-dd");
		jTextFieldFechaFin.setText("yyyy-mm-dd");
		Object[][] filas2 = {};
		jTableCoincidencias.setModel(new javax.swing.table.DefaultTableModel(filas2,columnas));
		jTableCoincidencias.repaint();
	}
	
}  //  @jve:decl-index=0:visual-constraint="10,10"
