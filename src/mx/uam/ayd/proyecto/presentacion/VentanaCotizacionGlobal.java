package mx.uam.ayd.proyecto.presentacion;

import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

/**
 * Ventana de la búsqueda de cotizaciones globales.
 */
public class VentanaCotizacionGlobal extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private final Font fuenteTitulo = new Font("Arial Black", Font.BOLD, 16);
	private final Font fuenteBotones = new Font("Arial Black", Font.BOLD, 14);
	private final Font fuenteLBTXT = new Font("Arial Black", Font.BOLD, 12);
	private final Class[] tiposColumnas = new Class[]{java.lang.String.class,java.lang.String.class,int.class,java.lang.String.class,java.lang.String.class,JButton.class};
	private String[] columnas = {"Sub-Folio","Empleado","Piezas","Proveedor","Estado","Detalles"};
	
	private JPanel jContentPane = null;

	//Este incluye el folio
	private JLabel jLabelTitulo = null;
	//Datos del Cliente
	private JLabel jLabelDatosCliente = null;
	private JLabel jLabelCliente = null;
	private JLabel jLabelCodigo = null;
	private JLabel jLabelRFC = null;
	private JLabel jLabelDireccion = null;
	private JLabel jLabelTelefono = null;
	private JLabel jLabelCorreoElectronico = null;
	//Datos de la cotizacion
	private JLabel jLabelDatosCotizacionGlobal = null;
	private JLabel jLabelSiniestro = null;
	private JLabel jLabelEstado = null;
	private JLabel jLabelFechaInicio = null;
	private JLabel jLabelFechaLimite = null;
	private JLabel jLabelFechaFin = null;
	private JLabel jLabelTotalPiezas = null;
	private JLabel jLabelCostoTotal = null;
	private JLabel jLabelVentaTotal = null;
	private JLabel jLabelMarcaAuto = null;
	private JLabel jLabelModeloAuto = null;
	private JLabel jLabelSubCotizaciones = null;

	private JButton jButtonCrearSubCotizacion = null;
	private JButton jButtonBorrar = null;
	private JButton jButtonGuardar = null;
	
	//Cliente
	private JTextField jTextFieldCliente = null;
	private JTextField jTextFieldCodigo = null;
	private JTextField jTextFieldRFC = null;
	private JTextField jTextFieldDireccion = null;
	private JTextField jTextFieldTelefono = null;
	private JTextField jTextFieldCorreoElectronico = null;
	//Ccotizacion
	private JTextField jTextFieldSiniestro = null;
	private JTextField jTextFieldFechaInicio = null;
	private JTextField jTextFieldFechaLimite = null;
	private JTextField jTextFieldFechaFin = null;
	private JTextField jTextFieldTotalPiezas = null;
	private JTextField jTextFieldCostoTotal = null;
	private JTextField jTextFieldVentaTotal = null;
	private JTextField jTextFieldMarcaAuto = null;
	private JTextField jTextFieldModeloAuto = null;
	
	private JComboBox jComboBoxEstado = null;
	
	private JScrollPane jScrollPaneSubCotizaciones = null;
	private JTable jTableSubCotizaciones = null;
	
	private ControlCotizacionGlobal controlGlobal;
	private ControlSubCotizacion controlSub;

	/**
	 * El constructor de la ventana cotizacion global.
	 * @param ctrl el control de la cotización global.
	 * @param ctrl2 el control de la subcotizacion-
	 */
	public VentanaCotizacionGlobal(ControlCotizacionGlobal ctrl,ControlSubCotizacion ctrl2) {
		super();
		initialize();
		controlGlobal = ctrl;
		controlSub = ctrl2;
	}

	/**
	 * This method initializes this
	 */
	private void initialize() {
		this.setSize(580,650);
		this.setContentPane(getJContentPane());
		this.setTitle("REDITSYS - Cotizacion Global");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			//JLabelTitulo
			jLabelTitulo = new JLabel("Cotizacion RDS1234",SwingConstants.CENTER);
			jLabelTitulo.setBounds(new Rectangle(155,10,270,30));
			jLabelTitulo.setFont(fuenteTitulo);
			//JLabelDatosCliente
			jLabelDatosCliente = new JLabel("Datos del Cliente:");
			jLabelDatosCliente.setBounds(new Rectangle(10,50,175,20));
			jLabelDatosCliente.setFont(fuenteBotones);
			//JLabelCliente
			jLabelCliente = new JLabel("Cliente:",SwingConstants.LEFT);
			jLabelCliente.setBounds(new Rectangle(10,80,60,15));
			jLabelCliente.setFont(fuenteLBTXT);
			jTextFieldCliente = new JTextField();
			jTextFieldCliente.setBounds(new Rectangle(10,95,100,20));
			jTextFieldCliente.setFont(fuenteLBTXT);
			jTextFieldCliente.setEnabled(false);
			//Codigo
			jLabelCodigo = new JLabel("Codigo:",SwingConstants.LEFT);
			jLabelCodigo.setBounds(new Rectangle(160,80,60,15));
			jLabelCodigo.setFont(fuenteLBTXT);
			jTextFieldCodigo = new JTextField();
			jTextFieldCodigo.setBounds(new Rectangle(160,95,100,20));
			jTextFieldCodigo.setFont(fuenteLBTXT);
			jTextFieldCodigo.setEnabled(false);
			//RFC
			jLabelRFC = new JLabel("RFC:",SwingConstants.LEFT);
			jLabelRFC.setBounds(new Rectangle(310,80,60,15));
			jLabelRFC.setFont(fuenteLBTXT);
			jTextFieldRFC = new JTextField();
			jTextFieldRFC.setBounds(new Rectangle(310,95,100,20));
			jTextFieldRFC.setFont(fuenteLBTXT);
			jTextFieldRFC.setEnabled(false);
			//Telefono
			jLabelTelefono = new JLabel("Telefono:",SwingConstants.LEFT);
			jLabelTelefono.setBounds(new Rectangle(460,80,100,15));
			jLabelTelefono.setFont(fuenteLBTXT);
			jTextFieldTelefono = new JTextField();
			jTextFieldTelefono.setBounds(new Rectangle(460,95,100,20));
			jTextFieldTelefono.setFont(fuenteLBTXT);
			jTextFieldTelefono.setEnabled(false);
			//Direccion
			jLabelDireccion = new JLabel("Direccion:",SwingConstants.LEFT);
			jLabelDireccion.setBounds(new Rectangle(10,130,100,15));
			jLabelDireccion.setFont(fuenteLBTXT);
			jTextFieldDireccion = new JTextField();
			jTextFieldDireccion.setBounds(new Rectangle(10,145,250,20));
			jTextFieldDireccion.setFont(fuenteLBTXT);
			jTextFieldDireccion.setEnabled(false);
			//Correo
			jLabelCorreoElectronico = new JLabel("Correo:",SwingConstants.LEFT);
			jLabelCorreoElectronico.setBounds(new Rectangle(310,130,60,15));
			jLabelCorreoElectronico.setFont(fuenteLBTXT);
			jTextFieldCorreoElectronico = new JTextField();
			jTextFieldCorreoElectronico.setBounds(new Rectangle(310,145,250,20));
			jTextFieldCorreoElectronico.setFont(fuenteLBTXT);
			jTextFieldCorreoElectronico.setEnabled(false);
			//Datos de Cotizacion Global
			jLabelDatosCotizacionGlobal = new JLabel("Datos de Cotizacion:");
			jLabelDatosCotizacionGlobal.setBounds(new Rectangle(10,180,200,20));
			jLabelDatosCotizacionGlobal.setFont(fuenteBotones);
			//JLabelSiniestro
			jLabelSiniestro = new JLabel("Siniestro:",SwingConstants.LEFT);
			jLabelSiniestro.setBounds(new Rectangle(10,210,100,15));
			jLabelSiniestro.setFont(fuenteLBTXT);
			jTextFieldSiniestro = new JTextField();
			jTextFieldSiniestro.setBounds(new Rectangle(10,225,100,20));
			jTextFieldSiniestro.setFont(fuenteLBTXT);
			jTextFieldSiniestro.setEnabled(false);
			//Fecha Inicio
			jLabelFechaInicio = new JLabel("Inicio:",SwingConstants.LEFT);
			jLabelFechaInicio.setBounds(new Rectangle(160,210,100,15));
			jLabelFechaInicio.setFont(fuenteLBTXT);
			jTextFieldFechaInicio = new JTextField();
			jTextFieldFechaInicio.setBounds(new Rectangle(160,225,100,20));
			jTextFieldFechaInicio.setFont(fuenteLBTXT);
			jTextFieldFechaInicio.setEnabled(false);
			//Fecha Objetivo
			jLabelFechaLimite = new JLabel("Objetivo:",SwingConstants.LEFT);
			jLabelFechaLimite.setBounds(new Rectangle(310,210,100,15));
			jLabelFechaLimite.setFont(fuenteLBTXT);
			jTextFieldFechaLimite = new JTextField();
			jTextFieldFechaLimite.setBounds(new Rectangle(310,225,100,20));
			jTextFieldFechaLimite.setFont(fuenteLBTXT);
			jTextFieldFechaLimite.setEnabled(false);
			//Fecha Fin
			jLabelFechaFin = new JLabel("Fin:",SwingConstants.LEFT);
			jLabelFechaFin.setBounds(new Rectangle(460,210,100,15));
			jLabelFechaFin.setFont(fuenteLBTXT);
			jTextFieldFechaFin = new JTextField();
			jTextFieldFechaFin.setBounds(new Rectangle(460,225,100,20));
			jTextFieldFechaFin.setFont(fuenteLBTXT);
			//Marca del Auto
			jLabelMarcaAuto = new JLabel("Marca:",SwingConstants.LEFT);
			jLabelMarcaAuto.setBounds(new Rectangle(10,260,100,15));
			jLabelMarcaAuto.setFont(fuenteLBTXT);
			jTextFieldMarcaAuto = new JTextField();
			jTextFieldMarcaAuto.setBounds(new Rectangle(10,275,100,20));
			jTextFieldMarcaAuto.setFont(fuenteLBTXT);
			jTextFieldMarcaAuto.setEnabled(false);
			//Modelo del Auto
			jLabelModeloAuto = new JLabel("Modelo:",SwingConstants.LEFT);
			jLabelModeloAuto.setBounds(new Rectangle(160,260,100,15));
			jLabelModeloAuto.setFont(fuenteLBTXT);
			jTextFieldModeloAuto = new JTextField();
			jTextFieldModeloAuto.setBounds(new Rectangle(160,275,100,20));
			jTextFieldModeloAuto.setFont(fuenteLBTXT);
			jTextFieldModeloAuto.setEnabled(false);
			//Datos de las SubCotizaciones
			jLabelSubCotizaciones = new JLabel("Sub Cotizaciones:");
			jLabelSubCotizaciones.setBounds(new Rectangle(10,310,200,20));
			jLabelSubCotizaciones.setFont(fuenteLBTXT);
			//JLabelEstado
			jLabelEstado = new JLabel("Estado:",SwingConstants.LEFT);
			jLabelEstado.setBounds(new Rectangle(10,510,60,15));
			jLabelEstado.setFont(fuenteLBTXT);
			jComboBoxEstado = new JComboBox();
			jComboBoxEstado.addItem("---");
			jComboBoxEstado.addItem("Nueva");
			jComboBoxEstado.addItem("Pendiente");
			jComboBoxEstado.addItem("Cerrada");
			jComboBoxEstado.setBounds(new Rectangle(10,525,100,20));
			jComboBoxEstado.setFont(fuenteLBTXT);
			//Fecha Inicio
			jLabelTotalPiezas = new JLabel("Piezas:",SwingConstants.LEFT);
			jLabelTotalPiezas.setBounds(new Rectangle(160,510,100,15));
			jLabelTotalPiezas.setFont(fuenteLBTXT);
			jTextFieldTotalPiezas = new JTextField();
			jTextFieldTotalPiezas.setBounds(new Rectangle(160,525,100,20));
			jTextFieldTotalPiezas.setFont(fuenteLBTXT);
			jTextFieldTotalPiezas.setEnabled(false);
			//Fecha Objetivo
			jLabelCostoTotal = new JLabel("Costo:",SwingConstants.LEFT);
			jLabelCostoTotal.setBounds(new Rectangle(310,510,100,15));
			jLabelCostoTotal.setFont(fuenteLBTXT);
			jTextFieldCostoTotal = new JTextField();
			jTextFieldCostoTotal.setBounds(new Rectangle(310,525,100,20));
			jTextFieldCostoTotal.setFont(fuenteLBTXT);
			jTextFieldCostoTotal.setEnabled(false);
			//Fecha Fin
			jLabelVentaTotal = new JLabel("Venta:",SwingConstants.LEFT);
			jLabelVentaTotal.setBounds(new Rectangle(460,510,100,15));
			jLabelVentaTotal.setFont(fuenteLBTXT);
			jTextFieldVentaTotal = new JTextField();
			jTextFieldVentaTotal.setBounds(new Rectangle(460,525,100,20));
			jTextFieldVentaTotal.setFont(fuenteLBTXT);
			jTextFieldVentaTotal.setEnabled(false);
			//Agregar todo
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(jLabelTitulo, null);
			jContentPane.add(jLabelDatosCliente,null);
			jContentPane.add(jLabelCliente,null);
			jContentPane.add(jTextFieldCliente,null);
			jContentPane.add(jLabelCodigo,null);
			jContentPane.add(jTextFieldCodigo,null);
			jContentPane.add(jLabelRFC,null);
			jContentPane.add(jTextFieldRFC,null);
			jContentPane.add(jLabelDireccion,null);
			jContentPane.add(jTextFieldDireccion,null);
			jContentPane.add(jLabelTelefono,null);
			jContentPane.add(jTextFieldTelefono,null);
			jContentPane.add(jLabelCorreoElectronico,null);
			jContentPane.add(jTextFieldCorreoElectronico,null);
			jContentPane.add(jLabelDatosCotizacionGlobal,null);
			jContentPane.add(jLabelSiniestro,null);
			jContentPane.add(jTextFieldSiniestro,null);
			jContentPane.add(jLabelFechaInicio,null);
			jContentPane.add(jTextFieldFechaInicio,null);
			jContentPane.add(jLabelFechaLimite,null);
			jContentPane.add(jTextFieldFechaLimite,null);
			jContentPane.add(jLabelFechaFin,null);
			jContentPane.add(jTextFieldFechaFin,null);
			jContentPane.add(jLabelMarcaAuto,null);
			jContentPane.add(jTextFieldMarcaAuto,null);
			jContentPane.add(jLabelModeloAuto,null);
			jContentPane.add(jTextFieldModeloAuto,null);
			jContentPane.add(jLabelSubCotizaciones,null);
			jContentPane.add(jLabelEstado,null);
			jContentPane.add(jComboBoxEstado,null);
			jContentPane.add(jLabelTotalPiezas,null);
			jContentPane.add(jTextFieldTotalPiezas,null);
			jContentPane.add(jLabelCostoTotal,null);
			jContentPane.add(jTextFieldCostoTotal,null);
			jContentPane.add(jLabelVentaTotal,null);
			jContentPane.add(jTextFieldVentaTotal,null);
			jContentPane.add(getScrollPaneSubCotizaciones(),null);
			jContentPane.add(getJButtonCrearSubCotizacion(), null);
			jContentPane.add(getJButtonBorrar(), null);
			jContentPane.add(getJButtonGuardar(), null);
		}
		return jContentPane;
	}
	
	/**
	 * This method initializes jTableSubCotizaciones
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JScrollPane getScrollPaneSubCotizaciones() {
		//DefaultTableModel modelo = new DefaultTableModel();
		jScrollPaneSubCotizaciones = new JScrollPane(getTableSubCotizaciones());
		jScrollPaneSubCotizaciones.setBounds(10,330,550,170);
		return jScrollPaneSubCotizaciones;
	}
	
	private JTable getTableSubCotizaciones() {
		jTableSubCotizaciones = new JTable();
		jTableSubCotizaciones.setBounds(0,0,550,200);
		jTableSubCotizaciones.setFont(fuenteLBTXT);
		Object[][] filas = {
			{"RDS1234-1","Juan",1,"Toyota","Aceptada",new JButton("Ver mas")},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
			{"","","",null,"",null},
		};
		
		jTableSubCotizaciones.setModel(new javax.swing.table.DefaultTableModel(filas,columnas) {
	        Class[] tipos = tiposColumnas;
	        @Override
	        public Class getColumnClass(int columnIndex) {
	            return tipos[columnIndex];
	        }

	        @Override
	        public boolean isCellEditable(int row, int column) {
	        	return false;
	        }
	    });

	    jTableSubCotizaciones.setDefaultRenderer(JButton.class, new TableCellRenderer() {
	    public Component getTableCellRendererComponent(JTable jtable, Object objeto, boolean estaSeleccionado, boolean tieneElFoco, int fila, int columna) {
	            return (Component) objeto;
	        }
	    });

	    jTableSubCotizaciones.addMouseListener(new MouseAdapter() {
	        @Override
	        public void mouseClicked(MouseEvent e) {
	            int fila = jTableSubCotizaciones.rowAtPoint(e.getPoint());
	            int columna = jTableSubCotizaciones.columnAtPoint(e.getPoint());
	           if (jTableSubCotizaciones.getModel().getColumnClass(columna).equals(JButton.class)) {
	               //Para que se vea mas profesional, que diga cotizacion a la que se va a ingresar
	               StringBuilder sb = new StringBuilder();
	               for (int i = 0; i < jTableSubCotizaciones.getModel().getColumnCount(); i++) {
	                   if (!jTableSubCotizaciones.getModel().getColumnClass(i).equals(JButton.class)) {
	                        sb.append("\n").append(jTableSubCotizaciones.getModel().getColumnName(i)).append(": ").append(jTableSubCotizaciones.getModel().getValueAt(fila, i));
	                    }
	                }
	                JOptionPane.showMessageDialog(null, "Ingreso a la Sub Cotizacion: " + sb.toString());
	                controlSub.inicia();
	            }
	        }
	    });
		
		return jTableSubCotizaciones;
	}

	/**
	 * This method initializes jButtonCrearCotizacionGlobal	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonCrearSubCotizacion() {
		if (jButtonCrearSubCotizacion == null) {
			jButtonCrearSubCotizacion = new JButton();
			jButtonCrearSubCotizacion.setBounds(new Rectangle(310, 270, 250, 30));
			jButtonCrearSubCotizacion.setText("Nueva Sub Cotizacion");
			jButtonCrearSubCotizacion.setFont(fuenteBotones);
			jButtonCrearSubCotizacion.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Invoca al control principal
					controlGlobal.agregarSubCotizacion();
				}
			});
		}
		return jButtonCrearSubCotizacion;
	}

	/**
	 * This method initializes jButtonBuscarCotizacionGlobal	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonGuardar() {
		if (jButtonGuardar == null) {
			jButtonGuardar = new JButton();
			jButtonGuardar.setBounds(new Rectangle(10, 570, 150, 30));
			jButtonGuardar.setText("Guardar");
			jButtonGuardar.setFont(fuenteBotones);
			jButtonGuardar.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Invoca al control principal
					controlGlobal.guardar();
				}
			});
		}
		return jButtonGuardar;
	}

	/**
	 * This method initializes jButtonCambioVentanaCotizaciones	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButtonBorrar() {
		if (jButtonBorrar == null) {
			jButtonBorrar = new JButton();
			jButtonBorrar.setBounds(new Rectangle(310, 570, 150, 30));
			jButtonBorrar.setText("Borrar");
			jButtonBorrar.setFont(fuenteBotones);
			jButtonBorrar.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					controlGlobal.borrar();
				}
			});
		}
		return jButtonBorrar;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"