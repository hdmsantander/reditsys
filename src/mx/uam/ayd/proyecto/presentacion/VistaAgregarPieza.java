package mx.uam.ayd.proyecto.presentacion;

import mx.uam.ayd.proyecto.negocio.dominio.TipoProducto;
import mx.uam.ayd.proyecto.negocio.dominio.Usuario;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

/**
 * Ventana de la interfaz para agregar piezas.
 */
public class VistaAgregarPieza extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	/**
	 * Componentes de Producto
	 * 
	 */
	private JLabel jlbProducto;
	private JLabel jlbNombreTP;
	private JLabel jlbNumeroProducto;
	private JLabel jlbCantidadProducto;
	private JLabel jblDescripcionProducto;

	private JComboBox<String> jcbNombresTiposProductos;
	private JTextField jtfNumeroProducto;
	private SpinnerModel spmCantidadProducto;
	private JSpinner jspCantidadProducto;
	private JTextArea jtaDescripcionProducto;
	private JScrollPane jspDescripcionProducto;

	/**
	 * Componentes de Empleado
	 * 
	 */	
	private JLabel jlbEmpleado;
	private JLabel jlbUsuario;
	private JLabel jlbNombreUsuario;
	
	private JComboBox<String> jcbIdsUsuarios;
	private JTextField jtfNombreUsuario;

	/**
	 * Botones
	 * 
	 */	
	private JButton jbtGuardar;
	private JButton jbtCancelar;

	/**
	 * Otros
	 * 
	 */
	private ControlCrearCotizacionGlobal controlCrearCotizacionGlobal;
	private ArrayList<Usuario> usuarios;
	private ArrayList<TipoProducto> tiposProductos;

	public VistaAgregarPieza(ControlCrearCotizacionGlobal controlCrearCotizacionGlobal, 
			ArrayList<Usuario> usuarios, ArrayList<TipoProducto> tiposProductos) {
		super();
		this.usuarios = usuarios;
		this.tiposProductos = tiposProductos;
		this.controlCrearCotizacionGlobal = controlCrearCotizacionGlobal;

		inicializa();
	}
	
	private void inicializa() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 330, 345);
		this.setTitle("REDITSYS - Agregar Pieza");
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);	
		contentPane.setLayout(null);
		
		agregaComponentesProducto();
		agregaComponentesEmpleado();
		agregaBotones();
	}

	private void agregaComponentesProducto(){
		jlbProducto = new JLabel();
		jlbProducto.setBounds(10, 0, 106, 39);
		jlbProducto.setFont(new Font("Arial Black", Font.BOLD, 14));
		jlbProducto.setText("Producto");
		contentPane.add(jlbProducto);
		
		jlbNombreTP = new JLabel("Nombre:");
		jlbNombreTP.setBounds(15, 35, 57, 14);
		contentPane.add(jlbNombreTP);

		jcbNombresTiposProductos = new JComboBox<String>();
		jcbNombresTiposProductos.setBounds(15, 55, 106, 20);

		for(int i = 0; i < tiposProductos.size(); i ++) {
			TipoProducto tipoProducto = tiposProductos.get(i);
			
			jcbNombresTiposProductos.addItem(tipoProducto.getNombre());
		}
		
		contentPane.add(jcbNombresTiposProductos);
		
		jlbNumeroProducto = new JLabel("No. Producto:");
		jlbNumeroProducto.setBounds(142, 35, 82, 14);
		contentPane.add(jlbNumeroProducto);
		
		jtfNumeroProducto = new JTextField();
		jtfNumeroProducto.setBounds(142, 55, 86, 20);
		contentPane.add(jtfNumeroProducto);
		jtfNumeroProducto.setColumns(10);
		
		jlbCantidadProducto = new JLabel("Cantidad:");
		jlbCantidadProducto.setBounds(250, 35, 54, 14);
		contentPane.add(jlbCantidadProducto);

	    spmCantidadProducto = new SpinnerNumberModel(1, 1, 25, 1);
    	jspCantidadProducto = new JSpinner(spmCantidadProducto);   
        jspCantidadProducto.setBounds(250,55,50,20);
        contentPane.add(jspCantidadProducto); 
        						
		jblDescripcionProducto = new JLabel("Descripcion:");
		jblDescripcionProducto.setBounds(15, 85, 96, 14);
		contentPane.add(jblDescripcionProducto);
		
		jtaDescripcionProducto = new JTextArea();
		//jtaDescripcionProducto.setBounds(15, 105, 289, 69);
		jspDescripcionProducto = new JScrollPane(jtaDescripcionProducto);
		jspDescripcionProducto.setBounds(15, 105, 289, 69);
		contentPane.add(jspDescripcionProducto);
	}

	private void agregaComponentesEmpleado(){		
		jlbEmpleado = new JLabel();
		jlbEmpleado.setBounds(10, 180, 106, 39);
		jlbEmpleado.setFont(new Font("Arial Black", Font.BOLD, 14));
		jlbEmpleado.setText("Empleado");
		contentPane.add(jlbEmpleado);		
		
		jlbNombreUsuario = new JLabel("Nombre:");
		jlbNombreUsuario.setBounds(120, 215, 74, 14);
		contentPane.add(jlbNombreUsuario);
		
		jtfNombreUsuario = new JTextField();
		jtfNombreUsuario.setBounds(120, 235, 184, 20);
		jtfNombreUsuario.setEditable(false) ;
		contentPane.add(jtfNombreUsuario);

		jlbUsuario = new JLabel("Usuario:");
		jlbUsuario.setBounds(15, 215, 57, 14);
		contentPane.add(jlbUsuario);
				
		jcbIdsUsuarios = new JComboBox<String>();
		jcbIdsUsuarios.setBounds(15, 235, 82, 20);

		for(int i = 0; i < usuarios.size(); i ++) {
			Usuario usuario = usuarios.get(i);
			
			jcbIdsUsuarios.addItem(usuario.getIdUsuario());
			
			if(i == 0) 
				jtfNombreUsuario.setText(usuario.getApellidoPaterno() + " " 
						+ usuario.getApellidoMaterno() + " "
						+ usuario.getNombre()
				);
		}
		
		jcbIdsUsuarios.addItemListener(new ItemListener() {
		      public void itemStateChanged(ItemEvent itemEvent) {	        
		        // 
		        if(itemEvent.getStateChange() == ItemEvent.SELECTED) {		        
					for(int i = 0; i < usuarios.size(); i ++) {
						Usuario usuario = usuarios.get(i);
						
						if(usuario.getIdUsuario().equals(itemEvent.getItem())) {
							jtfNombreUsuario.setText(usuario.getApellidoPaterno() + " " 
									+ usuario.getApellidoMaterno() + " "
									+ usuario.getNombre()
							);

							break;
						}
					}
		        }
		      }
		    });		 
		
		contentPane.add(jcbIdsUsuarios);
	}

	private void agregaBotones(){
		jbtGuardar = new JButton("Guardar");
		jbtGuardar.setBounds(117, 275, 89, 23);
		contentPane.add(jbtGuardar);
		
		jbtGuardar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String nombreProducto = String.valueOf(jcbNombresTiposProductos.getSelectedItem());
				String numeroProducto = jtfNumeroProducto.getText();
				String cantidadProducto = String.valueOf((Integer)jspCantidadProducto.getValue());
				String descripcionProducto = jtaDescripcionProducto.getText();
				String idUsuario = String.valueOf(jcbIdsUsuarios.getSelectedItem());

				controlCrearCotizacionGlobal.agregarNuevaPieza(nombreProducto, numeroProducto, cantidadProducto, 
						descripcionProducto, idUsuario);
			}
		});

		jbtCancelar = new JButton("Cancelar");
		jbtCancelar.setBounds(216, 275, 89, 23);
		contentPane.add(jbtCancelar);
				
		jbtCancelar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				controlCrearCotizacionGlobal.cerrarVistaAgregarPieza();
			}
		});
	}

	public void mostrarMensajeError(String mensaje){
		JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public void mostrarMensajeExito(String mensaje){
		JOptionPane.showMessageDialog(null, mensaje, "Exito", JOptionPane.INFORMATION_MESSAGE);
	}
}