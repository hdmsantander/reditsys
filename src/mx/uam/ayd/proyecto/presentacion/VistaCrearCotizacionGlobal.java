package mx.uam.ayd.proyecto.presentacion;

import mx.uam.ayd.proyecto.negocio.dominio.Cliente;
import mx.uam.ayd.proyecto.negocio.dominio.TipoProducto;
import mx.uam.ayd.proyecto.negocio.dominio.Usuario;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTable;

import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;

/**
 * Ventana de la interfaz para la creación de las cotizaciones globales.
 */
public class VistaCrearCotizacionGlobal extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	/**
	 * Componentes de Cliente
	 * 
	 */
	private JLabel jlCliente;
	private JLabel jlbNombreCliente;
	private JLabel jlbCodigoCliente;
	private JLabel jlbRfcCliente;
	private JLabel jlbDireccion;
	private JLabel lblTelefono;
	
	private JComboBox<String> jcbNombresClientes;
	private JTextField jtfCodigoCliente;
	private JTextField jtfRfcCliente;
	private JTextField jtfDireccionCliente;
	private JTextField jtfTelefonoCliente;

	/**
	 * Componentes de Cotizacion Global
	 * 
	 */
	private JLabel jlCotizacionGlobal;
	private JLabel jlbFolioCG;
	private JLabel jlbNumeroSiniestroCG;
	private JLabel jlbFechaCreacionCG;
	private JLabel jlbFechaLimiteCG;
	private JLabel jlbMarcaAutoCG;
	private JLabel jlbModeloAuto;

	private JTextField jtfFolioCG;
	private JTextField jtfNumeroSiniestroCG;
	private JTextField jtfFechaCreacionCG;
	private JTextField jtfFechaLimiteCG;
	private JTextField jtfMarcaAutoCG;
	private JTextField jtfModeloAuto;

	/**
	 * Componentes de la tabla de productos agregados
	 * 
	 */
	private String columnasTablaProductos[] = {"Numero", "Empleado", "Producto", "No. Producto", "Cantidad"};

	private JTable jtPtoductosAgregados;
	private JScrollPane jspProductosAgregados;

	/**
	 * Botones
	 * 
	 */
	private JButton jbtAgregarPieza;
	private JButton jbtGuardar;
	private JButton jbtCancelar;


	/**
	 * Otros
	 * 
	 */
	private ControlCrearCotizacionGlobal controlCrearCotizacionGlobal;
	private ArrayList<Cliente> clientes;
	
	public VistaCrearCotizacionGlobal(ControlCrearCotizacionGlobal controlCrearCotizacionGlobal, ArrayList<Cliente> clientes) {
		super();

		this.clientes = clientes;
		this.controlCrearCotizacionGlobal = controlCrearCotizacionGlobal;
		
		inicializa();
	}

	private void inicializa() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 460);
		this.setTitle("REDITSYS - Crear Cotizacion Global");

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		agregaComponentesCliente();
		agregaComponentesCotizacionGlobal();
		agregaBotones();
		agregaTablaProductos();
	}

	private void agregaComponentesCliente(){
		jlCliente = new JLabel();
		jlCliente.setBounds(10, 0, 106, 39);
		jlCliente.setFont(new Font("Arial Black", Font.BOLD, 14));
		jlCliente.setText("Cliente");
		contentPane.add(jlCliente);

		jlbCodigoCliente = new JLabel("Codigo:");
		jlbCodigoCliente.setBounds(175, 35, 86, 14);
		contentPane.add(jlbCodigoCliente);
		
		jtfCodigoCliente = new JTextField();
		jtfCodigoCliente.setBounds(175, 55, 72, 20);
		jtfCodigoCliente.setEditable(false);
		contentPane.add(jtfCodigoCliente);
		
		jlbRfcCliente = new JLabel("RFC:");
		jlbRfcCliente.setBounds(270, 35, 46, 14);
		contentPane.add(jlbRfcCliente);
		
		jtfRfcCliente = new JTextField();
		jtfRfcCliente.setBounds(270, 55, 115, 20);
		jtfRfcCliente.setEditable(false);
		contentPane.add(jtfRfcCliente);
		
		jlbDireccion = new JLabel("Direccion:");
		jlbDireccion.setBounds(15, 85, 86, 14);
		contentPane.add(jlbDireccion);

		jtfDireccionCliente = new JTextField();
		jtfDireccionCliente.setBounds(15, 105, 235, 20);
		jtfDireccionCliente.setEditable(false);
		contentPane.add(jtfDireccionCliente);
		
		lblTelefono = new JLabel("Telefono:");
		lblTelefono.setBounds(270, 85, 58, 14);
		contentPane.add(lblTelefono);
		
		jtfTelefonoCliente = new JTextField();
		jtfTelefonoCliente.setBounds(270, 105, 115, 20);
		jtfTelefonoCliente.setEditable(false);
		contentPane.add(jtfTelefonoCliente);
		
		jlbNombreCliente = new JLabel("Nombre:");
		jlbNombreCliente.setBounds(15, 35, 58, 14);
		contentPane.add(jlbNombreCliente);

		jcbNombresClientes = new JComboBox<String>();
		jcbNombresClientes.setBounds(15, 55, 140, 20);

		for(int i = 0; i < clientes.size(); i ++) {
			Cliente cliente = clientes.get(i);
			
			jcbNombresClientes.addItem(cliente.getNombre());
			
			if(i == 0) {
				jtfCodigoCliente.setText(cliente.getCodigoCliente());
				jtfRfcCliente.setText(cliente.getRfc());
				jtfDireccionCliente.setText(cliente.getDireccion());
				jtfTelefonoCliente.setText(cliente.getTelefono());
			}
		}

	    jcbNombresClientes.addItemListener(new ItemListener() {
	      public void itemStateChanged(ItemEvent itemEvent) {
	      	//
	        if(itemEvent.getStateChange() == ItemEvent.SELECTED) {		        
				for(int i = 0; i < clientes.size(); i ++) {
					Cliente cliente = clientes.get(i);
					
					if(cliente.getNombre().equals(itemEvent.getItem())) {
						jtfCodigoCliente.setText(cliente.getCodigoCliente());
						jtfRfcCliente.setText(cliente.getRfc());
						jtfDireccionCliente.setText(cliente.getDireccion());
						jtfTelefonoCliente.setText(cliente.getTelefono());
						break;
					}
				}
	        }
	      }
	    });
		
		contentPane.add(jcbNombresClientes);
	}

	private void agregaComponentesCotizacionGlobal(){
		jlCotizacionGlobal = new JLabel();
		jlCotizacionGlobal.setBounds(10, 130, 171, 39);
		jlCotizacionGlobal.setFont(new Font("Arial Black", Font.BOLD, 14));
		jlCotizacionGlobal.setText("Cotizacion Global");
		contentPane.add(jlCotizacionGlobal);
		
		jlbFolioCG = new JLabel("Folio:");
		jlbFolioCG.setBounds(15, 165, 46, 14);
		contentPane.add(jlbFolioCG);
		
		jtfFolioCG = new JTextField();
		jtfFolioCG.setBounds(15, 185, 86, 20);
		contentPane.add(jtfFolioCG);
		jtfFolioCG.setColumns(10);
		
		jlbNumeroSiniestroCG = new JLabel("No. siniestro:");
		jlbNumeroSiniestroCG.setBounds(120, 165, 86, 14);
		contentPane.add(jlbNumeroSiniestroCG);
		
		jtfNumeroSiniestroCG = new JTextField();
		jtfNumeroSiniestroCG.setBounds(120, 185, 86, 20);
		contentPane.add(jtfNumeroSiniestroCG);
		jtfNumeroSiniestroCG.setColumns(10);
		
		jlbFechaCreacionCG = new JLabel("Fecha creacion:");
		jlbFechaCreacionCG.setBounds(225, 165, 96, 14);
		contentPane.add(jlbFechaCreacionCG);
		
		jtfFechaCreacionCG = new JTextField();
		jtfFechaCreacionCG.setBounds(225, 185, 86, 20);
		contentPane.add(jtfFechaCreacionCG);
		jtfFechaCreacionCG.setColumns(10);
		
		jlbFechaLimiteCG = new JLabel("Fecha limite:");
		jlbFechaLimiteCG.setBounds(330, 165, 72, 14);
		contentPane.add(jlbFechaLimiteCG);
		
		jtfFechaLimiteCG = new JTextField();
		jtfFechaLimiteCG.setBounds(330, 185, 86, 20);
		contentPane.add(jtfFechaLimiteCG);
		jtfFechaLimiteCG.setColumns(10);
		
		jlbMarcaAutoCG = new JLabel("Marca:");
		jlbMarcaAutoCG.setBounds(15, 215, 72, 14);
		contentPane.add(jlbMarcaAutoCG);

		jtfMarcaAutoCG = new JTextField();
		jtfMarcaAutoCG.setBounds(15, 235, 86, 20);
		contentPane.add(jtfMarcaAutoCG);
		jtfMarcaAutoCG.setColumns(10);
		
		jlbModeloAuto = new JLabel("Modelo:");
		jlbModeloAuto.setBounds(120, 215, 76, 14);
		contentPane.add(jlbModeloAuto);
				
		jtfModeloAuto = new JTextField();
		jtfModeloAuto.setBounds(120, 235, 86, 20);
		contentPane.add(jtfModeloAuto);
		jtfModeloAuto.setColumns(10);
	}

	private void agregaBotones(){
		jbtAgregarPieza = new JButton("Agregar Pieza");
		jbtAgregarPieza.setBounds(15, 390, 115, 23);

		jbtAgregarPieza.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				controlCrearCotizacionGlobal.mostrarVistaAgregarPieza();
			}
		});

		contentPane.add(jbtAgregarPieza);

		jbtGuardar = new JButton("Guardar");
		jbtGuardar.setBounds(221, 390, 89, 23);
		
		jbtGuardar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String nombreCliente = String.valueOf(jcbNombresClientes.getSelectedItem());
				String folio = jtfFolioCG.getText();
				String numeroSiniestro = jtfNumeroSiniestroCG.getText();
				String fechaCreacion = jtfFechaCreacionCG.getText();
				String fechaLimite = jtfFechaLimiteCG.getText();
				String marcaAuto = jtfMarcaAutoCG.getText();
				String modeloAuto = jtfModeloAuto.getText();

				controlCrearCotizacionGlobal.crearCotizacionGlobal(nombreCliente, folio, numeroSiniestro, fechaCreacion, 
						fechaLimite, marcaAuto, modeloAuto);
			}
		});

		contentPane.add(jbtGuardar);

		jbtCancelar = new JButton("Cancelar");
		jbtCancelar.setBounds(326, 390, 89, 23);
		
		jbtCancelar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				controlCrearCotizacionGlobal.cerrarVistaCrearCotizacionGlobal();
			}
		});

		contentPane.add(jbtCancelar);
	}

	private void agregaTablaProductos(){
	    String filasTabla[][] = { 
	    	{"","","","",""},    
	    	{"","","","",""},    
	        {"","","","",""},
	        {"","","","",""},
	        {"","","","",""},
    	}; 

	    jtPtoductosAgregados = new JTable(filasTabla, columnasTablaProductos);
	    //jt.setBounds(100,100,200,300);    

	    jspProductosAgregados = new JScrollPane(jtPtoductosAgregados);	    
	    jspProductosAgregados.setBounds(15,273,400,100);
		contentPane.add(jspProductosAgregados);
	}
	
	public void actualizaTablaProductos(Hashtable<String, ArrayList<Object []>> listaProductosAgregados, int cantidadProductosAgregados){ 
		String filasTabla[][];
		int contadorProductos = 0;
		
		if(cantidadProductosAgregados < 5)
			filasTabla = new String[5][5];
		
		else
			filasTabla = new String[cantidadProductosAgregados][5];
		
	    Set<String> llaves = listaProductosAgregados.keySet();
        
        for(String idUsuario: llaves){
        	ArrayList<Object []> listaTemporal = listaProductosAgregados.get(idUsuario);
        	
        	for(int i = 0; i < listaTemporal.size(); i ++) {
        		Object datosTemporales[] = listaTemporal.get(i);
        		
        		Usuario usuario = (Usuario) datosTemporales[0];
        		TipoProducto tipoProducto = (TipoProducto) datosTemporales[1];
        		int numeroProducto = (Integer) datosTemporales[2];
        		int cantidadProducto = (Integer) datosTemporales[3];
        		String descripcionProducto = (String) datosTemporales[4];
        		
    	    	filasTabla[contadorProductos][0] = Integer.toString(contadorProductos + 1);
    	    	filasTabla[contadorProductos][1] = usuario.getIdUsuario();
    	    	filasTabla[contadorProductos][2] = tipoProducto.getNombre();
    	    	filasTabla[contadorProductos][3] = Integer.toString(numeroProducto);
    	    	filasTabla[contadorProductos][4] = Integer.toString(cantidadProducto);
    	    	
    	    	contadorProductos ++;
        	}
        }

        contentPane.remove(jspProductosAgregados);

	    jtPtoductosAgregados = new JTable(filasTabla, columnasTablaProductos);

	    jspProductosAgregados = new JScrollPane(jtPtoductosAgregados);	   
	    jspProductosAgregados.setBounds(15,273,400,100); 
		contentPane.add(jspProductosAgregados);
	}
	
	public void mostrarMensajeError(String mensaje){
		JOptionPane.showMessageDialog(null, mensaje, 
			"Error", JOptionPane.ERROR_MESSAGE);
	}

	public void mostrarMensajeExito(String mensaje){
		JOptionPane.showMessageDialog(null, mensaje, "Exito", JOptionPane.INFORMATION_MESSAGE);
	}
}