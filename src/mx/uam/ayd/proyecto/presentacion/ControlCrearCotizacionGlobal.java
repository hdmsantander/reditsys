package mx.uam.ayd.proyecto.presentacion;

import mx.uam.ayd.proyecto.negocio.dominio.Cliente;
import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;
import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;
import mx.uam.ayd.proyecto.negocio.dominio.Usuario;
import mx.uam.ayd.proyecto.negocio.dominio.TipoProducto;
import mx.uam.ayd.proyecto.negocio.dominio.Producto;

import mx.uam.ayd.proyecto.negocio.ServicioCotizacionGlobal;
import mx.uam.ayd.proyecto.datos.DAOCotizacionGlobalBD;
import mx.uam.ayd.proyecto.datos.DAOProductoBD;
import mx.uam.ayd.proyecto.datos.DAOSubCotizacionBD;
import mx.uam.ayd.proyecto.negocio.ServicioCliente;
import mx.uam.ayd.proyecto.negocio.ServicioUsuario;
import mx.uam.ayd.proyecto.negocio.ServicioTipoProducto;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.regex.Pattern;

public class ControlCrearCotizacionGlobal {
	
	// Servicios de la aplicacion
	private ServicioCotizacionGlobal servicioCotizacionGlobal = null;
	private ServicioCliente servicioCliente = null;
	private ServicioUsuario servicioUsuario = null;
	private ServicioTipoProducto servicioTipoProducto = null;
	
	// Vistas de la aplicacion
	private VistaCrearCotizacionGlobal vistaCrearCotizacionGlobal = null;
	private VistaAgregarPieza vistaAgregarPieza = null;
	
	private ArrayList<Cliente> clientes = null;
	private ArrayList<Usuario> usuarios = null;
	private ArrayList<TipoProducto> tiposProductos = null;
	private Hashtable<String, ArrayList<Object []>> listaProductosAgregados = null;
	private int cantidadProductosAgregados = 0;
	
	public ControlCrearCotizacionGlobal(ServicioCotizacionGlobal servicioCotizacionGlobal, ServicioCliente servicioCliente, 
			ServicioUsuario servicioUsuario, ServicioTipoProducto servicioTipoProducto) {
		this.servicioCotizacionGlobal = servicioCotizacionGlobal;
		this.servicioCliente = servicioCliente;
		this.servicioUsuario = servicioUsuario;
		this.servicioTipoProducto = servicioTipoProducto;
	}
	
	public void inicia() {
		inicializarListas();
		mostrarVistaCrearCotizacionGlobal();
	}

	private void inicializarListas(){
		clientes = servicioCliente.recuperaTodoClientes();
		usuarios = servicioUsuario.recuperaTodosUsuarios();
		tiposProductos = servicioTipoProducto.recuperaTodoTiposProducto();
		listaProductosAgregados = new Hashtable<String, ArrayList<Object []>>();		
	}
	
	private void mostrarVistaCrearCotizacionGlobal() {
		vistaCrearCotizacionGlobal = new VistaCrearCotizacionGlobal(this, clientes);
		vistaCrearCotizacionGlobal.setVisible(true);
	}
	
	public void cerrarVistaCrearCotizacionGlobal() {
		vistaCrearCotizacionGlobal.setVisible(false);
		vistaCrearCotizacionGlobal.dispose();
	}
	
	public void crearCotizacionGlobal(String nombreCliente, String folio, String numeroSiniestro, String fechaCreacion, 
			String fechaLimite, String marcaAuto, String modeloAuto) {
		boolean camposValidos = validadCamposVistaCrearCotizacionGlobal(nombreCliente, folio, numeroSiniestro, fechaCreacion, 
				fechaLimite, marcaAuto, modeloAuto);
		
		// Si todos los campos estan llenados correctamente
		if(camposValidos) {
			boolean existeCotizacionGlobal = servicioCotizacionGlobal.validaExisteCotizacionGlobal(folio);
			
			// Si ya existe una cotizaon global con ese folio
			if(existeCotizacionGlobal)
				vistaCrearCotizacionGlobal.mostrarMensajeError("Ya existe una Cotizacion Global con ese Folio");
			
			else {
				Cliente cliente = recuperaDatosCliente(nombreCliente);
				
				// Si no se encuentran los datos del cliente
				if(cliente == null)
					vistaCrearCotizacionGlobal.mostrarMensajeError("No se han podido encontrar los datos del Cliente");
				
				else {
					// Inserta la cotizacion global en la base de datos
					boolean cotizacionCreada = servicioCotizacionGlobal.creaCotizacionGlobal(cliente, folio, 
							numeroSiniestro, fechaCreacion, fechaLimite, marcaAuto, modeloAuto, 
							listaProductosAgregados, cantidadProductosAgregados);
					
					// Si ocurrio un error al crear la cotizacion global
					if(!cotizacionCreada)
						vistaCrearCotizacionGlobal.mostrarMensajeExito("Error al crear la Cotizacion Global");
					
					else {
						prueba(folio);
						vistaCrearCotizacionGlobal.mostrarMensajeExito("Cotizacion global creada correctamente");
						//cerrarVistaCrearCotizacionGlobal();
					}
				}
			}
		}
	}

	public void prueba(String folio){
		DAOCotizacionGlobalBD daoCotizacionGlobal = new DAOCotizacionGlobalBD();
		CotizacionGlobal cotizacionGlobal = daoCotizacionGlobal.recupera(folio);
		
		if(cotizacionGlobal != null) {
			System.out.println("Cotizacion Global");			
			System.out.println("Folio: " + cotizacionGlobal.getFolio());
			System.out.println("Estado: " + cotizacionGlobal.getEstado());
			System.out.println("No. siniestro: " + cotizacionGlobal.getNumeroSiniestro());
			System.out.println("Fecha creacion: " + cotizacionGlobal.getFechaCreacion());
			System.out.println("Fecha limite: " + cotizacionGlobal.getFechaLimite());
			System.out.println("Fecha entrega: " + cotizacionGlobal.getFechaEntrega());
			System.out.println("Numero de piezas totales: " + cotizacionGlobal.getNumeroPiezasTotal());
			System.out.println("Precio costo total: " + cotizacionGlobal.getPrecioCostoTotal());
			System.out.println("Precio venta total" + cotizacionGlobal.getPrecioVentaTotal());
			System.out.println("Marca: " + cotizacionGlobal.getMarcaAuto());
			System.out.println("Modelo: " + cotizacionGlobal.getModeloAuto());
		}
		
		DAOSubCotizacionBD daoSubCotizacion = new DAOSubCotizacionBD();
		DAOProductoBD daoproducto = new DAOProductoBD();
		//SubCotizacion subCotizacion = daoSubCotizacion.recupera(folio);
		
		ArrayList<SubCotizacion> subCotizacion = daoSubCotizacion.recuperaTodas(folio); 
		
		if(subCotizacion != null) {
			for(int i = 0; i < subCotizacion.size(); i ++){
				SubCotizacion sc = subCotizacion.get(i);
				System.out.println("\nSubCotizacion");
				System.out.println("Subfolio: " + sc.getSubFolio());
				System.out.println("Estado: " + sc.getEstado());
				System.out.println("Numero de piezas: " + sc.getNumeroPiezas());
				System.out.println("Precio costo subtotal: " + sc.getPrecioCostoSubTotal());
				System.out.println("Precio venta total: " + sc.getPrecioVentaSubTotal());
				
				ArrayList<Producto> producto = daoproducto.recuperaTodos(sc); 
				
				if(producto != null) {
					for(int j = 0; j < producto.size(); j ++){
						Producto p = producto.get(j);
						System.out.println("\nProducto");
						System.out.println("Numero producto: " + p.getNumeroProducto());
						System.out.println("Descripcion: " + p.getDescripcion());
						System.out.println("Cantidad: " + p.getCantidad());
						System.out.println("Precio venta unitario: " + p.getPrecioCostoUnitario());
						System.out.println("Precio costo unitario: " + p.getPrecioVentaUnitario());
					}				
				}

			}
		}
	}
	
	private boolean validadCamposVistaCrearCotizacionGlobal(String nombreCliente, String folio, String numeroSiniestro, 
			String fechaCreacion, String fechaLimite, String marcaAuto, String modeloAuto) {
		String regexpFecha = "\\d{4}-\\d{2}-\\d{2}";
		String regexpFolio = "\\w{1,7}";
		String regexpSiniestro = "\\w{1,15}";
		String regexpMarcaModelo = "\\w{1,20}";
		
		// Si alguno de los campos es null
		if(nombreCliente == null || folio == null || numeroSiniestro == null || fechaCreacion == null || 
				fechaLimite == null || marcaAuto == null ||	modeloAuto == null) {
			vistaCrearCotizacionGlobal.mostrarMensajeError("Por favor, llena todos los campos");
		
			return false;
		}
		
		// Si alguno de los campos se encuentra vacio
		if(nombreCliente.equals("") || folio.equals("") || numeroSiniestro.equals("") || fechaCreacion.equals("") || 
				fechaLimite.equals("") || marcaAuto.equals("") || modeloAuto.equals("")) {
			vistaCrearCotizacionGlobal.mostrarMensajeError("Por favor, llena todos los campos");
		
			return false;
		}
		
		else if(!Pattern.matches(regexpFecha, fechaCreacion) || !Pattern.matches(regexpFecha, fechaLimite) ) {
			vistaCrearCotizacionGlobal.mostrarMensajeError("Por favor, verifica los campos de fecha");
			
			return false;			
		}
		
		else if(!Pattern.matches(regexpFolio, folio)) {
			vistaCrearCotizacionGlobal.mostrarMensajeError("Por favor, verifica el Folio");
			
			return false;			
		}
		
		else if(!Pattern.matches(regexpSiniestro, numeroSiniestro)) {
			vistaCrearCotizacionGlobal.mostrarMensajeError("Por favor, verifica el No. Siniestro");
			
			return false;			
		}
		
		else if(!Pattern.matches(regexpMarcaModelo, marcaAuto) || !Pattern.matches(regexpMarcaModelo, modeloAuto)) {
			vistaCrearCotizacionGlobal.mostrarMensajeError("Por favor, verifica la marca y modelo");
			
			return false;			
		}
			
		return true;
	}
	
	private Cliente recuperaDatosCliente(String nombreCliente) {
		Cliente cliente = null;
		
		for(Cliente c : clientes) {
			if(nombreCliente.equals(c.getNombre())) {
				cliente = c;
				
				break;
			}
		}
		
		return cliente;
	}
	
	public void mostrarVistaAgregarPieza() {
		vistaAgregarPieza = new VistaAgregarPieza(this, usuarios, tiposProductos);
		vistaAgregarPieza.setVisible(true);
	}
	
	public void cerrarVistaAgregarPieza() {
		vistaAgregarPieza.setVisible(false);
		vistaAgregarPieza.dispose();
	}
	
	public void agregarNuevaPieza(String nombreProducto, String numeroProducto, String cantidadProducto, 
			String descripcionProducto, String idUsuario) {
		boolean camposValidos = validadCamposVistaAgregarPieza(nombreProducto, numeroProducto, cantidadProducto,
				descripcionProducto, idUsuario);
		
		// Si todos los campos estan llenados correctamente
		if(camposValidos) {
			Usuario usuario = recuperaDatosUsuario(idUsuario);
			TipoProducto tipoProducto = recuperaDatosTipoProducto(nombreProducto);
			
			ArrayList<Object []> listaTemporal;
			Object datosTemporales[] = new Object[5];
			
			// Si a este usuario ya se le asigno por lo menos un producto
			if(listaProductosAgregados.containsKey(idUsuario)) {
				// Recupero su array list asociado
				listaTemporal = listaProductosAgregados.get(idUsuario);
				
				datosTemporales[0] = usuario;
				datosTemporales[1] = tipoProducto;
				datosTemporales[2] = Integer.parseInt(numeroProducto);
				datosTemporales[3] = Integer.parseInt(cantidadProducto);
				datosTemporales[4] = descripcionProducto;
				
				listaTemporal.add(datosTemporales);
			}
			
			// Si a este usuario aun no se le ha asignado ningun producto
			else {
				listaTemporal = new ArrayList<Object []>();
				
				datosTemporales[0] = usuario;
				datosTemporales[1] = tipoProducto;
				datosTemporales[2] = Integer.parseInt(numeroProducto);
				datosTemporales[3] = Integer.parseInt(cantidadProducto);
				datosTemporales[4] = descripcionProducto;
				
				listaTemporal.add(datosTemporales);	
				
				listaProductosAgregados.put(idUsuario, listaTemporal);
			}
			
			cantidadProductosAgregados ++;
			
			vistaAgregarPieza.mostrarMensajeExito("Producto agregado correctamente");
			cerrarVistaAgregarPieza();

			vistaCrearCotizacionGlobal.actualizaTablaProductos(listaProductosAgregados, cantidadProductosAgregados);
		}
	}
	
	private Usuario recuperaDatosUsuario(String idUsuario) {
		Usuario usuario = null;
		
		for(Usuario u : usuarios) {
			if(idUsuario.equals(u.getIdUsuario())) {
				usuario = u;
				
				break;
			}
		}
		
		return usuario;
	}
	
	private TipoProducto recuperaDatosTipoProducto(String nombreProducto) {
		TipoProducto tipoProducto = null;
		
		for(TipoProducto tp : tiposProductos) {
			if(nombreProducto.equals(tp.getNombre())) {
				tipoProducto = tp;
				
				break;
			}
		}
		
		return tipoProducto;
	}
	
	private boolean validadCamposVistaAgregarPieza(String nombreProducto, String numeroProducto, 
			String cantidadProducto, String descripcionProducto, String idUsuario) {
		String regexNumeroProducto = "\\d{1,6}";
		String regexCantidadProducto = "\\d{1,2}";
		
		// Si algun campo es null
		if(nombreProducto == null || numeroProducto == null|| cantidadProducto == null||
				descripcionProducto == null|| idUsuario == null) {
			vistaAgregarPieza.mostrarMensajeError("Por favor, llena todos los campos");
			
			return false;
		}
		
		// Si algun campo se encuentra vacio
		else if(nombreProducto.equals("") || numeroProducto.equals("") || cantidadProducto.equals("") ||
				descripcionProducto.equals("") || idUsuario.equals("")) {
			vistaAgregarPieza.mostrarMensajeError("Por favor, llena todos los campos");
			
			return false;
		}
		
		else if(!Pattern.matches(regexNumeroProducto, numeroProducto) || !Pattern.matches(regexCantidadProducto, cantidadProducto) ) {
			vistaAgregarPieza.mostrarMensajeError("Por favor, verifica los campos numericos");
			
			return false;			
		}
				
		return true;
	}
}