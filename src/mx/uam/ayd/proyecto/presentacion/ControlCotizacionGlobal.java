package mx.uam.ayd.proyecto.presentacion;
import mx.uam.ayd.proyecto.datos.ManejadorBaseDatos;

/**
 * Control de la búsqueda de cotizaciones globales.
 */
public class ControlCotizacionGlobal {

	// Ventana principal
	private VentanaCotizacionGlobal ventana;
	private ControlSubCotizacion controlSub;
	
	//private ControlNuevaCotizacionGlobal controlNuevaCG;
	//private ControlListarCotizacionGlobal controlListarCG;
		

	public ControlCotizacionGlobal() {
		System.out.println("Control iniciado");
		controlSub = new ControlSubCotizacion();
	}
	
	/**
	 * Arranca el control principal y por lo tanto la aplicacion.
	 *
	 */
	public void inicia() {
		// Crea la ventana y la muestra
		ventana = new VentanaCotizacionGlobal(this,controlSub);
		ventana.setVisible(true);
		//ventana.setDefaultCloseOperation(ventana.EXIT_ON_CLOSE);	
	}

	/**
	 * Arranca la historia de usuario de crear cotizacion global
	 */
	public void agregarSubCotizacion() {
		//controlAgregarLibro.inicia();
		System.out.println("Boton Agregar Sub Cotizacion");
		
	}

	/**
	 * Arranca la historia de usuario de buscar cotizacion global
	 */
	public void guardar() {
		//controlAgregarLibro.inicia();
		System.out.println("Guarda cambios realizados");
		
	}
		
	/**
	 * Limpia los campos de la Ventana de Busqueda
	 */
	public void borrar() {
		//controlPrincipal.inicia();
		System.out.println("Borra la Cotizacion Global");
		
	}
	
	/**
	 * Termina la aplicación
	 */
	public void termina() {
		ManejadorBaseDatos.shutdown();
		System.exit(0);
	}
	
}
