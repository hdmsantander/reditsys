package mx.uam.ayd.proyecto.presentacion;
import mx.uam.ayd.proyecto.datos.ManejadorBaseDatos;

/**
 * Control de la interfaz principal de las sub cotizaciones.
 */
public class ControlSubCotizacion {
	// Ventana principal
	private VentanaSubCotizacion ventana;
	
	//private ControlNuevaCotizacionGlobal controlNuevaCG;
	//private ControlListarCotizacionGlobal controlListarCG;
		

	public ControlSubCotizacion() {
		System.out.println("Control iniciado");
	}
	
	/**
	 * Arranca el control principal y por lo tanto la aplicacion.
	 *
	 */
	public void inicia() {
		// Crea la ventana y la muestra
		ventana = new VentanaSubCotizacion(this);
		ventana.setVisible(true);
		//ventana.setDefaultCloseOperation(ventana.EXIT_ON_CLOSE);	
	}

	/**
	 * Arranca la historia de usuario de buscar cotizacion global
	 */
	public void guardar() {
		//controlAgregarLibro.inicia();
		System.out.println("Guarda cambios realizados");
		
	}
		
	/**
	 * Limpia los campos de la Ventana de Busqueda
	 */
	public void borrar() {
		//controlPrincipal.inicia();
		System.out.println("Borra la Sub Cotizacion");
		
	}
	
	/**
	 * Termina la aplicación
	 */
	public void termina() {
		ManejadorBaseDatos.shutdown();
		System.exit(0);
	}
}
