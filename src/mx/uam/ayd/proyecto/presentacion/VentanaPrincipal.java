package mx.uam.ayd.proyecto.presentacion;

import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import java.util.ArrayList;

import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;
import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.awt.Font;

/**
 * Ventana del menú principal de la aplicación.
 */
public class VentanaPrincipal extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ControlVentanaPrincipal controlVentanaPrincipal;
	private ArrayList<CotizacionGlobal> CotizacionesGlobales;
	private ArrayList<SubCotizacion> subCotizaciones;
	private JTable tabla1;
	private JPanel panel_2;
	private Object columnas[] = {"Folio","Cliente","Siniestro","SubCotizaciones","Estado","Detalles"};
	private Object filas[][];
	private ControlCotizacionGlobal controlGlobal = null;
	/**
	 * Launch the application.
	 */
	
	/*private void actualiza(){
		CotizacionesGlobales=controlVentanaPrincipal.TodasLasCotizacionesGlobales();
		Object[] fila = new Object[CotizacionesGlobales.size()];
		modelotabla = new DefaultTableModel(columnas,0);
		tabla1=new JTable(modelotabla);
		panel_2.add(tabla1);
		
		for(int i=0;i<CotizacionesGlobales.size();i++){
			 modelotabla.addRow(new Object[] {CotizacionesGlobales.get(i).getFolio(),CotizacionesGlobales.get(i).getCliente(),CotizacionesGlobales.get(i).getNumeroSiniestro(),CotizacionesGlobales.get(i).getSubCotizaciones().length,CotizacionesGlobales.get(i).getEstado(),""});
		}
	}*/

	/**
	 * Create the frame.
	 * @param controlV el control de la ventana principal
	 * @param ctrl el control de la cotizacion global
	 * @throws IOException excepción de entrada y salida
	 */
	public VentanaPrincipal(ControlVentanaPrincipal controlV, ControlCotizacionGlobal ctrl) throws IOException {
		
		this.controlVentanaPrincipal=controlV;
		this.controlGlobal = ctrl;
		setTitle("Reditsys");
		
	    
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 400, 1018, 650);
		setLocationByPlatform(true);
		getContentPane().setLayout(null);
		
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(10, 11, 185, 560);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		
		JMenuItem NombreUsuario = new JMenuItem("Nombre:"+controlVentanaPrincipal.DameNombreUsuario());
		NombreUsuario.setBackground(Color.LIGHT_GRAY);
		NombreUsuario.setBounds(10, 133, 165, 22);
		panel.add(NombreUsuario);
		
		JMenuItem IDusuario = new JMenuItem("ID:"+controlVentanaPrincipal.DameIDUsuario());
		IDusuario.setBackground(Color.LIGHT_GRAY);
		IDusuario.setBounds(10, 178, 129, 22);
		panel.add(IDusuario);
		
		JButton btnCotizaciones = new JButton("Cotizaciones");
		btnCotizaciones.setBounds(10, 231, 165, 23);
		panel.add(btnCotizaciones);
		
		
		
	
		
		ImageIcon Img = new ImageIcon(getClass().getResource("kubirk.jpg"));
		Image foto=Img.getImage();
		Image newimg = foto.getScaledInstance(80, 80,  java.awt.Image.SCALE_SMOOTH);
		Img = new ImageIcon(newimg);
		JLabel imagen=new JLabel(Img);
		JPanel panel_3 = new JPanel();
		
		panel_3.add(imagen);
		panel_3.setBounds(45, 18, 80, 80);
		panel.add(panel_3);
		
	
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(205, 11, 792, 560);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JMenuItem mntmNombre = new JMenuItem("Nombre:"+controlVentanaPrincipal.DameNombreUsuario());
		mntmNombre.setFont(new Font("Aharoni", Font.PLAIN, 13));
		mntmNombre.setBackground(Color.WHITE);
		mntmNombre.setBounds(173, 106, 157, 22);
		panel_1.add(mntmNombre);
		
		JMenuItem mntmApellido = new JMenuItem("Apellido:"+controlVentanaPrincipal.DameApellidoPaternoUsuario());
		mntmApellido.setFont(new Font("Aharoni", Font.PLAIN, 14));
		mntmApellido.setBounds(340, 106, 194, 22);
		panel_1.add(mntmApellido);
		
		JMenuItem mntmNumeroDeEmpleado = new JMenuItem("Numero de Empleado:"+controlVentanaPrincipal.DameIDUsuario());
		mntmNumeroDeEmpleado.setBounds(544, 106, 248, 22);
		panel_1.add(mntmNumeroDeEmpleado);
		
		JMenuItem mntmTelefono = new JMenuItem("Telefono:"+controlVentanaPrincipal.DameTelefonoUsuario());
		mntmTelefono.setBounds(340, 159, 194, 22);
		panel_1.add(mntmTelefono);
		
		JMenuItem mntmCorreo = new JMenuItem("Correo:"+controlVentanaPrincipal.DameCorreoUsuario());
		mntmCorreo.setBounds(544, 159, 226, 22);
		panel_1.add(mntmCorreo);
		
		
		JMenuItem mntmCotizacionesGlobales = new JMenuItem("Cotizaciones Globales");
		mntmCotizacionesGlobales.setBounds(32, 199, 157, 22);
		panel_1.add(mntmCotizacionesGlobales);
		
		JButton btnCerrarSesin = new JButton("Cerrar Sesi\u00F3n");
		btnCerrarSesin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controlVentanaPrincipal.CerrarSesion();
				dispose();
			}
		});
		btnCerrarSesin.setBounds(646, 537, 124, 23);
		panel_1.add(btnCerrarSesin);
		
		panel_2 = new JPanel();
		panel_2.setBounds(42, 231, 740, 295);
		panel_1.add(panel_2);
		panel_2.setLayout(null);
		
		
		
		CotizacionesGlobales=controlVentanaPrincipal.TodasLasCotizacionesGlobalesNuevas();
		
		if(CotizacionesGlobales!=null){
		filas=new Object[CotizacionesGlobales.size()][6];
		JButton[] enlaces=new JButton[CotizacionesGlobales.size()];
		
		for(int i=0;i<CotizacionesGlobales.size();i++){
			    enlaces[i]=new JButton("Ver mas");
				filas[i][0]=CotizacionesGlobales.get(i).getFolio();
				filas[i][1]=CotizacionesGlobales.get(i).getCliente();
				filas[i][2]=CotizacionesGlobales.get(i).getNumeroSiniestro();
				filas[i][3]=controlVentanaPrincipal.TodasLasSubCotizaciones(CotizacionesGlobales.get(i).getFolio()).size();
				filas[i][4]=CotizacionesGlobales.get(i).getEstado();
				filas[i][5]=enlaces[i];
		}
		
       
		tabla1=new JTable(filas,columnas);
		
		tabla1.setCellSelectionEnabled(true);
		tabla1.setColumnSelectionAllowed(true);
		JScrollPane scroll=new JScrollPane(tabla1);
		scroll.setBounds(10, 11, 720, 273);
		panel_2.add(scroll);
		}
		
	    foto=Img.getImage();
	    newimg = foto.getScaledInstance(136, 154,  java.awt.Image.SCALE_SMOOTH);
		Img = new ImageIcon(newimg);
		imagen=new JLabel(Img);
		
		JPanel panel_4 = new JPanel();
		panel_4.add(imagen);
		panel_4.setBounds(32, 23, 136, 154);
		panel_1.add(panel_4);
	}
}
