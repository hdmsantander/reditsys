package mx.uam.ayd.proyecto.presentacion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import mx.uam.ayd.proyecto.datos.DAOCotizacionGlobal;
import mx.uam.ayd.proyecto.datos.DAOCotizacionGlobalBD;
import mx.uam.ayd.proyecto.datos.DAOSubCotizacion;
import mx.uam.ayd.proyecto.datos.DAOSubCotizacionBD;
import mx.uam.ayd.proyecto.negocio.ServicioCotizacionGlobal;
import mx.uam.ayd.proyecto.negocio.ServicioCotizacionGlobalImpl;
import mx.uam.ayd.proyecto.negocio.ServicioSubCotizacion;
import mx.uam.ayd.proyecto.negocio.ServicioSubCotizacionImpl;
import mx.uam.ayd.proyecto.negocio.dominio.Usuario;
import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;
import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;
import mx.uam.ayd.proyecto.Aplicacion;

/**
 * Control de la búsqueda de cotizaciones globales.
 */
public class ControlVentanaPrincipal {
	
	private Usuario usuario;
	private DAOCotizacionGlobal daoCotizacionGlobal=new DAOCotizacionGlobalBD();
	private DAOSubCotizacion daoSubCotizacion = new DAOSubCotizacionBD();
	private ServicioCotizacionGlobal servicioCotizacionGlobal=new ServicioCotizacionGlobalImpl(daoCotizacionGlobal);
	private ServicioSubCotizacion servicioSubCotizacion = new ServicioSubCotizacionImpl(daoSubCotizacion, daoCotizacionGlobal);
	private Aplicacion aplicacion;
	private ControlCotizacionGlobal controlGlobal=new ControlCotizacionGlobal();

	public ControlVentanaPrincipal(Usuario u) {
		// TODO Auto-generated constructor stub
		this.usuario=u;
	}

	/**
	 * @throws IOException  cuando hay errores.
	 */
	public void iniciai() throws IOException{
		
		VentanaPrincipal ventanaPrincipal=new VentanaPrincipal(this,controlGlobal);
		ventanaPrincipal.setVisible(true);
	}
	
	public String DameNombreUsuario(){
		
		String nombre=usuario.getNombre();
		return nombre;
	}
	
	public String DameApellidoPaternoUsuario(){
		
		String apellidoPaterno=usuario.getApellidoPaterno();
		return apellidoPaterno;
		
	}
	
	public String DameApellidoMaternoUsuario(){
		String apellidoMaterno=usuario.getApellidoMaterno();
		return apellidoMaterno;
	}
	
	
	
	public String DameIDUsuario(){
		String idusuario=usuario.getIdUsuario();
		return idusuario;
		
	}
	
	public String DameTelefonoUsuario(){
		String telefono=usuario.getTelefono();
		return telefono;
	}
	
	public String DameCorreoUsuario(){
		String correo=usuario.getCorreoElectronico();
		return correo;
	}
	
	public  ArrayList<CotizacionGlobal> TodasLasCotizacionesGlobalesNuevas(){
		ArrayList<CotizacionGlobal> cotizacionesGlobales=new ArrayList<CotizacionGlobal>();
		cotizacionesGlobales=servicioCotizacionGlobal.RecuperaTodasCotizacionesNuevas("Nueva");
		if(cotizacionesGlobales!=null){
		return cotizacionesGlobales;
		}else{
			return null;
		}
	}
	
	public ArrayList<SubCotizacion> TodasLasSubCotizaciones(String folioGlobal) {
		ArrayList<SubCotizacion> subCotizaciones = new ArrayList<SubCotizacion>();
		subCotizaciones = servicioSubCotizacion.recuperaTodas(folioGlobal);
		return subCotizaciones;
	}
	
	public boolean CerrarSesion(){
		aplicacion.main(null);
		return true;
	}

}