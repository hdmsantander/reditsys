package test.mx.uam.ayd.proyecto.negocio;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Clase para las pruebas unitarias de ServicioUsuario.
 */
class ServicioUsuarioImplTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@Test
	void testServicioUsuarioImpl() {
	}

	@Test
	void testComparaContrasena() {
	}

	@Test
	void testValidaUsuario() {
	}

	@Test
	void testRecuperaTodosUsuarios() {
	}

	@Test
	void testActualizaUsuario() {
	}

	@Test
	void testBorraUsuario() {
	}

	@Test
	void testCrearU() {
	}

}
