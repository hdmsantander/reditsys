package test.mx.uam.ayd.proyecto.negocio;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Clase para las pruebas unitarias de ServicioTipoProducto.
 */
class ServicioTipoProductoImplTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@Test
	void testServicioTipoProductoImpl() {
	}

	@Test
	void testRecuperaTodoTiposProducto() {
	}

	@Test
	void testCrea() {
	}

	@Test
	void testActualizaTipoProducto() {
	}

	@Test
	void testBorraTipoProducto() {
	}

}
