package test.mx.uam.ayd.proyecto.negocio;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Clase para las pruebas unitarias de ServicioSubCotizacion.
 */
class ServicioSubCotizacionImplTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@Test
	void testServicioSubCotizacionImpl() {
	}

	@Test
	void testBuscarSubCotizaciones() {
	}

	@Test
	void testRecuperaSubCotizacion() {
	}

	@Test
	void testRecuperaCotizacion() {
	}

	@Test
	void testRecuperaYActualizaEstado() {
	}

	@Test
	void testActualizaEstado() {
	}

	@Test
	void testBorraSubCotizacion() {
	}

	@Test
	void testObtenListaProductos() {
	}

	@Test
	void testValidaSubCotizacionCerrada() {
	}

	@Test
	void testActualizaSubCotizacion() {
	}

}
