package test.mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import mx.uam.ayd.proyecto.datos.DAOSubCotizacion;
import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;

/**
 * Mock para la interfaz DAOSubCotizacion.
 */
public class DAOSubCotizacionMock implements DAOSubCotizacion {

	private Map <String, SubCotizacion> subCotizaciones = new HashMap <String, SubCotizacion>();

	@Override
	public boolean crea(SubCotizacion subCotizacion) {
		// Revisamos que no exista ya una entrada con ese identificador
		boolean existeSubCotizacion = subCotizaciones.containsKey(subCotizacion.getSubFolio());
		
		// Si no existe, la agregamos
		if (!existeSubCotizacion ) {
			subCotizaciones.put(subCotizacion.getSubFolio(), subCotizacion);
		}
		
		// Si no existía, regresamos verdadero, en otro caso falso.
		return (existeSubCotizacion == false) ? true:false;
	}

	@Override
	public SubCotizacion recupera(String subFolio) {
		SubCotizacion subCotizacion = subCotizaciones.get(subFolio);
		
		return (subCotizacion!=null) ? subCotizacion:null;
	}

	@Override
	public boolean actualiza(SubCotizacion subCotizacion) {
		// Revisamos que exista ya una entrada con ese identificador
		boolean existeSubCotizacion = subCotizaciones.containsKey(subCotizacion.getSubFolio());
		
		// Si existe, la actualizamos
		if (existeSubCotizacion) {
			subCotizaciones.put(subCotizacion.getSubFolio(), subCotizacion);
		}

		// Para que se comporte como la implementación de DAOUsuarioBD, regresamos true
		// Dado que no se marca un error en la consulta SQL si no existe el usuario.
		return true;
	}

	@Override
	public boolean borra(SubCotizacion subCotizacion) {
		subCotizaciones.remove(subCotizacion.getSubFolio());
		
		// Para que se comporte como la implementación de DAOUsuarioBD, regresamos true
		// Dado que no se marca un error en la consulta SQL si no existe el usuario.
		return true;
	}

	@Override
	public ArrayList<SubCotizacion> recuperaTodas() {
		Collection<SubCotizacion> coleccion = subCotizaciones.values();
		ArrayList<SubCotizacion> resultado = new ArrayList<SubCotizacion>(coleccion);
		return (!resultado.isEmpty()) ? resultado:null;  
	}

	@Override
	public ArrayList<SubCotizacion> recuperaTodas(String folioCotizacionGlobal) {
		int i;
		SubCotizacion subCotizacion;
		Collection<SubCotizacion> coleccion = subCotizaciones.values();
		ArrayList<SubCotizacion> resultadoUno = new ArrayList<SubCotizacion>(coleccion);
		ArrayList<SubCotizacion> resultado = new ArrayList<SubCotizacion>();
		for (i = 0 ; i < resultadoUno.size() ; i++) {
			subCotizacion = resultadoUno.get(i);
			if (!subCotizacion.getCotizacionGlobal().getFolio().equals(folioCotizacionGlobal)) {
				resultado.add(subCotizacion);
			}
		}		
		return (!resultado.isEmpty()) ? resultado:null; 
	}
}
