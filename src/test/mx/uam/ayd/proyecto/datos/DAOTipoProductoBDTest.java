package test.mx.uam.ayd.proyecto.datos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Clase para las pruebas unitarias de DAOTipoProducto.
 */
class DAOTipoProductoBDTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@Test
	void testCrea() {
	}

	@Test
	void testRecupera() {
	}

	@Test
	void testActualiza() {
	}

	@Test
	void testBorra() {
	}

	@Test
	void testRecuperaTodos() {
	}

}
