package test.mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import mx.uam.ayd.proyecto.datos.DAOCliente;
import mx.uam.ayd.proyecto.negocio.dominio.Cliente;

/**
 * Mock para la interfaz DAOCliente.
 */
public class DAOClienteMock implements DAOCliente {
	
	private Map <String, Cliente> clientes = new HashMap <String, Cliente>();

	@Override
	public boolean crea(Cliente cliente) {
		// Revisamos que no exista ya una entrada con ese identificador
		boolean existeCliente = clientes.containsKey(cliente.getCodigoCliente());
		
		// Si no existe, la agregamos
		if (!existeCliente ) {
			clientes.put(cliente.getCodigoCliente(), cliente);
		}
		
		// Si no existía, regresamos verdadero, en otro caso falso.
		return (existeCliente == false) ? true:false;
	}

	@Override
	public Cliente recupera(String codigoClient) {
		Cliente cliente = clientes.get(codigoClient);
		
		return (cliente!=null) ? cliente:null;
	}

	@Override
	public boolean actualiza(Cliente cliente) {
		// Revisamos que exista ya una entrada con ese identificador
		boolean existeCliente = clientes.containsKey(cliente.getCodigoCliente());
		
		// Si existe, la actualizamos
		if (existeCliente ) {
			clientes.put(cliente.getCodigoCliente(), cliente);
		}

		// Para que se comporte como la implementación de DAOUsuarioBD, regresamos true
		// Dado que no se marca un error en la consulta SQL si no existe el usuario.
		return true;
	}

	@Override
	public boolean borra(Cliente cliente) {
		clientes.remove(cliente.getCodigoCliente());
		
		// Para que se comporte como la implementación de DAOUsuarioBD, regresamos true
		// Dado que no se marca un error en la consulta SQL si no existe el usuario.
		return true;
	}

	@Override
	public ArrayList<Cliente> recuperaTodos() {
		Collection<Cliente> coleccion = clientes.values();
		ArrayList<Cliente> resultado = new ArrayList<Cliente>(coleccion);
		return (!resultado.isEmpty()) ? resultado:null;  
	}

}