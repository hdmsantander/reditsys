package test.mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import mx.uam.ayd.proyecto.datos.DAOProducto;
import mx.uam.ayd.proyecto.negocio.dominio.Producto;
import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;

/**
 * Mock para la interfaz DAOProducto.
 */
public class DAOProductoMock implements DAOProducto {

	private Map <Integer, Producto> productos = new HashMap <Integer, Producto>();
	private Map<String, SubCotizacion> subCotizaciones = new HashMap<String, SubCotizacion>();

	@Override
	public boolean crea(Producto producto) {
		// Revisamos que no exista ya una entrada con ese identificador
		boolean existeProducto = productos.containsKey(producto.getNumeroProducto());
		boolean existeSubCotizacion = subCotizaciones.containsKey(producto.getSubCotizacion().getSubFolio());
		
		if (!existeSubCotizacion && !existeSubCotizacion ) {
			productos.put(producto.getNumeroProducto(), producto);
			//subCotizaciones.put(subCotizacion.getSubFolio(), subCotizacion);
		}		
		
		// Si no existía, regresamos verdadero, en otro caso falso.
		return ((existeProducto == false) || (existeSubCotizacion == false)) ? true:false;
	}

	@Override
	public Producto recupera(int numeroProducto) {
		Producto producto = productos.get(numeroProducto);
		
		return (producto!=null) ? producto:null;
	}

	@Override
	public boolean actualiza(Producto producto) { 
		// Revisamos que exista ya una entrada con ese identificador
		boolean existeProducto = productos.containsKey(producto.getNumeroProducto());
		
		// Si existe, la actualizamos
		if (existeProducto) {
			productos.put(producto.getNumeroProducto(), producto);
		}

		// Para que se comporte como la implementación de DAOUsuarioBD, regresamos true
		// Dado que no se marca un error en la consulta SQL si no existe el usuario.
		return true;
	}

	@Override
	public boolean borra(Producto producto) {
		productos.remove(producto.getNumeroProducto());
		
		// Para que se comporte como la implementación de DAOUsuarioBD, regresamos true
		// Dado que no se marca un error en la consulta SQL si no existe el usuario.
		return true;
	}

	@Override
	public ArrayList<Producto> recuperaTodos(SubCotizacion subCotizacion) {
		int i;
		Producto producto;
		Collection<Producto> coleccion = productos.values();
		ArrayList<Producto> resultadoUno = new ArrayList<Producto>(coleccion);
		ArrayList<Producto> resultado = new ArrayList<Producto>();
		for (i = 0 ; i < resultadoUno.size() ; i++) {
			producto = resultadoUno.get(i);
			if (!subCotizacion.getCotizacionGlobal().getFolio().equals(subCotizacion.getSubFolio())) {
				resultado.add(producto);
			}
		}		
		return (!resultado.isEmpty()) ? resultado:null; 
	}
	
	@Override
	public ArrayList<Producto> recuperaTodos() {
		Collection<Producto> coleccion = productos.values();
		ArrayList<Producto> resultado = new ArrayList<Producto>(coleccion);
		
		return (!resultado.isEmpty()) ? resultado:null; 
	}
}
