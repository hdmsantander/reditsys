package test.mx.uam.ayd.proyecto.datos;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import mx.uam.ayd.proyecto.datos.DAOUsuario;
import mx.uam.ayd.proyecto.datos.DAOUsuarioBD;
import mx.uam.ayd.proyecto.datos.ManejadorBaseDatos;
import mx.uam.ayd.proyecto.negocio.dominio.Usuario;

/**
 * Clase para las pruebas unitarias de DAOUsuario.
 */
class DAOUsuarioBDTest {
	
	// Creando la instancia de logger para registrar los eventos.
	static Logger log = Logger.getRootLogger();
	
	// Creando la instancia de DAOUsuarioBD para realizar las pruebas.
	DAOUsuario dao = new DAOUsuarioBD();
	
	// Valores usados a lo largo de la prueba unitaria.
	static String nombreClase = "DAOUsuarioBD";
	static String llavePrimaria = "id_usuario";
	static String nombreBaseDatos = "APP.USUARIO";
	static String columnasDeDatos = "id_usuario, nombre, apellido_paterno, apellido_materno, rfc, direccion, telefono, correo_electronico, contrasena, privilegiado";
	
	// Comenzando con las precondiciones.
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
		log.info("Comenzando con las precondiciones de la prueba unitaria de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		String usuarioARecuperar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('recuperadotest', 'nombre', 'paterno', 'materno', '12345679', 'direccion', '5539393939', 'recuperadot@test.com', '17AD5CBB086511CEED018A7EB2CA9F5F435BCE86858F97169B44AA2737BCBDE7', 1)";
		String usuarioABorrar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('borradotest', 'nombre', 'paterno', 'materno', '12345679', 'direccion', '5539393939', 'borradotest@t.com', '17AD5CBB086511CEED018A7EB2CA9F5F435BCE86858F97169B44AA2737BCBDE7', 1)";
		String usuarioAActualizar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('actualizadotest', 'nombre', 'paterno', 'materno', '12345679', 'direccion', '5539393939', 'borradotest@t.com', '17AD5CBB086511CEED018A7EB2CA9F5F435BCE86858F97169B44AA2737BCBDE7', 1)";
		String usuarioRecuperarTodos1 = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('rectodos1', 'nombre', 'paterno', 'materno', '12345679', 'direccion', '5539393939', 'borradotest@t.com', '17AD5CBB086511CEED018A7EB2CA9F5F435BCE86858F97169B44AA2737BCBDE7', 1)";
		String usuarioRecuperarTodos2 ="INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('rectodos2', 'nombre', 'paterno', 'materno', '12345679', 'direccion', '5539393939', 'borradotest@t.com', '17AD5CBB086511CEED018A7EB2CA9F5F435BCE86858F97169B44AA2737BCBDE7', 1)";

		log.info("Agregando el usuario a recuperar para las pruebas unitarias");
		statement.execute(usuarioARecuperar);
		
		log.info("Agregando el usuario a borrar para las pruebas unitarias");
		statement.execute(usuarioABorrar);
		
		log.info("Agregando el usuario a actualizar para las pruebas unitarias");
		statement.execute(usuarioAActualizar);
		
		log.info("Agregando un usuario a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(usuarioRecuperarTodos1);
		
		log.info("Agregando un usuario a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(usuarioRecuperarTodos2);
	}
	
	// Revirtiendo las precondiciones a el estado incial de la base de datos.
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		
		log.info("Comenzando con el tearDown de la prueba unitaria de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		String borraUsuarioRecuperado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'recuperadotest'";
		String borraUsuarioBorrado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'borradotest'";
		String borraUsuarioCreado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'creadotest'";
		String borraUsuarioActualizado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'actualizadotest'";
		String borraUsuarioRecuperaTodos1 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'rectodos1'";
		String borraUsuarioRecuperaTodos2 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'rectodos2'";

		log.info("Borrando el usuario agregado para la prueba recupera");
		statement.execute(borraUsuarioRecuperado);
		log.info("Borrando el usuario agregado para la prueba borra");
		statement.execute(borraUsuarioBorrado);
		log.info("Borrando el usuario agregado para la prueba actualiza");
		statement.execute(borraUsuarioActualizado);
		log.info("Borrando el usuario agregado para la prueba recuperatodos");
		statement.execute(borraUsuarioRecuperaTodos1);
		log.info("Borrando el usuario agregado para la prueba recuperatodos");
		statement.execute(borraUsuarioRecuperaTodos2);
		
		// En caso de que la creación sea exitosa, se creó un usuario llamado creadotest, lo borramos en caso de estar ahí
		log.info("Borrando el usuario agregado por la prueba crea");
		statement.execute(borraUsuarioCreado);
	}
	
	/* 
	 * Comenzando con las pruebas unitarias.
	 */

	@Test
	public void testCrea() throws Exception { 
		log.info("Comenzando con la prueba del método Crea de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		// Creando un usuario que se agrega para la prueba de creado.
		Usuario usuario = new Usuario();
		usuario.setIdUsuario("creadotest");
		dao.crea(usuario);		

		// Revisando que esté en la base de datos. 
		String consultaUsuarioCreado = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'creadotest'";
		
		// Obteniendo el rst correspondiente a la query hecha al sistema del usuario creado por el DAO.
		ResultSet rst = statement.executeQuery(consultaUsuarioCreado);
		
		// Si hay un resultado en el rst, devuelve true, falso si no. 
		boolean hayUnResultado = rst.next();
		
		assertEquals(hayUnResultado,true);	
	}

	@Test
	public void testRecupera() throws Exception {
		log.info("Comenzando con la prueba del método Recupera de " + nombreClase);
		
		Usuario usuario = dao.recupera("recuperadotest");
		
		boolean hayUnResultado = (usuario != null)? true:false;
		
		assertEquals(hayUnResultado,true);
	}

	@Test
	public void testBorra() throws Exception {
		log.info("Comenzando con la prueba del método Borra de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		// Creando un usuario para pasárselo como argumento a borra.
		Usuario usuario = new Usuario();
		usuario.setIdUsuario("borradotest");
		
		dao.borra(usuario);
		
		// Revisando que ya no esté en la base de datos.
		String consultaUsuarioBorrado = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'borradotest'";
		
		// Obteniendo el rst correspondiente a la query hecha al sistema del usuario creado por el DAO.
		ResultSet rst = statement.executeQuery(consultaUsuarioBorrado);
		
		// Si hay un resultado en el rst, devuelve true, falso si no. 
		boolean hayUnResultado = rst.next();
		
		assertEquals(hayUnResultado,false);	
				
	}

	@Test
	public void testRecuperaTodos() {
		log.info("Comenzando con la prueba del método RecuperaTodos de " + nombreClase);
		
		// Creando un ArrayList para guardar el resultado de la consulta.
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		
		usuarios = dao.recuperaTodos();
		
		boolean elArregloTieneElementos = (usuarios != null) ? true:false;
		
		assertEquals(elArregloTieneElementos,true);		
	}

	@Test
	public void testActualiza() throws Exception {
		log.info("Comenzando con la prueba del método Actualiza de " + nombreClase);
		
		boolean usuarioActualizado;
		
		// Creando un usuario que ya existe en la base de datos
		Usuario usuario = new Usuario("actualizadotest", "nombrecambiado", "paterno", "materno", "12345679", "direccion", "5539393939", "borradotest@t.com", "17AD5CBB086511CEED018A7EB2CA9F5F435BCE86858F97169B44AA2737BCBDE7", true);
		
		// Pasándoselo al método actualiza.
		boolean resultado = dao.actualiza(usuario);
		
		if (resultado == true) {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			// revisando que esté actualizado el usuario en la base de datos.
			String consultaUsuarioCreado = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'actualizadotest'";
			
			// Obteniendo el rst correspondiente a la query hecha al sistema del usuario creado por el DAO.
			ResultSet rst = statement.executeQuery(consultaUsuarioCreado);
			
			rst.next();
			
			String nombre = rst.getString("nombre");
			
			if (nombre.equals("nombrecambiado"))
				usuarioActualizado = true;
			else
				usuarioActualizado = false;
		}
		else
			usuarioActualizado = false;
		assertEquals(usuarioActualizado,true);		
	}
	
}
