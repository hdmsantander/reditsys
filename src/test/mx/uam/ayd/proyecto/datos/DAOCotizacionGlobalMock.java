package test.mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import mx.uam.ayd.proyecto.datos.DAOCotizacionGlobal;
import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;

/**
 * Mock para la interfaz DAOCotizacionGlobal.
 */
public class DAOCotizacionGlobalMock implements DAOCotizacionGlobal {

	private Map <String, CotizacionGlobal> cotizacionesGlobales = new HashMap <String, CotizacionGlobal>();

	@Override
	public boolean crea(CotizacionGlobal cotizacionGlobal) {
		// Revisamos que no exista ya una entrada con ese identificador
		boolean existeCotizacionGlobal = cotizacionesGlobales.containsKey(cotizacionGlobal.getFolio());
		
		// Si no existe, la agregamos
		if (!existeCotizacionGlobal ) {
			cotizacionesGlobales.put(cotizacionGlobal.getFolio(), cotizacionGlobal);
		}
		
		// Si no existía, regresamos verdadero, en otro caso falso.
		return (existeCotizacionGlobal == false) ? true:false;
	}

	@Override
	public CotizacionGlobal recupera(String folio) { 
		CotizacionGlobal cotizacionGlobal = cotizacionesGlobales.get(folio);
		
		return (cotizacionGlobal!=null) ? cotizacionGlobal:null;
	}

	@Override
	public boolean actualiza(CotizacionGlobal cotizacionGlobal) {
		// Revisamos que exista ya una entrada con ese identificador
		boolean existeCotizacionGlobal = cotizacionesGlobales.containsKey(cotizacionGlobal.getFolio());
		
		// Si existe, la actualizamos
		if (existeCotizacionGlobal) {
			cotizacionesGlobales.put(cotizacionGlobal.getFolio(), cotizacionGlobal);
		}

		// Para que se comporte como la implementación de DAOUsuarioBD, regresamos true
		// Dado que no se marca un error en la consulta SQL si no existe el usuario.
		return true;
	}

	@Override
	public boolean borra(CotizacionGlobal cotizacionGlobal) {
		cotizacionesGlobales.remove(cotizacionGlobal.getFolio());
		
		// Para que se comporte como la implementación de DAOUsuarioBD, regresamos true
		// Dado que no se marca un error en la consulta SQL si no existe el usuario.
		return true;
	}

	@Override
	public ArrayList<CotizacionGlobal> recuperaTodas() {
		Collection<CotizacionGlobal> coleccion = cotizacionesGlobales.values();
		ArrayList<CotizacionGlobal> resultado = new ArrayList<CotizacionGlobal>(coleccion);
		return (!resultado.isEmpty()) ? resultado:null;  
	}
}
