package test.mx.uam.ayd.proyecto.datos;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import mx.uam.ayd.proyecto.datos.DAOSubCotizacion;
import mx.uam.ayd.proyecto.datos.DAOSubCotizacionBD;
import mx.uam.ayd.proyecto.datos.ManejadorBaseDatos;
import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;
import mx.uam.ayd.proyecto.negocio.dominio.Producto;
import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;
import mx.uam.ayd.proyecto.negocio.dominio.Usuario;

/**
 * Clase para las pruebas unitarias de DAOSubCotizacion.
 */
class DAOSubCotizacionBDTest {
	// Creando la instancia de logger para registrar los eventos.
	static Logger log = Logger.getRootLogger();
		
	// Creando la instancia de DAOUsuarioBD para realizar las pruebas.
	DAOSubCotizacion dao = new DAOSubCotizacionBD();
	
	// Valores usados a lo largo de la prueba unitaria.
	static String nombreClase = "DAOSubCotizacionBD";
	static String llavePrimaria = "sub_folio";
	static String nombreBaseDatos = "APP.SUBCOTIZACION";
	static String columnasDeDatos = "sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario";
	
	// Comenzando con las precondiciones.
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		log.info("Comenzando con las precondiciones de la prueba unitaria de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		// Dado que las subcotizaciones requieren un un cliente, usuario y una cotizacion global hecha, las creamos de antemano.
		String clienteACrear = "INSERT INTO APP.CLIENTE (codigo_cliente, nombre, rfc, direccion, telefono, correo_electronico) VALUES ('66669420', 'pepito', '101010', 'direccion', '5539368484', 'pr@test.com') ";
		String cotizacionGlobalACrear = "INSERT INTO APP.COTIZACION_GLOBAL (folio, numero_siniestro, estado, fecha_creacion, fecha_limite, fecha_entrega, numero_piezas_total, precio_costo_total, precio_venta_total, marca_auto, modelo_auto, codigo_cliente) VALUES ('420', '9000', 'prueba', '2019-01-01', '2019-02-02', '2020-03-03', 10, 20.20, 20.20, 'nissan', 'modelo', '66669420')";
		String usuarioACrear = "INSERT INTO APP.USUARIO (id_usuario, nombre, apellido_paterno, apellido_materno, rfc, direccion, telefono, correo_electronico, contrasena, privilegiado) VALUES ('usuariotest', 'nombre', 'paterno', 'materno', '12345679', 'direccion', '5539393939', 'recuperadot@test.com', '17AD5CBB086511CEED018A7EB2CA9F5F435BCE86858F97169B44AA2737BCBDE7', 1)";

		
		log.info("Agregando el cliente a referenciar en todas las inserciones de subcotizacion.");
		statement.execute(clienteACrear);
		
		log.info("Agregando la cotizacion global a referenciar en todas las inserciones de cotizacion global");
		statement.execute(cotizacionGlobalACrear);
		
		log.info("Agregando el usuario a referenciar en todas las inserciones de subcotizacion.");
		statement.execute(usuarioACrear);
		
		String subCotizacionARecuperar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('69', 'prueba', 10, 10.20, 30.10, '420', 'usuariotest')";
		String subCotizacionABorrar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('70', 'prueba', 10, 10.20, 30.10, '420', 'usuariotest')";
		String subCotizacionAActualizar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('71', 'prueba', 10, 10.20, 30.10, '420', 'usuariotest')";
		String subCotizacionRecuperarTodos1 = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('72', 'prueba', 10, 10.20, 30.10, '420', 'usuariotest')";
		String subCotizacionRecuperarTodos2 ="INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('73', 'prueba', 10, 10.20, 30.10, '420', 'usuariotest')";
		
		log.info("Agregando la subcotizacion a recuperar para las pruebas unitarias");
		statement.execute(subCotizacionARecuperar);
		
		log.info("Agregando la subcotizacion a borrar para las pruebas unitarias");
		statement.execute(subCotizacionABorrar);
		
		log.info("Agregando la subcotizacion a actualizar para las pruebas unitarias");
		statement.execute(subCotizacionAActualizar);
		
		log.info("Agregando la subcotizacion a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(subCotizacionRecuperarTodos1);
		
		log.info("Agregando la subcotizacion a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(subCotizacionRecuperarTodos2);
		
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		log.info("Comenzando con el tearDown de la prueba unitaria de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		String borraCliente = "DELETE FROM APP.CLIENTE WHERE codigo_cliente = '66669420'";
		String borraCotizacionGlobal = "DELETE FROM APP.COTIZACION_GLOBAL WHERE folio = '420'";
		String borraUsuario = "DELETE FROM APP.USUARIO WHERE id_usuario = 'usuariotest'";
		
		String borrasubCotizacionCreado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = '74'";
		
		String borrasubCotizacionRecuperado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = '69'";
		String borrasubCotizacionBorrado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = '70'";
		String borrasubCotizacionActualizado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = '71'";
		String borrasubCotizacionRecuperaTodos1 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = '72'";
		String borrasubCotizacionRecuperaTodos2 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = '73'";

		log.info("Borrando la subcotizacion agregada para la prueba recupera");
		statement.execute(borrasubCotizacionRecuperado);
		log.info("Borrando la subcotizacion agregada para la prueba borra");
		statement.execute(borrasubCotizacionBorrado);
		log.info("Borrando la subcotizacion agregada para la prueba actualiza");
		statement.execute(borrasubCotizacionActualizado);
		log.info("Borrando la subcotizacion agregada para la prueba recuperatodos");
		statement.execute(borrasubCotizacionRecuperaTodos1);
		log.info("Borrando la subcotizacion agregada para la prueba recuperatodos");
		statement.execute(borrasubCotizacionRecuperaTodos2);
		
		// En caso de que la creación sea exitosa, se creó una subcotizacion, la borramos en caso de estar ahí
		log.info("Borrando la subcotizacion agregada por la prueba crea.");
		statement.execute(borrasubCotizacionCreado);
		
		// Borrando la cotización global que se referenció en todas las sub cotizaciones.
		log.info("Borrando la cotización global agregada para referenciarla en todas las sub cotizaciones.");
		statement.execute(borraCotizacionGlobal);
		
		// Borrando el cliente que se referenció en la cotización global.
		log.info("Borrando el cliente agregada para referenciarla en la cotizacion global.");
		statement.execute(borraCliente);
		
		// Borrando el usuario que se referenció en todas las sub cotizaciones.
		log.info("Borrando el usuario agregado para referenciarlo en todas las sub cotizaciones.");
		statement.execute(borraUsuario);
	}

	@Test
	void testCrea() throws Exception {
		log.info("Comenzando con la prueba del método Crea de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
	
		// Creando un usuario para colocarlo en la sub cotizacion.
		Usuario usuario = new Usuario();
		usuario.setIdUsuario("usuariotest");
		
		// Creando un arreglo de productos para colocarlos en la subcotización.
		Producto[] productos = new Producto[1];
		
		// Creando una cotización global para colocarla en la sub cotización.
		CotizacionGlobal cotizacionGlobal = new CotizacionGlobal();
		cotizacionGlobal.setFolio("420");
		
		// Creando una subcotizacion para agregarla a la base de datos.
		SubCotizacion subCotizacion = new SubCotizacion("74", "prueba", 10, 10.10, 30.30, usuario, cotizacionGlobal, productos);
		
		dao.crea(subCotizacion);
		

		// Revisando que esté en la base de datos. 
		String consultaSubCotizacionCreada = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = '74'";
		
		// Obteniendo el rst correspondiente a la query hecha al sistema de la subcotizacion creada por el DAO.
		ResultSet rst = statement.executeQuery(consultaSubCotizacionCreada);
		
		// Si hay un resultado en el rst, devuelve true, falso si no. 
		boolean hayUnResultado = rst.next();
		
		assertEquals(hayUnResultado,true);	
	}

	@Test
	void testRecupera() {
		log.info("Comenzando con la prueba del método Recupera de " + nombreClase);
		
		SubCotizacion subCotizacion = dao.recupera("69");
		
		boolean hayUnResultado = (subCotizacion != null) ? true:false;
		
		assertEquals(hayUnResultado,true);
	}

	@Test
	void testActualiza() throws Exception {
		log.info("Comenzando con la prueba del método Actualiza de " + nombreClase);
		
		// Creando un usuario para colocarlo en la sub cotizacion.
		Usuario usuario = new Usuario();
		usuario.setIdUsuario("usuariotest");
		
		// Creando un arreglo de productos para colocarlos en la subcotización.
		Producto[] productos = new Producto[1];
		
		// Creando una cotización global para colocarla en la sub cotización.
		CotizacionGlobal cotizacionGlobal = new CotizacionGlobal();
		cotizacionGlobal.setFolio("420");
		
		// Creando una subcotizacion para agregarla a la base de datos.
		SubCotizacion subCotizacion = new SubCotizacion("71", "pruebamodificada", 10, 10.10, 30.30, usuario, cotizacionGlobal, productos);
		
		boolean subCotizacionActualizada;
		
		// Pasándosela al método actualiza.
		boolean resultado = dao.actualiza(subCotizacion);
		
		if (resultado == true) {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			// revisando que esté actualizado el usuario en la base de datos.
			String consultaCotizacionGlobalCreada = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = '71'";
			
			// Obteniendo el rst correspondiente a la query hecha al sistema de la subcotizacion creada por el DAO.
			ResultSet rst = statement.executeQuery(consultaCotizacionGlobalCreada);
			
			rst.next();
			
			String nombre = rst.getString("estado");
			
			if (nombre.equals("pruebamodificada"))
				subCotizacionActualizada = true;
			else
				subCotizacionActualizada = false;
		}
		else
			subCotizacionActualizada = false;
		assertEquals(subCotizacionActualizada,true);
	}

	@Test
	void testBorra() throws Exception{
		log.info("Comenzando con la prueba del método Borra de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		// Creando una sub cotizacion para pasársela como argumento a borra.
		SubCotizacion subCotizacion = new SubCotizacion();
		subCotizacion.setSubFolio("70");
		
		dao.borra(subCotizacion);
		
		// Revisando que ya no esté en la base de datos.
		String consultaSubCotizacionBorrada = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = '70'";
		
		// Obteniendo el rst correspondiente a la query hecha al sistema de la subcotizacion creada por el DAO.
		ResultSet rst = statement.executeQuery(consultaSubCotizacionBorrada);
		
		// Si hay un resultado en el rst, devuelve true, falso si no. 
		boolean hayUnResultado = rst.next();
		
		assertEquals(hayUnResultado,false);	
	}

	@Test
	void testRecuperaTodas() {
		log.info("Comenzando con la prueba del método RecuperaTodos de " + nombreClase);
		
		// Creando un ArrayList para guardar el resultado de la consulta.
		ArrayList<SubCotizacion> subcotizaciones = new ArrayList<SubCotizacion>();
		
		subcotizaciones = dao.recuperaTodas();
		
		boolean elArregloTieneElementos = (subcotizaciones != null) ? true:false;
		
		assertEquals(elArregloTieneElementos,true);	
	}
	
	@Test
	void testRecuperaTodasCotizacionGlobal() throws Exception {
		log.info("Comenzando con la prueba del método RecuperaTodos por Cotizacion Global de " + nombreClase);
		
		// Creando un ArrayList para guardar el resultado de la consulta.
		ArrayList<SubCotizacion> subcotizaciones = new ArrayList<SubCotizacion>();
		
		subcotizaciones = dao.recuperaTodas("420");
		
		boolean elArregloTieneElementos = (subcotizaciones != null) ? true:false;
		
		assertEquals(elArregloTieneElementos,true);	
	}

}
