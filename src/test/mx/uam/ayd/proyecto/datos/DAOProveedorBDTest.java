package test.mx.uam.ayd.proyecto.datos;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import mx.uam.ayd.proyecto.datos.DAOProveedor;
import mx.uam.ayd.proyecto.datos.DAOProveedorBD;
import mx.uam.ayd.proyecto.datos.ManejadorBaseDatos;
import mx.uam.ayd.proyecto.negocio.dominio.Proveedor;

/**
 * Clase para las pruebas unitarias de DAOProveedor.
 */
class DAOProveedorBDTest {
	
	// Creando la instancia de logger para registrar los eventos.
	static Logger log = Logger.getRootLogger();
	
	// Creando la instancia de DAOClienteBD para realizar las pruebas.
	DAOProveedor dao = new DAOProveedorBD();
	
	// Valores usados a lo largo de la prueba unitaria.
	static String nombreClase = "DAOProveedorBD";
	static String llavePrimaria = "id_proveedor";
	static String nombreBaseDatos = "APP.PROVEEDOR";
	static String columnasDeDatos = "id_proveedor, nombre, rfc, direccion, telefono, correo_electronico, descripcion";

	// Comenzando con las precondiciones.
	@BeforeAll
	static void setUpAfterClass() throws Exception {
		
		log.info("Comenzando con las precondiciones de la prueba unitaria de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		String proveedorARecuperar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES (1, 'proveedor1', 'PVDR8501016F2', 'direccion1', '5598362541', 'prov1@cliente.com', 'descripcion1')";
		String proveedorABorrar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES (2, 'proveedor2', 'PVDR8501013K8', 'direccion2', '5519739874', 'prov2@cliente.com', 'descripcion2')";
		String proveedorAActualizar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES (3,  'proveedor3', 'PVDR8501018T1', 'direccion3', '5598523659', 'prov3@cliente.com', 'descripcion3')";
		String proveedorRecuperarTodos1 = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES (4,  'proveedor4', 'PVDR8501014Y6', 'direccion4', '5523645638', 'prov4@cliente.com', 'descripcion4')";
		String proveedorRecuperarTodos2 ="INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES (5,  'proveedor5', 'PVDR850103P7',  'direccion5','5545211478', 'prov5@cliente.com', 'descripcion5')";

		log.info("Agregando el proveedor a recuperar para las pruebas unitarias");
		statement.execute(proveedorARecuperar);
		
		log.info("Agregando el proveedor a borrar para las pruebas unitarias");
		statement.execute(proveedorABorrar);
		
		log.info("Agregando el proveedor a actualizar para las pruebas unitarias");
		statement.execute(proveedorAActualizar);
		
		log.info("Agregando un proveedor a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(proveedorRecuperarTodos1);
		
		log.info("Agregando un cliente a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(proveedorRecuperarTodos2);
	}

	// Revirtiendo las precondiciones a el estado incial de la base de datos.
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		
		log.info("Comenzando con el tearDown de la prueba unitaria de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		String borraProveedorRecuperado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 1";
		String borraProveedorBorrado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 2";
		String borraProveedorCreado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 6";
		String borraProveedorActualizado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 3";
		String borraProveedorRecuperaTodos1 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 4";
		String borraProveedorRecuperaTodos2 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 5";

		log.info("Borrando el cliente agregado para la prueba recupera");
		statement.execute(borraProveedorRecuperado);
		log.info("Borrando el cliente agregado para la prueba borra");
		statement.execute(borraProveedorBorrado);
		log.info("Borrando el cliente agregado para la prueba actualiza");
		statement.execute(borraProveedorActualizado);
		log.info("Borrando el cliente agregado para la prueba recuperatodos");
		statement.execute(borraProveedorRecuperaTodos1);
		log.info("Borrando el cliente agregado para la prueba recuperatodos");
		statement.execute(borraProveedorRecuperaTodos2);
		
		// En caso de que la creación sea exitosa, se creó un cliente llamado createst, lo borramos en caso de estar ahí
		log.info("Borrando el cliente agregado por la prueba crea");
		statement.execute(borraProveedorCreado);
	}

	@Test
	public void testCrea() throws Exception { 
		log.info("Comenzando con la prueba del método Crea de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		// Creando un proveedor que se agrega para la prueba de creado.
		Proveedor proveedor = new Proveedor();
		proveedor.setNumeroProveedor(6);
		proveedor.setNombre("proveedor6");
		proveedor.setRfc("PVDR8501016K2");
		proveedor.setDireccion("direccion6");
		proveedor.setTelefono("5598152541");
		proveedor.setCorreoElectronico("prov6@cliente.com");
		proveedor.setDescripcion("descripcion6");
		dao.crea(proveedor);		

		// Revisando que esté en la base de datos. 
		String consultaProveedorCreado = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 6";
		
		// Obteniendo el rst correspondiente a la query hecha al sistema del proveedor creado por el DAO.
		ResultSet rst = statement.executeQuery(consultaProveedorCreado);
		
		// Si hay un resultado en el rst, devuelve true, falso si no. 
		boolean hayUnResultado = rst.next();
		
		assertEquals(hayUnResultado,true);	
	}

	@Test
	public void testRecupera() throws Exception {
		log.info("Comenzando con la prueba del método Recupera de " + nombreClase);
		
		Proveedor proveedor = dao.recupera(1);
		
		boolean hayUnResultado = (proveedor != null) ? true:false;
		
		assertEquals(hayUnResultado,true);
	}
	
	@Test
	public void testBorra() throws Exception {
		log.info("Comenzando con la prueba del método Borra de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		// Creando un proveedor para pasárselo como argumento a borra.
		Proveedor proveedor = new Proveedor();
		proveedor.setNumeroProveedor(2);
		proveedor.setNombre("proveedor2");
		proveedor.setRfc("PVDR8501013K8");
		proveedor.setDireccion("direccion2");
		proveedor.setTelefono("5519739874");
		proveedor.setCorreoElectronico("prov2@cliente.com");
		proveedor.setDescripcion("descripcion2");
		
		dao.borra(proveedor);
		
		// Revisando que ya no esté en la base de datos.
		String consultaProveedorBorrado = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 2";
		
		// Obteniendo el rst correspondiente a la query hecha al sistema del proveedor creado por el DAO.
		ResultSet rst = statement.executeQuery(consultaProveedorBorrado);
		
		// Si hay un resultado en el rst, devuelve true, falso si no. 
		boolean hayUnResultado = rst.next();
		
		assertEquals(hayUnResultado,false);			
	}

	@Test
	public void testActualiza() throws Exception {
		log.info("Comenzando con la prueba del método Actualiza de " + nombreClase);
		
		boolean clienteActualizado;
		
		// Creando un proveedor que ya existe en la base de datos
		Proveedor proveedor = new Proveedor(3,  "proveedorCambiado", "PVDR8501018T1", "direccion3", "5598523659", "prov3@cliente.com", "descripcion3");
		
		// Pasándoselo al método actualiza.
		boolean resultado = dao.actualiza(proveedor);
		
		if (resultado == true) {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			// revisando que esté actualizado el cliente en la base de datos.
			String consultaUsuarioCreado = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 3";
			
			// Obteniendo el rst correspondiente a la query hecha al sistema del cliente creado por el DAO.
			ResultSet rst = statement.executeQuery(consultaUsuarioCreado);
			
			rst.next();
			
			String nombre = rst.getString("nombre");
			
			if (nombre.equals("proveedorCambiado"))
				clienteActualizado = true;
			else
				clienteActualizado = false;
		}
		else
			clienteActualizado = false;

		assertEquals(clienteActualizado,true);		
	}
	
	@Test
	void testRecuperaTodos() {
		log.info("Comenzando con la prueba del método RecuperaTodos de " + nombreClase);
		
		// Creando un ArrayList para guardar el resultado de la consulta.
		ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
		
		proveedores = dao.recuperaTodos();
		
		boolean elArregloTieneElementos = (proveedores != null) ? true:false;
		
		assertEquals(elArregloTieneElementos,true);	
	}
}
