package test.mx.uam.ayd.proyecto.datos;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import mx.uam.ayd.proyecto.datos.DAOCliente;
import mx.uam.ayd.proyecto.datos.DAOClienteBD;
import mx.uam.ayd.proyecto.datos.ManejadorBaseDatos;
import mx.uam.ayd.proyecto.negocio.dominio.Cliente;

/**
 * Clase para las pruebas unitarias de DAOCliente.
 */
class DAOClienteBDTest {
	
	// Creando la instancia de logger para registrar los eventos.
	static Logger log = Logger.getRootLogger();
	
	// Creando la instancia de DAOClienteBD para realizar las pruebas.
	DAOCliente dao = new DAOClienteBD();
	
	// Valores usados a lo largo de la prueba unitaria.
	static String nombreClase = "DAOClienteBD";
	static String llavePrimaria = "codigo_cliente";
	static String nombreBaseDatos = "APP.CLIENTE";
	static String columnasDeDatos = "codigo_cliente, nombre, rfc, direccion, telefono, correo_electronico";

	// Comenzando con las precondiciones.
	@BeforeAll
	static void setUpAfterClass() throws Exception {
		
		log.info("Comenzando con las precondiciones de la prueba unitaria de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		String clienteARecuperar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('recuperado', 'cliente1', 'CLTE9001016F2', 'direccion1', '5598362514', 'cliente1@cliente.com')";
		String clienteABorrar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('borrado', 'cliente2', 'CLTE9001013K8', 'direccion2', '5519732513', 'cliente2@cliente.com')";
		String clienteAActualizar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('actualiza',  'cliente3', 'CLTE9001018T1', 'direccion3', '5598523647', 'cliente3@cliente.com')";
		String clienteRecuperarTodos1 = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('rectodos1',  'cliente4', 'CLTE9001014Y6', 'direccion4', '5523648169', 'cliente4@cliente.com')";
		String clienteRecuperarTodos2 ="INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES ('rectodos2',  'cliente5', 'CLTE900103P7',  'direccion5','5545216328', 'cliente5@cliente.com')";

		log.info("Agregando el cliente a recuperar para las pruebas unitarias");
		statement.execute(clienteARecuperar);
		
		log.info("Agregando el cliente a borrar para las pruebas unitarias");
		statement.execute(clienteABorrar);
		
		log.info("Agregando el cliente a actualizar para las pruebas unitarias");
		statement.execute(clienteAActualizar);
		
		log.info("Agregando un cliente a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(clienteRecuperarTodos1);
		
		log.info("Agregando un cliente a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(clienteRecuperarTodos2);
	}

	// Revirtiendo las precondiciones a el estado incial de la base de datos.
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		
		log.info("Comenzando con el tearDown de la prueba unitaria de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		String borraClienteRecuperado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'recuperado'";
		String borraClienteBorrado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'borrado'";
		String borraClienteCreado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'createst'";
		String borraClienteActualizado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'actualiza'";
		String borraClienteRecuperaTodos1 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'rectodos1'";
		String borraClienteRecuperaTodos2 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'rectodos2'";

		log.info("Borrando el cliente agregado para la prueba recupera");
		statement.execute(borraClienteRecuperado);
		log.info("Borrando el cliente agregado para la prueba borra");
		statement.execute(borraClienteBorrado);
		log.info("Borrando el cliente agregado para la prueba actualiza");
		statement.execute(borraClienteActualizado);
		log.info("Borrando el cliente agregado para la prueba recuperatodos");
		statement.execute(borraClienteRecuperaTodos1);
		log.info("Borrando el cliente agregado para la prueba recuperatodos");
		statement.execute(borraClienteRecuperaTodos2);
		
		// En caso de que la creación sea exitosa, se creó un cliente llamado createst, lo borramos en caso de estar ahí
		log.info("Borrando el cliente agregado por la prueba crea");
		statement.execute(borraClienteCreado);
	}

	/* 
	 * Comenzando con las pruebas unitarias.
	 */

	@Test
	public void testCrea() throws Exception { 
		log.info("Comenzando con la prueba del método Crea de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		// Creando un cliente que se agrega para la prueba de creado.
		Cliente cliente = new Cliente();
		cliente.setCodigoCliente("createst");
		dao.crea(cliente);		

		// Revisando que esté en la base de datos. 
		String consultaClienteCreado = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'createst'";
		
		// Obteniendo el rst correspondiente a la query hecha al sistema del cliente creado por el DAO.
		ResultSet rst = statement.executeQuery(consultaClienteCreado);
		
		// Si hay un resultado en el rst, devuelve true, falso si no. 
		boolean hayUnResultado = rst.next();
		
		assertEquals(hayUnResultado,true);	
	}

	@Test
	public void testRecupera() throws Exception {
		log.info("Comenzando con la prueba del método Recupera de " + nombreClase);
		
		Cliente cliente = dao.recupera("recuperado");
		
		boolean hayUnResultado = (cliente != null) ? true:false;
		
		assertEquals(hayUnResultado,true);
	}

	@Test
	public void testBorra() throws Exception {
		log.info("Comenzando con la prueba del método Borra de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		// Creando un cliente para pasárselo como argumento a borra.
		Cliente cliente = new Cliente();
		cliente.setCodigoCliente("borrado");
		
		dao.borra(cliente);
		
		// Revisando que ya no esté en la base de datos.
		String consultaClienteBorrado = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'borrado'";
		
		// Obteniendo el rst correspondiente a la query hecha al sistema del cliente creado por el DAO.
		ResultSet rst = statement.executeQuery(consultaClienteBorrado);
		
		// Si hay un resultado en el rst, devuelve true, falso si no. 
		boolean hayUnResultado = rst.next();
		
		assertEquals(hayUnResultado,false);			
	}

	@Test
	public void testActualiza() throws Exception {
		log.info("Comenzando con la prueba del método Actualiza de " + nombreClase);
		
		boolean clienteActualizado;
		
		// Creando un cliente que ya existe en la base de datos
		Cliente cliente = new Cliente("actualiza", "nombrecambiado", "CLTE9001018T1", "direccion3", "5598523647", "cliente3@cliente.com");
		
		// Pasándoselo al método actualiza.
		boolean resultado = dao.actualiza(cliente);
		
		if (resultado == true) {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			// revisando que esté actualizado el cliente en la base de datos.
			String consultaUsuarioCreado = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 'actualiza'";
			
			// Obteniendo el rst correspondiente a la query hecha al sistema del cliente creado por el DAO.
			ResultSet rst = statement.executeQuery(consultaUsuarioCreado);
			
			rst.next();
			
			String nombre = rst.getString("nombre");
			
			if (nombre.equals("nombrecambiado"))
				clienteActualizado = true;
			else
				clienteActualizado = false;
		}
		else
			clienteActualizado = false;

		assertEquals(clienteActualizado,true);		
	}
	
	@Test
	void testRecuperaTodos() {
		log.info("Comenzando con la prueba del método RecuperaTodos de " + nombreClase);
		
		// Creando un ArrayList para guardar el resultado de la consulta.
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();
		
		clientes = dao.recuperaTodos();
		
		boolean elArregloTieneElementos = (clientes != null) ? true:false;
		
		assertEquals(elArregloTieneElementos,true);	
	}
}
