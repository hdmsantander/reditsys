package test.mx.uam.ayd.proyecto.datos;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import mx.uam.ayd.proyecto.datos.DAOProducto;
import mx.uam.ayd.proyecto.datos.DAOProductoBD;
import mx.uam.ayd.proyecto.datos.ManejadorBaseDatos;
import mx.uam.ayd.proyecto.negocio.dominio.Producto;
import mx.uam.ayd.proyecto.negocio.dominio.Proveedor;
import mx.uam.ayd.proyecto.negocio.dominio.SubCotizacion;
import mx.uam.ayd.proyecto.negocio.dominio.TipoProducto;

/**
 * Clase para las pruebas unitarias de DAOProducto.
 */
class DAOProductoBDTest {
	// Creando la instancia de logger para registrar los eventos.
	static Logger log = Logger.getRootLogger();
	
	// Creando la instancia de DAOProductoBD para realizar las pruebas.
	DAOProducto dao = new DAOProductoBD();
	
	// Valores usados a lo largo de la prueba unitaria.
	static String nombreClase = "DAOProductoDB";
	static String llavePrimaria = "id_producto";
	static String nombreBaseDatos = "APP.PRODUCTO";
	static String columnasDeDatos = "id_producto, descripcion, cantidad, precio_costo_unitario, precio_venta_unitario, subfolio, id_tipo_producto, id_proveedor";
		
	// Comenzando con las precondiciones.
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		log.info("Comenzando con las precondiciones de la prueba unitaria de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		// Dado que las subcotizaciones requieren un un cliente, usuario y una cotizacion global hecha, las creamos de antemano.
		String clienteACrear = "INSERT INTO APP.CLIENTE (codigo_cliente, nombre, rfc, direccion, telefono, correo_electronico) VALUES ('66669420', 'pepito', '101010', 'direccion', '5539368484', 'pr@test.com') ";
		String cotizacionGlobalACrear = "INSERT INTO APP.COTIZACION_GLOBAL (folio, numero_siniestro, estado, fecha_creacion, fecha_limite, fecha_entrega, numero_piezas_total, precio_costo_total, precio_venta_total, marca_auto, modelo_auto, codigo_cliente) VALUES ('420', '9000', 'prueba', '2019-01-01', '2019-02-02', '2020-03-03', 10, 20.20, 20.20, 'nissan', 'modelo', '66669420')";
		String usuarioACrear = "INSERT INTO APP.USUARIO (id_usuario, nombre, apellido_paterno, apellido_materno, rfc, direccion, telefono, correo_electronico, contrasena, privilegiado) VALUES ('usuariotest', 'nombre', 'paterno', 'materno', '12345679', 'direccion', '5539393939', 'recuperadot@test.com', '17AD5CBB086511CEED018A7EB2CA9F5F435BCE86858F97169B44AA2737BCBDE7', 1)";
				
		log.info("Agregando el cliente a referenciar en todas las inserciones de subcotizacion.");
		statement.execute(clienteACrear);
		
		log.info("Agregando la cotizacion global a referenciar en todas las inserciones de cotizacion global");
		statement.execute(cotizacionGlobalACrear);
		
		log.info("Agregando el usuario a referenciar en todas las inserciones de subcotizacion.");
		statement.execute(usuarioACrear);
		
		// Creamos la subcotizacion
		String subCotizacionACrear = "INSERT INTO APP.SUBCOTIZACION (sub_folio, estado, numero_piezas, precio_costo_subtotal, precio_venta_subtotal, folio, id_usuario) VALUES ('69', 'prueba', 10, 10.20, 30.10, '420', 'usuariotest')";
		
		log.info("Agregando la subcotizacion a referenciar en las inserciones de producto");
		statement.execute(subCotizacionACrear);
		
		// Dado que los productos requieren un tipo de producto y un proveedor, los creamos.
		String tipoProductoACrear = "INSERT INTO APP.TIPO_PRODUCTO (id_tipo_producto, nombre) VALUES (12345,'tipoproductotest')";
		String proveedorACrear = "INSERT INTO APP.PROVEEDOR (id_proveedor, nombre, rfc, direccion, telefono, correo_electronico, descripcion) VALUES (12345,'proveedortest', '1234123', 'direccion', '3124312', 'prov@test.com', 'descripcion')";
		
		log.info("Agregando el tipo producto a referenciar en todas las inserciones de producto.");
		statement.execute(tipoProductoACrear);
		
		log.info("Agregando el proveedor a referenciar en todas las inserciones de producto.");
		statement.execute(proveedorACrear);
		
		// Agregando los productos para las pruebas.
		String productoARecuperar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES (9991, 'prueba', 10, 10.20, 30.10, '69', 12345, 12345)";
		String productoABorrar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES (9992, 'prueba', 10, 10.20, 30.10, '69', 12345, 12345)";
		String productoAActualizar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES (9993, 'prueba', 10, 10.20, 30.10, '69', 12345, 12345)";
		String productoRecuperarTodos1 = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES (9994, 'prueba', 10, 10.20, 30.10, '69', 12345, 12345)";
		String productoRecuperarTodos2 ="INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos + ") VALUES (9995, 'prueba', 10, 10.20, 30.10, '69', 12345, 12345)";
		
		log.info("Agregando el producto a recuperar para las pruebas unitarias");
		statement.execute(productoARecuperar);
		
		log.info("Agregando el producto a borrar para las pruebas unitarias");
		statement.execute(productoABorrar);
		
		log.info("Agregando el producto a actualizar para las pruebas unitarias");
		statement.execute(productoAActualizar);
		
		log.info("Agregando el producto a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(productoRecuperarTodos1);
		
		log.info("Agregando el producto a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(productoRecuperarTodos2);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		log.info("Comenzando con el tearDown de la prueba unitaria de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		String borraCliente = "DELETE FROM APP.CLIENTE WHERE codigo_cliente = '66669420'";
		String borraCotizacionGlobal = "DELETE FROM APP.COTIZACION_GLOBAL WHERE folio = '420'";
		String borraUsuario = "DELETE FROM APP.USUARIO WHERE id_usuario = 'usuariotest'";
	
		String borrasubCotizacion = "DELETE FROM APP.SUBCOTIZACION WHERE sub_folio = '69'";
		
		String borraTipoProducto = "DELETE FROM APP.TIPO_PRODUCTO WHERE id_tipo_producto = 12345";
		String borraProveedor = "DELETE FROM APP.PROVEEDOR WHERE id_proveedor = 12345";
		
		String borraProductoCreado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 9996";
		
		String borraProductoRecuperado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 9991";
		String borraProductoBorrado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 9992";
		String borrasProductoActualizado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 9993";
		String borraProductoRecuperaTodos1 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 9994";
		String borraProductoRecuperaTodos2 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 9995";
		
		log.info("Borrando el producto agregado para la prueba recupera");
		statement.execute(borraProductoRecuperado);
		
		log.info("Borrando el producto agregado para la prueba borra");
		statement.execute(borraProductoBorrado);
		
		log.info("Borrando el producto agregado para la prueba actualiza");
		statement.execute(borrasProductoActualizado);
		
		log.info("Borrando el producto agregado para la prueba recupera todos");
		statement.execute(borraProductoRecuperaTodos1);
		
		log.info("Borrando el producto agregado para la prueba recupera todos");
		statement.execute(borraProductoRecuperaTodos2);
		
		log.info("Borrando el producto agregado para la prueba crea");
		statement.execute(borraProductoCreado);
		
		// Borrando el tipo producto que se referenció en el producto.
		log.info("Borrando el tipo de producto que se referenció en producto");
		statement.execute(borraTipoProducto);
		
		// Borrando el proveedor que se referenció en el producto.
		log.info("Borrando el proveedor que se referención en producto");
		statement.execute(borraProveedor);
		
		// Borrando la sub cotización que se referenció en el producto.
		log.info("Borrando la subcotizacion agregada por la prueba crea.");
		statement.execute(borrasubCotizacion);
		
		// Borrando la cotización global que se referenció en todas las sub cotizaciones.
		log.info("Borrando la cotización global agregada para referenciarla en todas las sub cotizaciones.");
		statement.execute(borraCotizacionGlobal);
		
		// Borrando el cliente que se referenció en la cotización global.
		log.info("Borrando el cliente agregada para referenciarla en la cotizacion global.");
		statement.execute(borraCliente);
		
		// Borrando el usuario que se referenció en todas las sub cotizaciones.
		log.info("Borrando el usuario agregado para referenciarlo en todas las sub cotizaciones.");
		statement.execute(borraUsuario);
		
	}

	@Test
	void testCrea() throws Exception{
		log.info("Comenzando con la prueba del método Crea de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
	
		//Creando una subcotizacion que ya existe en la base de datos.
		SubCotizacion subCotizacion = new SubCotizacion();
		subCotizacion.setSubFolio("69");
		
		//Creando un tipo de producto que ya existe en la base de datos.
		TipoProducto tipoProducto = new TipoProducto();
		tipoProducto.setNumeroTipoProducto(12345);
		
		//Creando un proveedor que ya existe en la base de datos.
		Proveedor proveedor = new Proveedor();
		proveedor.setNumeroProveedor(12345);
		
		//Creando un producto
		Producto producto = new Producto();
		producto.setNumeroProducto(9996);
		producto.setSubCotizacion(subCotizacion);
		producto.setTipoProducto(tipoProducto);
		producto.setProveedor(proveedor);
		
		dao.crea(producto);
		
		// Revisando que esté en la base de datos. 
		String consultaProductoCreada = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 9996";
		
		// Obteniendo el rst correspondiente a la query hecha al sistema de la subcotizacion creada por el DAO.
		ResultSet rst = statement.executeQuery(consultaProductoCreada);
		
		// Si hay un resultado en el rst, devuelve true, falso si no. 
		boolean hayUnResultado = rst.next();
		
		assertEquals(hayUnResultado,true);
	}

	@Test
	void testRecupera() {
		log.info("Comenzando con la prueba del método Recupera de " + nombreClase);
		
		Producto producto = dao.recupera(9991);
		
		boolean hayUnResultado = (producto != null) ? true:false;
		
		assertEquals(hayUnResultado,true);
	}

	@Test
	void testActualiza() throws Exception {
		log.info("Comenzando con la prueba del método Actualiza de " + nombreClase);
		
		//Creando una subcotizacion que ya existe en la base de datos.
		SubCotizacion subCotizacion = new SubCotizacion();
		subCotizacion.setSubFolio("69");
		
		//Creando un tipo de producto que ya existe en la base de datos.
		TipoProducto tipoProducto = new TipoProducto();
		tipoProducto.setNumeroTipoProducto(12345);
		
		//Creando un proveedor que ya existe en la base de datos.
		Proveedor proveedor = new Proveedor();
		proveedor.setNumeroProveedor(12345);
		
		//Creando un producto
		Producto producto = new Producto(9993, "pruebaactualizado", 10, 10.20, 30.10, subCotizacion, tipoProducto, proveedor);
		
		boolean productoActualizado;
		
		// Pasándosela al método actualiza.
		boolean resultado = dao.actualiza(producto);
		
		if (resultado == true) {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();
			
			// revisando que esté actualizado el usuario en la base de datos.
			String consultaCotizacionGlobalCreada = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 9993";
			
			// Obteniendo el rst correspondiente a la query hecha al sistema de la subcotizacion creada por el DAO.
			ResultSet rst = statement.executeQuery(consultaCotizacionGlobalCreada);
			
			rst.next();
			
			String nombre = rst.getString("descripcion");
			
			if (nombre.equals("pruebaactualizado"))
				productoActualizado = true;
			else
				productoActualizado = false;
		}
		else
			productoActualizado = false;
		assertEquals(productoActualizado,true);
	}

	@Test
	void testBorra() throws Exception{
		log.info("Comenzando con la prueba del método Borra de " + nombreClase);
		
		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();
		
		// Creando un producto para pasárselo a borra.
		Producto producto = new Producto();
		producto.setNumeroProducto(9992);
		
		dao.borra(producto);
		
		// Revisando que ya no esté en la base de datos.
		String consultaSubCotizacionBorrada = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = 9992";
		
		// Obteniendo el rst correspondiente a la query hecha al sistema de la subcotizacion creada por el DAO.
		ResultSet rst = statement.executeQuery(consultaSubCotizacionBorrada);
		
		// Si hay un resultado en el rst, devuelve true, falso si no. 
		boolean hayUnResultado = rst.next();
		
		assertEquals(hayUnResultado,false);	
	}

	@Test
	void testRecuperaTodos() {
		log.info("Comenzando con la prueba del método RecuperaTodos de " + nombreClase);
		
		// Creando un ArrayList para guardar el resultado de la consulta.
		ArrayList<Producto> productos = new ArrayList<Producto>();
		
		productos = dao.recuperaTodos();
		
		boolean elArregloTieneElementos = (productos != null) ? true:false;
		
		assertEquals(elArregloTieneElementos,true);	
	}

	@Test
	void testRecuperaTodosSubCotizacion() {
		log.info("Comenzando con la prueba del método RecuperaTodos de " + nombreClase);

		// Creando una subcotizacion que ya existe en la base de datos.
		SubCotizacion subCotizacion = new SubCotizacion();
		subCotizacion.setSubFolio("69");
		
		// Creando un ArrayList para guardar el resultado de la consulta.
		ArrayList<Producto> productos = new ArrayList<Producto>();
		
		productos = dao.recuperaTodos(subCotizacion);
		
		boolean elArregloTieneElementos = (productos != null) ? true:false;
		
		assertEquals(elArregloTieneElementos,true);	
	}

}
