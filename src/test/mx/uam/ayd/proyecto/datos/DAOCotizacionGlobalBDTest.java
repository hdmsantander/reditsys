package test.mx.uam.ayd.proyecto.datos;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import mx.uam.ayd.proyecto.datos.DAOCotizacionGlobal;
import mx.uam.ayd.proyecto.datos.DAOCotizacionGlobalBD;
import mx.uam.ayd.proyecto.datos.ManejadorBaseDatos;
import mx.uam.ayd.proyecto.negocio.dominio.Cliente;
import mx.uam.ayd.proyecto.negocio.dominio.CotizacionGlobal;

/**
 * Clase para las pruebas unitarias de DAOCotizacionGlobal.
 */
class DAOCotizacionGlobalBDTest {

	// Creando la instancia de logger para registrar los eventos.
	static Logger log = Logger.getRootLogger();

	// Creando la instancia de DAOUsuarioBD para realizar las pruebas.
	DAOCotizacionGlobal dao = new DAOCotizacionGlobalBD();

	// Valores usados a lo largo de la prueba unitaria.
	static String nombreClase = "DAOCotizacionGlobalBD";
	static String llavePrimaria = "folio";
	static String nombreBaseDatos = "APP.COTIZACION_GLOBAL";
	static String columnasDeDatos = "folio, numero_siniestro, estado, fecha_creacion, fecha_limite, fecha_entrega, numero_piezas_total, precio_costo_total, precio_venta_total, marca_auto, modelo_auto, codigo_cliente";

	// Comenzando con las precondiciones.
	@BeforeAll
	static void setUpBeforeClass() throws Exception {

		log.info("Comenzando con las precondiciones de la prueba unitaria de " + nombreClase);

		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();

		String clienteACrear = "INSERT INTO APP.CLIENTE (codigo_cliente, nombre, rfc, direccion, telefono, correo_electronico) VALUES ('66669420', 'pepito', '101010', 'direccion', '5539368484', 'pr@test.com')";
		String cotizacionGlobalARecuperar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos
				+ ") VALUES ('420', '9000', 'prueba', '2019-01-01', '2019-02-02', '2020-03-03', 10, 20.20, 20.20, 'nissan', 'modelo', '66669420')";
		String cotizacionGlobaloABorrar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos
				+ ") VALUES ('421', '9000', 'prueba', '2019-01-01', '2019-02-02', '2020-03-03', 10, 20.20, 20.20, 'nissan', 'modelo', '66669420')";
		String cotizacionGlobalAActualizar = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos
				+ ") VALUES ('422', '9000', 'prueba', '2019-01-01', '2019-02-02', '2020-03-03', 10, 20.20, 20.20, 'nissan', 'modelo', '66669420')";
		String cotizacionGlobalRecuperarTodos1 = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos
				+ ") VALUES ('423', '9000', 'prueba', '2019-01-01', '2019-02-02', '2020-03-03', 10, 20.20, 20.20, 'nissan', 'modelo', '66669420')";
		String cotizacionGlobalRecuperarTodos2 = "INSERT INTO " + nombreBaseDatos + " (" + columnasDeDatos
				+ ") VALUES ('424', '9000', 'prueba', '2019-01-01', '2019-02-02', '2020-03-03', 10, 20.20, 20.20, 'nissan', 'modelo', '66669420')";

		log.info("Agregando el cliente a referenciar en todas las inserciones de cotizacion global.");
		statement.execute(clienteACrear);

		log.info("Agregando la cotizacion global a recuperar para las pruebas unitarias");
		statement.execute(cotizacionGlobalARecuperar);

		log.info("Agregando la cotizacion global a borrar para las pruebas unitarias");
		statement.execute(cotizacionGlobaloABorrar);

		log.info("Agregando la cotizacion global a actualizar para las pruebas unitarias");
		statement.execute(cotizacionGlobalAActualizar);

		log.info("Agregando la cotizacion global a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(cotizacionGlobalRecuperarTodos1);

		log.info("Agregando la cotizacion global a recuperar para las pruebas unitarias cuando se recuperan todos");
		statement.execute(cotizacionGlobalRecuperarTodos2);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {

		log.info("Comenzando con el tearDown de la prueba unitaria de " + nombreClase);

		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();

		String borraCliente = "DELETE FROM APP.CLIENTE WHERE codigo_cliente = '66669420'";

		String borracotizacionGlobalCreado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = '425'";

		String borracotizacionGlobalRecuperado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria
				+ " = '420'";
		String borracotizacionGlobalBorrado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria + " = '421'";
		String borracotizacionGlobalActualizado = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria
				+ " = '422'";
		String borracotizacionGlobalRecuperaTodos1 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria
				+ " = '423'";
		String borracotizacionGlobalRecuperaTodos2 = "DELETE FROM " + nombreBaseDatos + " WHERE " + llavePrimaria
				+ " = '424'";

		log.info("Borrando la cotizacion global agregada para la prueba recupera");
		statement.execute(borracotizacionGlobalRecuperado);
		log.info("Borrando la cotizacion global agregada para la prueba borra");
		statement.execute(borracotizacionGlobalBorrado);
		log.info("Borrando la cotizacion global agregada para la prueba actualiza");
		statement.execute(borracotizacionGlobalActualizado);
		log.info("Borrando la cotizacion global agregada para la prueba recuperatodos");
		statement.execute(borracotizacionGlobalRecuperaTodos1);
		log.info("Borrando la cotizacion global agregada para la prueba recuperatodos");
		statement.execute(borracotizacionGlobalRecuperaTodos2);

		// En caso de que la creación sea exitosa, se creó una cotizacion global la
		// borramos en caso de estar ahí
		log.info("Borrando la cotizacion global agregada por la prueba crea.");
		statement.execute(borracotizacionGlobalCreado);

		// Borrando el cliente que se referenció en todas las cotizaciones globales.
		log.info("Borrando el cliente agregado para referenciarlo en todas las cotizaciones globales.");
		statement.execute(borraCliente);
	}

	@Test
	void testCrea() throws Exception {
		log.info("Comenzando con la prueba del método Crea de " + nombreClase);

		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();

		// Creando un cliente para colocarlo en la cotizacion global.
		Cliente cliente = new Cliente();
		cliente.setCodigoCliente("66669420");

		String fecha1 = "2019-01-01";
		String fecha2 = "2019-02-02";
		String fecha3 = "2020-03-03";

		// Creando las fechas de la cotización global.
		Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha1);
		Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha2);
		Date date3 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha3);

		// Creando una cotizacionGlobal que se agrega para la prueba de creado.
		CotizacionGlobal cotizacionGlobal = new CotizacionGlobal("425", "9000", "prueba", date1, date2, date3, 10,
				20.20, 20.20, "nissan", "modelo", cliente, null);
		dao.crea(cotizacionGlobal);

		// Revisando que esté en la base de datos.
		String consultaCotizacionGlobalCreada = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria
				+ " = '425'";

		// Obteniendo el rst correspondiente a la query hecha al sistema de la
		// cotizacion global creada por el DAO.
		ResultSet rst = statement.executeQuery(consultaCotizacionGlobalCreada);

		// Si hay un resultado en el rst, devuelve true, falso si no.
		boolean hayUnResultado = rst.next();

		assertEquals(hayUnResultado, true);
	}

	@Test
	void testRecupera() throws Exception {
		log.info("Comenzando con la prueba del método Recupera de " + nombreClase);

		CotizacionGlobal cotizacionGlobal = dao.recupera("420");

		boolean hayUnResultado = (cotizacionGlobal != null) ? true : false;

		assertEquals(hayUnResultado, true);
	}

	@Test
	void testActualiza() throws Exception {
		log.info("Comenzando con la prueba del método Actualiza de " + nombreClase);

		// Creando un cliente para colocarlo en la cotizacion global.
		Cliente cliente = new Cliente();
		cliente.setCodigoCliente("66669420");

		String fecha1 = "2019-01-01";
		String fecha2 = "2019-02-02";
		String fecha3 = "2020-03-03";

		// Creando las fechas de la cotización global.
		Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha1);
		Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha2);
		Date date3 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha3);

		boolean cotizacionActualizada;

		// Creando una cotizacion global que ya existe en la base de datos
		CotizacionGlobal cotizacionGlobal = new CotizacionGlobal("422", "9000", "pruebacambiada", date1, date2, date3,
				10, 20.20, 20.20, "nissan", "modelo", cliente, null);

		// Pasándosela al método actualiza.
		boolean resultado = dao.actualiza(cotizacionGlobal);

		if (resultado == true) {
			// Creando conexión a la base de datos.
			Connection connection = ManejadorBaseDatos.getConnection();
			Statement statement = connection.createStatement();

			// revisando que esté actualizada la cotizacion global en la base de datos.
			String consultaCotizacionGlobalCreada = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria
					+ " = '422'";

			// Obteniendo el rst correspondiente a la query hecha al sistema de la
			// cotizacion global creada por el DAO.
			ResultSet rst = statement.executeQuery(consultaCotizacionGlobalCreada);

			rst.next();

			String nombre = rst.getString("estado");

			if (nombre.equals("pruebacambiada"))
				cotizacionActualizada = true;
			else
				cotizacionActualizada = false;
		} else
			cotizacionActualizada = false;
		assertEquals(cotizacionActualizada, true);
	}

	@Test
	void testBorra() throws Exception {
		log.info("Comenzando con la prueba del método Borra de " + nombreClase);

		// Creando conexión a la base de datos.
		Connection connection = ManejadorBaseDatos.getConnection();
		Statement statement = connection.createStatement();

		// Creando una cotizacion global para pasársela como argumento a borra.
		CotizacionGlobal cotizacionGlobal = new CotizacionGlobal();
		cotizacionGlobal.setFolio("421");

		dao.borra(cotizacionGlobal);

		// Revisando que ya no esté en la base de datos.
		String consultaCotizacionGlobalBorrada = "SELECT * FROM " + nombreBaseDatos + " WHERE " + llavePrimaria
				+ " = '421'";

		// Obteniendo el rst correspondiente a la query hecha al sistema de la
		// cotizacion global creada por el DAO.
		ResultSet rst = statement.executeQuery(consultaCotizacionGlobalBorrada);

		// Si hay un resultado en el rst, devuelve true, falso si no.
		boolean hayUnResultado = rst.next();

		assertEquals(hayUnResultado, false);
	}

	@Test
	void testRecuperaTodas() {
		log.info("Comenzando con la prueba del método RecuperaTodos de " + nombreClase);

		// Creando un ArrayList para guardar el resultado de la consulta.
		ArrayList<CotizacionGlobal> cotizaciones = new ArrayList<CotizacionGlobal>();

		cotizaciones = dao.recuperaTodas();

		boolean elArregloTieneElementos = (cotizaciones != null) ? true : false;

		assertEquals(elArregloTieneElementos, true);
	}

}
