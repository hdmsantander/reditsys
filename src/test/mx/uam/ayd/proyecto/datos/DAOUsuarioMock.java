package test.mx.uam.ayd.proyecto.datos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import mx.uam.ayd.proyecto.datos.DAOUsuario;
import mx.uam.ayd.proyecto.negocio.dominio.Usuario;

/**
 * Mock para la interfaz DAOUsuario.
 */
public class DAOUsuarioMock implements DAOUsuario {
	
	private Map <String, Usuario> usuarios = new HashMap <String, Usuario>();

	@Override
	public boolean crea(Usuario usuario) {
		// Revisamos que no exista ya una entrada con ese identificador
		boolean existeUsuario = usuarios.containsKey(usuario.getIdUsuario());
		
		// Si no existe, la agregamos
		if (!existeUsuario ) {
			usuarios.put(usuario.getIdUsuario(), usuario);
		}
		
		// Si no existía, regresamos verdadero, en otro caso falso.
		return (existeUsuario == false) ? true:false;
	}

	@Override
	public Usuario recupera(String nombre) {
		Usuario usuario = usuarios.get(nombre);
		
		return (usuario!=null) ? usuario:null;
	}

	@Override
	public boolean actualiza(Usuario usuario) {
		// Revisamos que exista ya una entrada con ese identificador
		boolean existeUsuario = usuarios.containsKey(usuario.getIdUsuario());
		
		// Si existe, la actualizamos
		if (existeUsuario ) {
			usuarios.put(usuario.getIdUsuario(), usuario);
		}

		// Para que se comporte como la implementación de DAOUsuarioBD, regresamos true
		// Dado que no se marca un error en la consulta SQL si no existe el usuario.
		return true;
	}

	@Override
	public boolean borra(Usuario usuario) {
		usuarios.remove(usuario.getIdUsuario());
		
		// Para que se comporte como la implementación de DAOUsuarioBD, regresamos true
		// Dado que no se marca un error en la consulta SQL si no existe el usuario.
		return true;
	}

	@Override
	public ArrayList<Usuario> recuperaTodos() {
		Collection<Usuario> coleccion = usuarios.values();
		ArrayList<Usuario> resultado = new ArrayList<Usuario>(coleccion);
		return (!resultado.isEmpty()) ? resultado:null;  
	}

}
